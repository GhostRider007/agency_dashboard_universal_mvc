﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelLibrary
{
    public class MailingCredentialModel
    {
        public class MailingCredential
        {
            public int Counter { get; set; }
            public string MailingDepartment { get; set; }
            public string MailFrom { get; set; }
            public string MailTo { get; set; }
            public string CC { get; set; }
            public string BCC { get; set; }
            public string SmtpClient { get; set; }
            public string Subject { get; set; }
            public string AgentId { get; set; }
            public string CreatedDate { get; set; }
            public string UserID { get; set; }
            public string Pass { get; set; }
            public string Body { get; set; }
            public bool Status { get; set; }
            public string Regards { get; set; }
        }        
    }
}
