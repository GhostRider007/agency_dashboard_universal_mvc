﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace ModelLibrary
{
    public class AccountModel
    {
        public class AgentStaffLogin
        {
            public string UserId { get; set; }
            public string Password { get; set; }
            public string IpAddress { get; set; }
        }
        public class ForgetPassword
        {
            public string UserId { get; set; }
            public string Email { get; set; }
            public string Mobile { get; set; }
        }
        public class AccountLedgerReport
        {
            public string Id { get; set; }

            public string Passbook { get; set; }
            public string IsCorp { get; set; }
            public string LogoUrl { get; set; }
            public string PaidStatus { get; set; }
            public string AgencyID { get; set; }
            public string ResuID { get; set; }
            public string ResuCharge { get; set; }
            public string ResuServiseCharge { get; set; }
            public string ResuFareDiff { get; set; }
            public string AdminMrk { get; set; }
            public string AgentMrk { get; set; }
            public string CashBack { get; set; }
            public string BillNoCorp { get; set; }
            public string BookedBy { get; set; }
            public string SCSRVTAX { get; set; }
            public string DepDate { get; set; }
            public string FltNumber { get; set; }
            public string PaxName { get; set; }
            public string CreatedBy { get; set; }
            public string AgencyName { get; set; }
            public string InvoiceNo { get; set; }
            public string PnrNo { get; set; }
            public string TicketNo { get; set; }
            public string TicketingCarrier { get; set; }
            public string ExecutiveID { get; set; }
            public string Debit { get; set; }
            public string Credit { get; set; }
            public string Aval_Balance { get; set; }

            public string CreatedDate { get; set; }

            public string BookingType { get; set; }
            public string Remark { get; set; }
            public string C { get; set; }
            public string D { get; set; }
            public string PaymentMode { get; set; }
            public string ClosingBalance { get; set; }
            public string DueAmount { get; set; }
            public string CreditLimit { get; set; }
            public string TransType { get; set; }
            public string DistrAgencyID { get; set; }
            public string AccountID { get; set; }
            public string Aircode { get; set; }
            public string AirlinePNR { get; set; }
            public string Loginid { get; set; }
            public string UserType { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string OrderId { get; set; }
            public string Status { get; set; }
            public string SearchType { get; set; }
            public string ServiceType { get; set; }
            public string TransAmount { get; set; }
            public string AvalBal { get; set; }
            public string StaffUserId { get; set; }
            public string StaffId { get; set; }
            public string AgentBal { get; set; }
            public string AgentLimit { get; set; }
            public string OwnerId { get; set; }
            public string DistrID { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }
            public string AgentMobile { get; set; }
            public string Module { get; set; }
            public string PaymentLog { get; set; }
            public string Trip { get; set; }
            public string ProjectID { get; set; }
            public int Totalcount { get; set; }
            public string EasyID { get; set; }
            public string EasyTransID { get; set; }
            public string AgentID { get; set; }
            public string AgentType { get; set; }
            public string GdsPnr { get; set; }
            public string AirLinePnr { get; set; }
            public string JourneyDate { get; set; }
            public string Date { get; set; }
            public string Time { get; set; }
            public string Sector { get; set; }
            public string VC { get; set; }
            public string CreateDate { get; set; }
            public string Title { get; set; }
            public string FName { get; set; }
            public string LName { get; set; }
            public string PaxType { get; set; }
            public string AirLine { get; set; }
            public string TicketNumber { get; set; }
            public string BaseFare { get; set; }
            public string YQ { get; set; }
            public string TotalTax { get; set; }
            public string ServiceTax { get; set; }
            public string TranFee { get; set; }
            public string Discount { get; set; }
            public string TotalDiscount { get; set; }
            public string TotalAfterDis { get; set; }
            public string Tds { get; set; }
            public string ORIGINALDISCOUNT { get; set; }
            public string MgtFee { get; set; }
            public string GST { get; set; }
            public string TotalFare { get; set; }
            public string TicketcopyLink { get; set; }
            public string InvoicecopyLink { get; set; }
            public string YatraAccountID { get; set; }
            public string AgencyAddress { get; set; }
            public string AgencyCity { get; set; }
            public string AgencyZipcode { get; set; }
            public string AgencyState { get; set; }
            public string AgencyCountry { get; set; }
            public string COMPANYGST { get; set; }
            public string FAX { get; set; }
            public string COMPANYNAME { get; set; }
            public string COMPANYADDRESS { get; set; }
            public string PHONE { get; set; }
            public string MOBILENO { get; set; }
            public string COMPANYEMAIL { get; set; }
            public string PANNO { get; set; }
            public string Price { get; set; }
            public List<AccountLedgerReport> AccountLedgerReportList { get; set; }
        }
        public class AccountLedgerFilter
        {
            public string Loginid { get; set; }
            public string Price { get; set; }
            public string UserType { get; set; }
            public string ProjectID { get; set; }
            public string Trip { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string OrderId { get; set; }
            public string PNR { get; set; }
            public string AirlinePNR { get; set; }
            public string TicketNo { get; set; }
            public string AgentId { get; set; }
            public string Status { get; set; }
            public string BookingType { get; set; }
            public string SearchType { get; set; }
            public string Module { get; set; }
            public string ServiceType { get; set; }
            public string StaffUserId { get; set; }
        }
        public class AgentProfileModel
        {
            public string Mobile { get; set; }
            public string Email { get; set; }
            public string FName { get; set; }
            public string LName { get; set; }
            public string Password { get; set; }
            public string Agency_Name { get; set; }
            public string Address { get; set; }
            public string User_Id { get; set; }
            public string Is_GST_Apply { get; set; }
            public string GST_Remark { get; set; }
            public string GST_Number { get; set; }
            public string Gst_CompanyName { get; set; }
            public string Gst_CompAddress { get; set; }
            public string Gst_State { get; set; }
            public string Gst_StateCode { get; set; }
            public string Gst_City { get; set; }
            public string Gst_Pincode { get; set; }
            public string Gst_Phone { get; set; }
            public string Gst_Email { get; set; }
            public string Image_Url { get; set; }
            public List<SelectListItem> StateList { get; set; }
            public List<SelectListItem> CityList { get; set; }
        }
        public class AgentUpdateProfile
        {
            public string Type { get; set; }
            public string AgentId { get; set; }
            public string Password { get; set; }
            public string AgentEmail { get; set; }
            public string LandLine { get; set; }
            public string Fax { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string StateCode { get; set; }
            public string Country { get; set; }
            public string GSTNO { get; set; }
            public string GSTCompanyName { get; set; }
            public string GSTCompanyAddress { get; set; }
            public string GSTPhoneNo { get; set; }
            public string GSTEmail { get; set; }
            public bool IsGSTApply { get; set; }
            public string GSTRemark { get; set; }
            public string GstPinCode { get; set; }       
        }
        public class State
        {
            public string StateName { get; set; }
            public string StateCode { get; set; }
        }

        public class City
        {
            public string CityName { get; set; }
            public string CityCode { get; set; }
        }
    }
}
