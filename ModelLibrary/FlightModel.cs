﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelLibrary
{
    public class FlightModel
    {
        public class FlightTicketReport
        {
            public int HeaderId { get; set; }
            public string Sector { get; set; }
            public string HoldTicket { get; set; }

            public string URL { get; set; }
            public string TotalPax { get; set; }
            public string IsBagFares { get; set; }
            public string CancellationCharge { get; set; }
            public string CancelStatus { get; set; }
            public string RegardingCancel { get; set; }
            public string RefundID { get; set; }
            public string FlightNo { get; set; }
            public string Discount { get; set; }
            public string AcceptDate { get; set; }
            public string Status { get; set; }
            public string JourneyDate { get; set; }
            public string PaxType { get; set; }
            public int Totalcount { get; set; }
            public string Provider { get; set; }
            public string PName { get; set; }
            public string MordifyStatus { get; set; }
            public string GdsPnr { get; set; }
            public string AirlinePnr { get; set; }
            public string VC { get; set; }
            public string PGCharges { get; set; }
            public string Duration { get; set; }
            public string TripType { get; set; }
            public string TourCode { get; set; }
            public string Destination { get; set; }
            public string Departure { get; set; }
            public decimal TotalBookingCost { get; set; }
            public decimal TotalAfterDis { get; set; }
            public decimal SFDis { get; set; }
            public decimal AdditionalMarkup { get; set; }
            public int Adult { get; set; }
            public int Child { get; set; }
            public int Infant { get; set; }
            public string AgentId { get; set; }
            public string AgencyName { get; set; }
            public string AgentType { get; set; }
            public string DistrId { get; set; }
            public string ExecutiveId { get; set; }
            public string PaymentType { get; set; }
            public string PgTitle { get; set; }
            public string PgFName { get; set; }
            public string PgLName { get; set; }
            public string PgMobile { get; set; }
            public string PgEmail { get; set; }
            public string Remark { get; set; }
            public string RejectedRemark { get; set; }
            public string CreateDate { get; set; }
            public string UpdateDate { get; set; }
            public string SubmitDate { get; set; }
            public string ResuID { get; set; }
            public decimal ResuCharge { get; set; }
            public decimal ResuServiseCharge { get; set; }
            public decimal ResuFareDiff { get; set; }
            public int PaxId { get; set; }
            public decimal ImportCharge { get; set; }
            public bool YFLAG { get; set; }
            public bool YCRN { get; set; }
            public bool Y_CAN_FARE { get; set; }
            public string ProjectID { get; set; }
            public string BillNoCorp { get; set; }
            public string BookedBy { get; set; }
            public string IsMobile { get; set; }
            public string FareType { get; set; }
            public string ReferenceNo { get; set; }
            public string APIID { get; set; }
            public string PartnerName { get; set; }
            public string PreHoldREmark { get; set; }
            public string PreHoldUpdatedBy { get; set; }
            public string PreholdUpdateDate { get; set; }
            public string PaymentMode { get; set; }
            public string PgTid { get; set; }
            public string PgCharges { get; set; }
            public string FareRule { get; set; }
            public bool ReIssueRefundStatus { get; set; }
            public bool MSent { get; set; }
            public string GSTNO { get; set; }
            public string GST_Company_Name { get; set; }
            public string GST_Company_Address { get; set; }
            public string GST_PhoneNo { get; set; }
            public string GST_Email { get; set; }
            public string GSTRemark { get; set; }
            public string SearchId { get; set; }
            public string PNRId { get; set; }
            public string Pnr_Locator { get; set; }
            public string Block_Issue { get; set; }
            public string Base_Fare { get; set; }
            public string Tax { get; set; }
            public string YQ { get; set; }
            public string Service_Tax { get; set; }
            public string TDS { get; set; }
            public string CB { get; set; }
            public string TicketId { get; set; }
            public decimal HoldCharge { get; set; }
            public string IsHoldByAgent { get; set; }
            public string HoldDateTime { get; set; }
            public decimal CreditNode { get; set; }
            public decimal DebitNode { get; set; }
            public string PGStatus { get; set; }
            public string TicketStatus { get; set; }
            public string FamilyFare { get; set; }
            public decimal GSTDIFF { get; set; }
            public DateTime GstUpdateDateTime { get; set; }
            public bool BookedByStaff { get; set; }
            public string StaffId { get; set; }
            public string CouponCode { get; set; }
            public string OldOrderId { get; set; }
            public string OldAirlinePnr { get; set; }
            public DateTime OldPnrDateTime { get; set; }

            public string BookingDate { get; set; }
            public string UserID { get; set; }
            public string ArrDate { get; set; }
            public string FNo { get; set; }
            public string ArrTime { get; set; }
            public string DepTime { get; set; }
            public string Title { get; set; }
            public string FName { get; set; }
            public string MName { get; set; }
            public string LName { get; set; }
            public string BaseFare { get; set; }
            public string STax { get; set; }
            public string TFee { get; set; }
            public string Dis { get; set; }
            public string TotalFare { get; set; }
            public string TotalFareAfterDis { get; set; }
            public string MgtFee { get; set; }
            public string DepartDate { get; set; }
            public List<FlightTicketReport> FlightTicketReportList { get; set; }

            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string OrderId { get; set; }
            public string PNR { get; set; }
            public string AirlinePNR { get; set; }
            public string PaxName { get; set; }
            public string TicketNo { get; set; }
            public string Trip { get; set; }


        }
        public class FlightTicketFilter
        {
            public string Loginid { get; set; }
            public string Actionby { get; set; }
            public string ExecID { get; set; }
            public string distrid { get; set; }
            public string PartnerName { get; set; }
            public string PaymentMode { get; set; }
            public string UserType { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string OrderId { get; set; }
            public string PNR { get; set; }
            public string AirlinePNR { get; set; }
            public string PaxName { get; set; }
            public string TicketNo { get; set; }
            public string AgentId { get; set; }
            public string Trip { get; set; }
            public string Status { get; set; }
            public string ProxyTrip { get; set; }
        }
        public enum FlightStatus
        {
            Pending,
            InProcess,
            Ticketed,
            Rejected,
            Cancelled,
            CancelRequest,
            CancelInprocess,
            CancelRejecRejectt,
            ReIssueRequest,
            ReIssueInProcess,
            ReIssue,
            ProxyRequest,
            ProxyInProcess,
            Confirm,
            Hold
        }
        public class ReIssueReFund
        {
            public string PNR { get; set; }
            public string Sector { get; set; }
            public string BookingDate { get; set; }
            public decimal TotalBookingCost { get; set; }
            public decimal TotalAfterDis { get; set; }
            public string UserID { get; set; }
            public string AgencyName { get; set; }
            public string OrderId { get; set; }
            public string AirlinePNR { get; set; }
            public string Trip { get; set; }
            public string ProjectID { get; set; }
            public string VC { get; set; }
            public string Departure { get; set; }
            public string Destination { get; set; }
            public string DepartDate { get; set; }
            public string ArrDate { get; set; }
            public string FNo { get; set; }
            public string ArrTime { get; set; }
            public string DepTime { get; set; }
            public string TicketNo { get; set; }
            public string Title { get; set; }
            public string FName { get; set; }
            public string MName { get; set; }
            public string LName { get; set; }
            public string PaxType { get; set; }
            public string PaxID { get; set; }
            public string PaxName { get; set; }
            public string BaseFare { get; set; }
            public string Tax { get; set; }
            public string YQ { get; set; }
            public string STax { get; set; }
            public string TFee { get; set; }
            public string Dis { get; set; }
            public string CB { get; set; }
            public string TDS { get; set; }
            public string TotalFare { get; set; }
            public string TotalFareAfterDis { get; set; }
            public decimal MgtFee { get; set; }
            public string BillNoCorp { get; set; }
            public string PartnerName { get; set; }
            public string ResuID { get; set; }
            public string PName { get; set; }
            public string Remark { get; set; }
            public string Status { get; set; }
            public string ReqTyp { get; set; }
            public string RefundID { get; set; }
            public string CancelledBy { get; set; }
            public string BillNoAir { get; set; }
            public string ActionType { get; set; }
        }
        public class OfflineRequest
        {
            public int ProxyID { get; set; }
            public string TripType { get; set; }
            public string ExecID { get; set; }
            public string BookingType { get; set; }
            public string FromCity { get; set; }
            public string FromCityCode { get; set; }
            public string DestinationCity { get; set; }
            public string ToCityCode { get; set; }
            public string DepartureDate { get; set; }//"17/07/20"
            public string DepartTime { get; set; }
            public string DepartDate { get; set; }
            public string DepartDay { get; set; }
            public string DepartMonth { get; set; }
            public string DepartYear { get; set; }
            public string ReturnDate { get; set; }
            public string Time { get; set; }//"Midday(10:00-14:00)"
            public string ReturnTime { get; set; }
            public string PaymentMode { get; set; }
            public string Adult { get; set; }
            public string Child { get; set; }
            public string Infant { get; set; }
            public string ClassType { get; set; }//"Business"
            public string PreferedAirlines { get; set; }
            public string PreferedAirlinesCode { get; set; }//"6E"
            public string Classes { get; set; }//M"
            public string PaymentLimit { get; set; }//"CL"
            public string Remark { get; set; }//""
            public string AgentId { get; set; }//"B2BAPI"
            public string AgentUser_Id { get; set; }
            public string Agentname { get; set; }//"Richa Travels"
            public string FromCountry { get; set; }//"IN"
            public string ToCountry { get; set; }//"IN"
            public string PType { get; set; }//"Nothing"
            public string ProjectId { get; set; }//"Nothing"
            public string BookedBy { get; set; }//"Nothing"
            public string Status { get; set; }
            public string Trip { get; set; }
            public string TravleType { get; set; }
            public string ProxyFrom { get; set; }
            public string ProxyTo { get; set; }
            public string requestDateTime { get; set; }
            public string UpdateDate { get; set; }
            public string AcceptedDate { get; set; }
            public string ProxyType { get; set; }
            public int Totalcount { get; set; }
            public string FromDate { get; set; }

            public string ToDate { get; set; }
            public List<OfflineBookFlightDetail> AdultDetails { get; set; }
            public List<OfflineBookFlightDetail> ChildDetails { get; set; }
            public List<OfflineBookFlightDetail> InFantDetails { get; set; }
            public List<OfflineRequest> OfflineRequestList { get; set; }
        }
        public class OfflineBookFlightDetail
        {
            public string AgentID { get; set; }
            public string ProxyId { get; set; }
            public string Title { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Age { get; set; }
            public string PaxType { get; set; }
            public string FreqFlyerNO { get; set; }
            public string PassportNo { get; set; }
            public string PPExp { get; set; }
            public string VisaDet { get; set; }
        }
        public class FlightPassengerDetail
        {
            public string AgentID { get; set; }
            public string BookingType { get; set; }
            public string TravelType { get; set; }
            public string ProxyTo { get; set; }
            public string ProxyFrom { get; set; }
            public string DepartDate { get; set; }
            public string ReturnDate { get; set; }
            public string DepartTime { get; set; }
            public string ReturnTime { get; set; }
            public string Adult { get; set; }
            public string Child { get; set; }
            public string Infrant { get; set; }
            public string Class { get; set; }
            public string Airlines { get; set; }
            public string Classes { get; set; }
            public string PaymentMode { get; set; }
            public string Remark { get; set; }
            public string RejectComment { get; set; }

            public string AGName { get; set; }
            public string AGAddress { get; set; }
            public string AGAddr { get; set; }
            public string AGMobile { get; set; }
            public string AGEmail { get; set; }
            public string Agency_Name { get; set; }
            public string AGCrd_Limit { get; set; }

            public List<AdultChildInfrant> AdultList { get; set; }
            public List<AdultChildInfrant> ChildList { get; set; }
            public List<AdultChildInfrant> InfrantList { get; set; }
        }
        public class AdultChildInfrant
        {
            public int EmpId { get; set; }
            public string ProxyID { get; set; }
            public string SirName { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Age { get; set; }
            public string AgentID { get; set; }
            public string FFno { get; set; }
            public string PassportNo { get; set; }
            public string PPexp { get; set; }
            public string VisaDet { get; set; }
        }

        //For TicketCopy       
        public class FlightHeaderModel
        {
            public string OrderId { get; set; }

            public string logoUrl { get; set; }
            public string sector { get; set; }
            public string Status { get; set; }
            public string MordifyStatus { get; set; }
            public string GdsPnr { get; set; }
            public string AirlinePnr { get; set; }
            public string VC { get; set; }
            public string Duration { get; set; }
            public string TripType { get; set; }
            public string Trip { get; set; }
            public string TourCode { get; set; }
            public string TotalBookingCost { get; set; }
            public string TotalAfterDis { get; set; }
            public string SFDis { get; set; }
            public string AdditionalMarkup { get; set; }
            public string Adult { get; set; }
            public string Child { get; set; }
            public string Infant { get; set; }
            public string AgentId { get; set; }
            public string AgencyName { get; set; }
            public string DistrId { get; set; }
            public string ExecutiveId { get; set; }
            public string PaymentType { get; set; }
            public string PgTitle { get; set; }
            public string PgFName { get; set; }
            public string PgLName { get; set; }
            public string PgMobile { get; set; }
            public string PgEmail { get; set; }
            public string Remark { get; set; }
            public string RejectedRemark { get; set; }
            public string CreateDate { get; set; }
            public string UpdateDate { get; set; }
            public string ResuID { get; set; }
            public string ResuCharge { get; set; }
            public string ResuServiseCharge { get; set; }
            public string ResuFareDiff { get; set; }
            public string PaxId { get; set; }
            public string ImportCharge { get; set; }
            public string YFLAG { get; set; }
            public string YCRN { get; set; }
            public string Y_CAN_FARE { get; set; }
            public string ProjectID { get; set; }
            public string BillNoCorp { get; set; }
            public string BookedBy { get; set; }
            public string IsMobile { get; set; }
            public string FareType { get; set; }
            public string ReferenceNo { get; set; }
            public string APIID { get; set; }
            public string PartnerName { get; set; }
            public string PreHoldREmark { get; set; }
            public string PreHoldUpdatedBy { get; set; }
            public string PreholdUpdateDate { get; set; }
            public string PaymentMode { get; set; }
            public string PgTid { get; set; }
            public string PgCharges { get; set; }
            public string FareRule { get; set; }
            public string ReIssueRefundStatus { get; set; }
            public string MSent { get; set; }
            public string GSTNO { get; set; }
            public string GST_Company_Name { get; set; }
            public string GST_Company_Address { get; set; }
            public string GST_PhoneNo { get; set; }
            public string GST_Email { get; set; }
            public string GSTRemark { get; set; }
            public string SearchId { get; set; }
            public string PNRId { get; set; }
            public string TicketId { get; set; }
            public string HoldCharge { get; set; }
            public string IsHoldByAgent { get; set; }
            public string HoldDateTime { get; set; }
            public string CreditNode { get; set; }
            public string DebitNode { get; set; }
            public string GSTDIFF { get; set; }
            public string GstUpdateDateTime { get; set; }
            public string BookedByStaff { get; set; }
            public string StaffId { get; set; }
            public string CouponCode { get; set; }
            public List<FlightHeaderModel> FltHeaderList { get; set; }
            public List<PaxDetailModel> PaxList { get; set; }
            public List<FltDetailsModel> FltDetailsList { get; set; }
            public List<SelectedFlightDetailsGalModel> SelectedFlightDetailsList { get; set; }
            public List<BagInfomartionModel> BagInfomartionList { get; set; }
            public List<AgencyModel> AgencyList { get; set; }
            public List<FltFareDetailsModel> FltFareDetailList { get; set; }
            public List<T_Flt_Meal_And_Baggage_RequestModel> MealAndBaggageList { get; set; }
        }

        public class PaxDetailModel
        {
            public int PaxId { get; set; }
            public string OrderId { get; set; }
            public string Name { get; set; }
            public string PaxType { get; set; }
            public string TicketNumber { get; set; }
            public string DOB { get; set; }
            public string FFNumber { get; set; }
            public string FFAirline { get; set; }
            public string MealType { get; set; }
            public string SeatType { get; set; }
        }

        public class FltDetailsModel
        {
            public int FltId { get; set; }
            public string OrderId { get; set; }
            public string DFrom { get; set; }
            public string DepAirName { get; set; }
            public string ATo { get; set; }
            public string ArrAirName { get; set; }
            public string DepDate { get; set; }
            public string DepTime { get; set; }
            public string ArrDate { get; set; }
            public string ArrTime { get; set; }
            public string AirlineCode { get; set; }
            public string AirlineName { get; set; }
            public string FltNumber { get; set; }
            public string AirCraft { get; set; }
            public string CreateDate { get; set; }
            public string AdtRbd { get; set; }
            public string ChdRbd { get; set; }

            public string LogoUrl { get; set; }

        }
        public class SelectedFlightDetailsGalModel
        {
            public string OrgDestFrom { get; set; }
            public string OrgDestTo { get; set; }
            public string DepartureLocation { get; set; }
            public string DepartureCityName { get; set; }
            public string DepAirportCode { get; set; }
            public string DepartureTerminal { get; set; }
            public string ArrivalLocation { get; set; }
            public string ArrivalCityName { get; set; }
            public string ArrAirportCode { get; set; }
            public string ArrivalTerminal { get; set; }
            public string DepartureDate { get; set; }
            public string Departure_Date { get; set; }
            public string DepartureTime { get; set; }
            public string ArrivalDate { get; set; }
            public string Arrival_Date { get; set; }
            public string ArrivalTime { get; set; }
            public string Adult { get; set; }
            public string Child { get; set; }
            public string Infant { get; set; }
            public string TotPax { get; set; }
            public string MarketingCarrier { get; set; }
            public string OperatingCarrier { get; set; }
            public string FlightIdentification { get; set; }
            public string ValiDatingCarrier { get; set; }
            public string AirLineName { get; set; }
            public string AvailableSeats { get; set; }
            public string AdtCabin { get; set; }
            public string ChdCabin { get; set; }
            public string InfCabin { get; set; }
            public string AdtRbd { get; set; }
            public string ChdRbd { get; set; }
            public string InfRbd { get; set; }
            public string RBD { get; set; }
            public string AdtFare { get; set; }
            public string AdtBfare { get; set; }
            public string AdtTax { get; set; }
            public string ChdFare { get; set; }
            public string ChdBfare { get; set; }
            public string ChdTax { get; set; }
            public string InfFare { get; set; }
            public string InfBfare { get; set; }
            public string InfTax { get; set; }
            public string TotalBfare { get; set; }
            public string TotFare { get; set; }
            public string TotalTax { get; set; }
            public string NetFare { get; set; }
            public string STax { get; set; }
            public string TFee { get; set; }
            public string DisCount { get; set; }
            public string Searchvalue { get; set; }
            public string LineItemNumber { get; set; }
            public string Leg { get; set; }
            public string Flight { get; set; }
            public string Provider { get; set; }
            public string Tot_Dur { get; set; }
            public string TripType { get; set; }
            public string EQ { get; set; }
            public string Stops { get; set; }
            public string Trip { get; set; }
            public string Sector { get; set; }
            public string TripCnt { get; set; }
            public string Currency { get; set; }
            public string ADTAdminMrk { get; set; }
            public string ADTAgentMrk { get; set; }
            public string CHDAdminMrk { get; set; }
            public string CHDAgentMrk { get; set; }
            public string InfAdminMrk { get; set; }
            public string InfAgentMrk { get; set; }
            public string IATAComm { get; set; }
            public string AdtFareType { get; set; }
            public string AdtFarebasis { get; set; }
            public string ChdFareType { get; set; }
            public string ChdFarebasis { get; set; }
            public string InfFareType { get; set; }
            public string InfFarebasis { get; set; }
            public string FareBasis { get; set; }
            public string FBPaxType { get; set; }
            public string AdtFSur { get; set; }
            public string ChdFSur { get; set; }
            public string InfFSur { get; set; }
            public string TotalFuelSur { get; set; }
            public string sno { get; set; }
            public string depdatelcc { get; set; }
            public string arrdatelcc { get; set; }
            public string OriginalTF { get; set; }
            public string OriginalTT { get; set; }
            public string Track_id { get; set; }
            public string FlightStatus { get; set; }
            public string Adt_Tax { get; set; }
            public string AdtOT { get; set; }
            public string AdtSrvTax { get; set; }
            public string Chd_Tax { get; set; }
            public string ChdOT { get; set; }
            public string ChdSrvTax { get; set; }
            public string Inf_Tax { get; set; }
            public string InfOT { get; set; }
            public string InfSrvTax { get; set; }
            public string SrvTax { get; set; }
            public string TF { get; set; }
            public string TC { get; set; }
            public string AdtTds { get; set; }
            public string ChdTds { get; set; }
            public string InfTds { get; set; }
            public string AdtComm { get; set; }
            public string ChdComm { get; set; }
            public string InfComm { get; set; }
            public string AdtCB { get; set; }
            public string ChdCB { get; set; }
            public string InfCB { get; set; }
            public string ElectronicTicketing { get; set; }
            public string User_id { get; set; }
            public string AdtMgtFee { get; set; }
            public string ChdMgtFee { get; set; }
            public string InfMgtFee { get; set; }
            public string TotMgtFee { get; set; }
            public string IsCorp { get; set; }
            public string CreatedDate { get; set; }
            public string AdtComm1 { get; set; }
            public string ChdComm1 { get; set; }
            public string AdtSrvTax1 { get; set; }
            public string ChdSrvTax1 { get; set; }
            public string IsMobile { get; set; }
            public string RESULTTYPE { get; set; }
            public string OldTrack_Id { get; set; }
            public string AirlineRemark { get; set; }
            public string SearchId { get; set; }
            public string PNRId { get; set; }
            public string TicketId { get; set; }
            public string IsBagFare { get; set; }
            public string SSRCode { get; set; }
            public string ADTSTOCKISTMRK { get; set; }
            public string CHDSTOCKISTMRK { get; set; }
            public string INFTOCKISTMRK { get; set; }
            public string STADTTDS { get; set; }
            public string STCHDTDS { get; set; }
            public string STINFTDS { get; set; }
            public string STADTCOMM { get; set; }
            public string STINFCOMM { get; set; }
            public string STADTCB { get; set; }
            public string STCHDCB { get; set; }
            public string STINFCB { get; set; }
            public string STADTCOMM1 { get; set; }
            public string STCHDCOMM1 { get; set; }
            public string STIATACOMM { get; set; }
            public string FamilyFare { get; set; }
            public string IsSMEFare { get; set; }
            public string STChdComm { get; set; }
        }

        public class BagInfomartionModel
        {
            public string BaggageName { get; set; }
            public string Weight { get; set; }
            public string Trip { get; set; }
        }

        public class AgencyModel
        {
            public string Agency_Name { get; set; }
            public string Email { get; set; }
            public string Mobile { get; set; }

            public string Address { get; set; }
            public string Address1 { get; set; }
            public string IsCorp { get; set; }
        }

        public class FltFareDetailsModel
        {
            public string PaxType { get; set; }
            public string BaseFare { get; set; }
            public string Fuel { get; set; }
            public string Tax { get; set; }
            public string ServiceTax { get; set; }
            public string TFee { get; set; }
            public string TCharge { get; set; }
            public string Total { get; set; }
            public string TotalAfterDis { get; set; }
            public string MgtFee { get; set; }
            public string FareType { get; set; }
            public string ticketcopymarkupforTAX { get; set; }
            public string ticketcopymarkupforTC { get; set; }
        }
        public class T_Flt_Meal_And_Baggage_RequestModel
        {
            public string Title { get; set; }
            public string FName { get; set; }
            public string MName { get; set; }
            public string LName { get; set; }
            public string MealPrice { get; set; }
            public string TotalPrice { get; set; }
        }
    }
}
