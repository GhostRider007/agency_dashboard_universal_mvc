﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace ModelLibrary
{
    public class WalletModel
    {
        public class UploadModel
        {
            public int Counter { get; set; }

            public int Totalcount { get; set; }
            public string flowType { get; set; }
            public string AgencyName { get; set; }
            public string CreatedDate { get; set; }
            public string UpdatedRemark { get; set; }
            public string AgencyID { get; set; }
            public string Amount { get; set; }
            public string ModeOfPayment { get; set; }
            public string BankName { get; set; }
            public string BranchName { get; set; }
            public string AccountNo { get; set; }
            public string ChequeNo { get; set; }
            public string ChequeDate { get; set; }
            public string TransactionID { get; set; }
            public string BankAreaCode { get; set; }
            public string DepositCity { get; set; }
            public string DepositeDate { get; set; }
            public string Remark { get; set; }
            public string RemarkByAccounts { get; set; }
            public string Status { get; set; }
            public string AccountID { get; set; }
            public string SalesExecID { get; set; }
            public string UpdatedDateTime { get; set; }
            public string Date { get; set; }
            public string UploadType { get; set; }
            public string DepositeOffice { get; set; }
            public string ConcernPerson { get; set; }
            public string RecieptNo { get; set; }
            public string BranchCode { get; set; }
            public string AgentBankName { get; set; }
            public string AgentType { get; set; }
            public string AcceptedDateTime { get; set; }
            public string DISTRID { get; set; }
            public string InvoiceNo { get; set; }
            public string IsMobile { get; set; }
            public string OTPRefNo { get; set; }
            public string PaymentType { get; set; }
            public string SearchType { get; set; }
            public string PaymentMode { get; set; }
            public string Loginid { get; set; }
            public string UserType { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public List<UploadModel> UploadDetialsList { get; set; }
        }

        public class UploadModelFilter
        {
            public string AgencyName { get; set; }
            public string Loginid { get; set; }
            public string UserType { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string Status { get; set; }
            public string PaymentType { get; set; }
            public string PaymentMode { get; set; }
            public string AgentID { get; set; }

            public string UploadType { get; set; }
            public string flowType { get; set; }
            public string SearchType { get; set; }
        }

        public class BankModel
        {
            public int Counter { get; set; }
            public int Totalcount { get; set; }

            public string Agentid { get; set; }
            public string Operation { get; set; }
            public string BankName { get; set; }
            public string BranchName { get; set; }
            public string Area { get; set; }
            public string AccountNumber { get; set; }
            public string NEFTCode { get; set; }
            public string CreatedDate { get; set; }
            public string DISTRID { get; set; }
            public List<BankModel> Banklist { get; set; }

        }

        public class UploadRequest
        {
            public bool IsShowReqType { get; set; }
            public string UploadeRequestType { get; set; }
            public string Amount { get; set; }

            public string Status { get; set; }
            public string BankAreaCode { get; set; }
            public string PaymentMode { get; set; }
            public string ChequeDate { get; set; }
            public string ChequeNumber { get; set; }
            public string BankName { get; set; }

            public string AgentBankName { get; set; }

            public string SalesExecID { get; set; }
            public string AgentType { get; set; }

            public string OTPRefNo { get; set; }
            public string LoginByOTP { get; set; }

            public string OTPId { get; set; }
            public string DepositeBank { get; set; }
            public List<SelectListItem> DepositeBankList { get; set; }
            public string DepositeOffice { get; set; }
            public List<SelectListItem> DepositeOfficeList { get; set; }
            public string DepositeBranch { get; set; }
            public List<SelectListItem> DepositeBranchList { get; set; }
            public string DepositeAccountNo { get; set; }
            public List<SelectListItem> DepositeAccountNoList { get; set; }
            //public string BankAreaCode { get; set; }
            public string TransactionID { get; set; }
            public string BranchCode { get; set; }
            public string DepositCity { get; set; }
            public string DepositDate { get; set; }
            public string ConcernPerson { get; set; }
            public string ReceiptNumber { get; set; }
            public string ReferenceNumber { get; set; }
            public string Remark { get; set; }

            public string AgencyName { get; set; }
            public string AgencyID { get; set; }
        }

        public class UploadBankInformation
        {
            public string BankName { get; set; }
            public string BranchName { get; set; }
            public string Area { get; set; }
            public string AccountNumber { get; set; }
            public string NEFTCode { get; set; }
        }
    }
}
