﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelLibrary
{
    public class SettingModel
    {
        public class MarkupModel
        {
            public int Counter { get; set; }
            public string user_id { get; set; }
            public string airline { get; set; }
            public string airlinecode { get; set; }
            public string Markup { get; set; }
            public string markup_type { get; set; }
            public string createddate { get; set; }
            public string updatedate { get; set; }
            public string updateby { get; set; }

            public string CmdType { get; set; }
            public int Totalcount { get; set; }
            public List<MarkupModel> DomMarkupList { get; set; }
        }

        public class StaffModel
        {
            public int Id { get; set; }

            public int Totalcount { get; set; }
            public string OrderId { get; set; }
            public string ModuleType { get; set; }
            public string Remarks { get; set; }
            public string Amount { get; set; }

            public string TransType { get; set; }
            public string StaffId { get; set; }
            public string ActionType { get; set; }
            public string UserId { get; set; }
            public string Password { get; set; }
            public string OwnerId { get; set; }
            public string AgencyId { get; set; }
            public string Name { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }
            public string Address { get; set; }
            public string CreditLimit { get; set; }
            public string CreditLimitTrnsDate { get; set; }
            public string StaffLimit { get; set; }
            public string StaffLimitTrnsDate { get; set; }
            public string UserType { get; set; }
            public string RoleType { get; set; }
            public bool Status { get; set; }
            public bool Flight { get; set; }
            public bool Hotel { get; set; }
            public bool Bus { get; set; }
            public bool Rail { get; set; }
            public bool Cab { get; set; }

            public string sStatus { get; set; }
            public string sFlight { get; set; }
            public string sHotel { get; set; }
            public string sBus { get; set; }
            public string sRail { get; set; }
            public string sCab { get; set; }
            public string Holidays { get; set; }
            public string GiftCard { get; set; }
            public string HomeStay { get; set; }
            public string CreatedDate { get; set; }
            public string CreatedBy { get; set; }
            public string UpdatedDate { get; set; }
            public string UpdatedBy { get; set; }
            public string DelStatus { get; set; }
            public string DeletedBy { get; set; }
            public string DeletedDate { get; set; }
            public string CheckBalance { get; set; }
            public List<StaffModel> StaffList { get; set; }
        }
    }
}
