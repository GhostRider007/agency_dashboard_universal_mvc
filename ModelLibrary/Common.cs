﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace ModelLibrary
{
    public class Common
    {
        public class CityAutoSearch
        {
            public string AirportCode { get; set; }
            public string AirportName { get; set; }
            public string CityName { get; set; }
            public string CountryName { get; set; }
            public string CountryCode { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
            public string WorldAreaCode { get; set; }
            public string Counter { get; set; }
            public string IsOrder { get; set; }
            public string IsActive { get; set; }
        }

        public class AirLineModel
        {
            public string Counter { get; set; }
            public string AL_Code { get; set; }
            public string AL_Name { get; set; }
            public string CreatedDate { get; set; }
        }
    }
}
