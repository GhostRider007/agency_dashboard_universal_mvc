﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using static ModelLibrary.SettingModel;

namespace DataBaseLibrary.Setting
{
    public static class SettingDatabase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static DataTable GetDomMarkupDetials(MarkupModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_AgentMarkup", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@cmd", (!string.IsNullOrEmpty(model.CmdType) ? model.CmdType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@userid", (!string.IsNullOrEmpty(model.user_id) ? model.user_id : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetAgentMarkup");
                dataTable = dataSet.Tables["GetAgentMarkup"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static bool InsertDomMarkup(MarkupModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_AgentMarkup", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@cmd", "check"));
                sqlCommand.Parameters.Add(new SqlParameter("@userid", (!string.IsNullOrEmpty(model.user_id) ? model.user_id : string.Empty)));
                sqlCommand.Parameters.AddWithValue("@airline", model.airline);
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "CheckAgentMarkup");
                dataTable = dataSet.Tables["CheckAgentMarkup"];
                if (dataTable.Rows.Count > 0)
                {

                }
                else
                {
                    sqlCommand = new SqlCommand("USP_AgentMarkup", ConnectToDataBase.MyAmdDBConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@cmd", "insert");
                    sqlCommand.Parameters.AddWithValue("@userid", model.user_id);
                    sqlCommand.Parameters.AddWithValue("@airline", model.airline.ToUpper());
                    sqlCommand.Parameters.AddWithValue("@markup", model.Markup.Trim());
                    sqlCommand.Parameters.AddWithValue("@markuptype", model.markup_type.Trim());
                    sqlCommand.Parameters.AddWithValue("@updatedby", model.user_id);
                    ConnectToDataBase.MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    ConnectToDataBase.MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool UpdateDomMarkup(MarkupModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_AgentMarkup", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@cmd", "check"));
                sqlCommand.Parameters.Add(new SqlParameter("@userid", (!string.IsNullOrEmpty(model.user_id) ? model.user_id : string.Empty)));
                sqlCommand.Parameters.AddWithValue("@airline", model.airline);
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "CheckAgentMarkup");
                dataTable = dataSet.Tables["CheckAgentMarkup"];
                if (dataTable.Rows.Count > 0)
                {

                }
                else
                {
                    sqlCommand = new SqlCommand("USP_AgentMarkup", ConnectToDataBase.MyAmdDBConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@cmd", "update");
                    sqlCommand.Parameters.AddWithValue("@markup", model.Markup);
                    sqlCommand.Parameters.AddWithValue("@markuptype", model.markup_type);
                    sqlCommand.Parameters.AddWithValue("@cat_id", model.Counter);
                    sqlCommand.Parameters.AddWithValue("@updatedby", model.user_id);
                    ConnectToDataBase.MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    ConnectToDataBase.MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool DeleteDomMarkupBycounter(string counter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_AgentMarkup", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@cmd", "delete");
                sqlCommand.Parameters.AddWithValue("@cat_id", Convert.ToInt64(counter));
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        //International
        public static DataTable GetIntMarkupDetials(MarkupModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_AgentMarkupIntl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@cmd", (!string.IsNullOrEmpty(model.CmdType) ? model.CmdType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@userid", (!string.IsNullOrEmpty(model.user_id) ? model.user_id : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetAgentMarkupint");
                dataTable = dataSet.Tables["GetAgentMarkupint"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static bool InsertIntMarkup(MarkupModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_AgentMarkupIntl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@cmd", "check"));
                sqlCommand.Parameters.Add(new SqlParameter("@userid", (!string.IsNullOrEmpty(model.user_id) ? model.user_id : string.Empty)));
                sqlCommand.Parameters.AddWithValue("@airline", model.airlinecode);
                sqlCommand.Parameters.AddWithValue("@trip", "I");
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "CheckAgentMarkupInt");
                dataTable = dataSet.Tables["CheckAgentMarkupInt"];
                if (dataTable.Rows.Count > 0)
                {

                }
                else
                {
                    sqlCommand = new SqlCommand("USP_AgentMarkupIntl", ConnectToDataBase.MyAmdDBConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@cmd", "insert");
                    sqlCommand.Parameters.AddWithValue("@userid", model.user_id);
                    sqlCommand.Parameters.AddWithValue("@airline", model.airline.ToUpper());
                    sqlCommand.Parameters.AddWithValue("@airlinecode", model.airlinecode.ToUpper());
                    sqlCommand.Parameters.AddWithValue("@markup", model.Markup.Trim());
                    sqlCommand.Parameters.AddWithValue("@markuptype", model.markup_type.Trim());
                    sqlCommand.Parameters.AddWithValue("@updatedby", model.user_id);
                    sqlCommand.Parameters.AddWithValue("@trip", "I");
                    ConnectToDataBase.MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    ConnectToDataBase.MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool UpdateIntMarkup(MarkupModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_AgentMarkupIntl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@cmd", "update");
                sqlCommand.Parameters.AddWithValue("@markup", model.Markup);
                sqlCommand.Parameters.AddWithValue("@markuptype", model.markup_type);
                sqlCommand.Parameters.AddWithValue("@cat_id", model.Counter);
                sqlCommand.Parameters.AddWithValue("@updatedby", model.user_id);
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool DeleteIntMarkupBycounter(string counter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_AgentMarkupIntl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@cmd", "delete");
                sqlCommand.Parameters.AddWithValue("@cat_id", Convert.ToInt64(counter));
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool InsertAgentStaff(StaffModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("Sp_AgentStaffMaster", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Id", model.Id);
                sqlCommand.Parameters.AddWithValue("@UserId", model.Email);
                sqlCommand.Parameters.AddWithValue("@Password", model.Password);
                sqlCommand.Parameters.AddWithValue("@OwnerId", model.OwnerId);
                sqlCommand.Parameters.AddWithValue("@AgencyId", model.AgencyId);
                sqlCommand.Parameters.AddWithValue("@Name", model.Name);
                sqlCommand.Parameters.AddWithValue("@Mobile", model.Mobile);
                sqlCommand.Parameters.AddWithValue("@Email", model.Email);
                sqlCommand.Parameters.AddWithValue("@Address", model.Address);
                sqlCommand.Parameters.AddWithValue("@UserType", model.UserType);
                sqlCommand.Parameters.AddWithValue("@RoleType", model.RoleType);
                sqlCommand.Parameters.AddWithValue("@Status", model.Status);
                sqlCommand.Parameters.AddWithValue("@Flight", model.Flight);
                sqlCommand.Parameters.AddWithValue("@Hotel", model.Hotel);
                sqlCommand.Parameters.AddWithValue("@Bus", model.Bus);
                sqlCommand.Parameters.AddWithValue("@Rail", model.Rail);
                sqlCommand.Parameters.AddWithValue("@Cab", model.Cab);
                sqlCommand.Parameters.AddWithValue("@Holidays", model.Holidays);
                sqlCommand.Parameters.AddWithValue("@GiftCard", model.GiftCard);
                sqlCommand.Parameters.AddWithValue("@HomeStay", model.HomeStay);
                sqlCommand.Parameters.AddWithValue("@CheckBalance", Convert.ToBoolean(model.CheckBalance));
                sqlCommand.Parameters.AddWithValue("@CreatedBy", model.AgencyId);
                sqlCommand.Parameters.AddWithValue("@ActionType", model.ActionType);
                sqlCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                sqlCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();
                string msgout = sqlCommand.Parameters["@Msg"].Value.ToString();
                if (isSuccess > 0)
                {
                    return true;
                }     
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static DataTable GetStaffList(StaffModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("Sp_AgentStaffMaster", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@OwnerId", (!string.IsNullOrEmpty(model.CreatedBy) ? model.CreatedBy : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@CreatedBy", (!string.IsNullOrEmpty(model.CreatedBy) ? model.CreatedBy : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ActionType", "select"));
                sqlCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                sqlCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetAgentStafflist");
                dataTable = dataSet.Tables["GetAgentStafflist"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static bool StaffTransaction(StaffModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_StaffAmountDebitCredit", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@OrderId", model.OrderId);
                sqlCommand.Parameters.AddWithValue("@ServiceType", "UPLOAD");
                sqlCommand.Parameters.AddWithValue("@TransAmount", Convert.ToInt32(model.Amount.Trim()));
                sqlCommand.Parameters.AddWithValue("@StaffUserId", model.UserId);
                sqlCommand.Parameters.AddWithValue("@OwnerId", model.OwnerId);
                sqlCommand.Parameters.AddWithValue("@IPAddress", "#:1");
                sqlCommand.Parameters.AddWithValue("@Remark", model.Remarks);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", null);
                sqlCommand.Parameters.AddWithValue("@DebitCredit", model.TransType.ToUpper());
                sqlCommand.Parameters.AddWithValue("@Module", model.ModuleType);
                sqlCommand.Parameters.AddWithValue("@ActionType", "DEBITCREDIT");               
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();               
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
    }
}
