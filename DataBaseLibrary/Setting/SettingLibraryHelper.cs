﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using static ModelLibrary.SettingModel;

namespace DataBaseLibrary.Setting
{
    public static class SettingLibraryHelper
    {
        public static List<MarkupModel> GetDomMarkupDetials(MarkupModel model)
        {
            List<MarkupModel> result = new List<MarkupModel>();
            try
            {
                DataTable dtmarkupdetails = SettingDatabase.GetDomMarkupDetials(model);

                if (dtmarkupdetails != null && dtmarkupdetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtmarkupdetails.Rows.Count; i++)
                    {
                        MarkupModel tempmodel = new MarkupModel();
                        tempmodel.Counter = Convert.ToInt32(dtmarkupdetails.Rows[i]["Counter"].ToString());
                        tempmodel.markup_type = !string.IsNullOrEmpty(dtmarkupdetails.Rows[i]["markup_type"].ToString()) ? dtmarkupdetails.Rows[i]["markup_type"].ToString() : string.Empty;
                        tempmodel.Markup = !string.IsNullOrEmpty(dtmarkupdetails.Rows[i]["Markup"].ToString()) ? dtmarkupdetails.Rows[i]["Markup"].ToString() : string.Empty;
                        tempmodel.airline = !string.IsNullOrEmpty(dtmarkupdetails.Rows[i]["airline"].ToString()) ? dtmarkupdetails.Rows[i]["airline"].ToString() : string.Empty;
                        tempmodel.user_id = !string.IsNullOrEmpty(dtmarkupdetails.Rows[i]["user_id"].ToString()) ? dtmarkupdetails.Rows[i]["user_id"].ToString() : string.Empty;                       
                        result.Add(tempmodel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }

        public static bool InsertDomMarkup(MarkupModel model)
        {
            return SettingDatabase.InsertDomMarkup(model);
        }
        public static bool UpdateDomMarkup(MarkupModel model)
        {
            return SettingDatabase.UpdateDomMarkup(model);
        }
        public static bool DeleteDomMarkupBycounter(string counter)
        {
            return SettingDatabase.DeleteDomMarkupBycounter(counter);
        }
        //International
        public static List<MarkupModel> GetIntMarkupDetials(MarkupModel model)
        {
            List<MarkupModel> result = new List<MarkupModel>();
            try
            {
                DataTable dtmarkupdetails = SettingDatabase.GetIntMarkupDetials(model);

                if (dtmarkupdetails != null && dtmarkupdetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtmarkupdetails.Rows.Count; i++)
                    {
                        MarkupModel tempmodel = new MarkupModel();
                        tempmodel.Counter = Convert.ToInt32(dtmarkupdetails.Rows[i]["Counter"].ToString());
                        tempmodel.markup_type = !string.IsNullOrEmpty(dtmarkupdetails.Rows[i]["MarkupType"].ToString()) ? dtmarkupdetails.Rows[i]["MarkupType"].ToString() : string.Empty;
                        tempmodel.Markup = !string.IsNullOrEmpty(dtmarkupdetails.Rows[i]["MarkupValue"].ToString()) ? dtmarkupdetails.Rows[i]["MarkupValue"].ToString() : string.Empty;
                        tempmodel.airline = !string.IsNullOrEmpty(dtmarkupdetails.Rows[i]["AirlineName"].ToString()) ? dtmarkupdetails.Rows[i]["AirlineName"].ToString() : string.Empty;
                        tempmodel.user_id = !string.IsNullOrEmpty(dtmarkupdetails.Rows[i]["Userid"].ToString()) ? dtmarkupdetails.Rows[i]["Userid"].ToString() : string.Empty;
                        result.Add(tempmodel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }

        public static bool InsertIntMarkup(MarkupModel model)
        {
            return SettingDatabase.InsertIntMarkup(model);
        }
        public static bool UpdateIntMarkup(MarkupModel model)
        {
            return SettingDatabase.UpdateIntMarkup(model);
        }
        public static bool DeleteIntMarkupBycounter(string counter)
        {
            return SettingDatabase.DeleteIntMarkupBycounter(counter);
        }
        public static bool InsertAgentStaff(StaffModel model)
        {
            return SettingDatabase.InsertAgentStaff(model);
        }
        public static List<StaffModel> GetStaffList(StaffModel model)
        {
            List<StaffModel> result = new List<StaffModel>();
            try
            {
                DataTable dtstaffdetails = SettingDatabase.GetStaffList(model);

                if (dtstaffdetails != null && dtstaffdetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtstaffdetails.Rows.Count; i++)
                    {
                        StaffModel tempmodel = new StaffModel();
                        tempmodel.Id = Convert.ToInt32(dtstaffdetails.Rows[i]["Id"].ToString());
                        tempmodel.Status = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Status"].ToString()) ?Convert.ToBoolean(dtstaffdetails.Rows[i]["Status"]) :false;
                        tempmodel.Flight = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Flight"].ToString()) ? Convert.ToBoolean(dtstaffdetails.Rows[i]["Flight"]) : false;
                        tempmodel.Hotel = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Hotel"].ToString()) ? Convert.ToBoolean(dtstaffdetails.Rows[i]["Hotel"]) : false;
                        tempmodel.Rail = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Rail"].ToString()) ? Convert.ToBoolean(dtstaffdetails.Rows[i]["Rail"]) : false;
                        tempmodel.Cab = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Cab"].ToString()) ? Convert.ToBoolean(dtstaffdetails.Rows[i]["Cab"]) : false;
                        tempmodel.Bus = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Bus"].ToString()) ? Convert.ToBoolean(dtstaffdetails.Rows[i]["Bus"]) : false;
                        tempmodel.sStatus = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["sStatus"].ToString()) ? dtstaffdetails.Rows[i]["sStatus"].ToString() : string.Empty;
                        tempmodel.sFlight = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["sFlight"].ToString()) ? dtstaffdetails.Rows[i]["sFlight"].ToString() : string.Empty;
                        tempmodel.sHotel = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["sHotel"].ToString()) ? dtstaffdetails.Rows[i]["sHotel"].ToString() : string.Empty;
                        tempmodel.sRail = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["sRail"].ToString()) ? dtstaffdetails.Rows[i]["sRail"].ToString() : string.Empty;
                        tempmodel.sCab = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["sCab"].ToString()) ? dtstaffdetails.Rows[i]["sCab"].ToString() : string.Empty;
                        tempmodel.sBus = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["sBus"].ToString()) ? dtstaffdetails.Rows[i]["sBus"].ToString() : string.Empty;
                        tempmodel.Holidays = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["sHolidays"].ToString()) ? dtstaffdetails.Rows[i]["sHolidays"].ToString() : string.Empty;
                        tempmodel.GiftCard = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["sGiftCard"].ToString()) ? dtstaffdetails.Rows[i]["sGiftCard"].ToString() : string.Empty;
                        tempmodel.HomeStay = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["sHomeStay"].ToString()) ? dtstaffdetails.Rows[i]["sHomeStay"].ToString() : string.Empty;
                        tempmodel.CheckBalance = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["CheckBalance"].ToString()) ? dtstaffdetails.Rows[i]["CheckBalance"].ToString() : string.Empty;
                        tempmodel.StaffId = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["StaffId"].ToString()) ? dtstaffdetails.Rows[i]["StaffId"].ToString() : string.Empty;
                        tempmodel.UserId = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["UserId"].ToString()) ? dtstaffdetails.Rows[i]["UserId"].ToString() : string.Empty;
                        tempmodel.Password = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Password"].ToString()) ? dtstaffdetails.Rows[i]["Password"].ToString() : string.Empty;
                        tempmodel.OwnerId = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["OwnerId"].ToString()) ? dtstaffdetails.Rows[i]["OwnerId"].ToString() : string.Empty;
                        tempmodel.AgencyId = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["AgencyId"].ToString()) ? dtstaffdetails.Rows[i]["AgencyId"].ToString() : string.Empty;
                        tempmodel.Name = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Name"].ToString()) ? dtstaffdetails.Rows[i]["Name"].ToString() : string.Empty;
                        tempmodel.Mobile = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Mobile"].ToString()) ? dtstaffdetails.Rows[i]["Mobile"].ToString() : string.Empty;
                        tempmodel.Email = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Email"].ToString()) ? dtstaffdetails.Rows[i]["Email"].ToString() : string.Empty;
                        tempmodel.Address = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["Address"].ToString()) ? dtstaffdetails.Rows[i]["Address"].ToString() : string.Empty;
                        tempmodel.CreditLimit = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["CreditLimit"].ToString()) ? dtstaffdetails.Rows[i]["CreditLimit"].ToString() : string.Empty;
                        tempmodel.CreditLimitTrnsDate = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["CreditLimitTrnsDate"].ToString()) ? dtstaffdetails.Rows[i]["CreditLimitTrnsDate"].ToString() : string.Empty;
                        tempmodel.StaffLimit = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["StaffLimit"].ToString()) ? dtstaffdetails.Rows[i]["StaffLimit"].ToString() : string.Empty;
                        tempmodel.StaffLimitTrnsDate = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["StaffLimitTrnsDate"].ToString()) ? dtstaffdetails.Rows[i]["StaffLimitTrnsDate"].ToString() : string.Empty;
                        tempmodel.UserType = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["UserType"].ToString()) ? dtstaffdetails.Rows[i]["UserType"].ToString() : string.Empty;
                        tempmodel.RoleType = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["RoleType"].ToString()) ? dtstaffdetails.Rows[i]["RoleType"].ToString() : string.Empty;
                        tempmodel.CreatedDate = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["CreatedDate"].ToString()) ? dtstaffdetails.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        tempmodel.CreatedBy = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["CreatedBy"].ToString()) ? dtstaffdetails.Rows[i]["CreatedBy"].ToString() : string.Empty;
                        tempmodel.CheckBalance = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["CheckBalance"].ToString()) ? dtstaffdetails.Rows[i]["CheckBalance"].ToString() : string.Empty;
                        tempmodel.UpdatedBy = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["UpdatedBy"].ToString()) ? dtstaffdetails.Rows[i]["UpdatedBy"].ToString() : string.Empty;
                        tempmodel.UpdatedDate = !string.IsNullOrEmpty(dtstaffdetails.Rows[i]["UpdatedDate"].ToString()) ? dtstaffdetails.Rows[i]["UpdatedDate"].ToString() : string.Empty;
                        result.Add(tempmodel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }

        public static bool StaffTransaction(StaffModel model)
        {
            return SettingDatabase.StaffTransaction(model);
        }
    }
}
