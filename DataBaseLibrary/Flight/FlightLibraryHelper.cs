﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml.XPath;
using static ModelLibrary.Common;
using static ModelLibrary.FlightModel;

namespace DataBaseLibrary.Flight
{
    public static class FlightLibraryHelper
    {
        public static List<FlightTicketReport> GetFlightTicketReports(FlightTicketFilter filter)
        {
            List<FlightTicketReport> result = new List<FlightTicketReport>();
            try
            {
                DataTable dtFlightTicket = FlightDataBase.GetTicektReport(filter);

                if (dtFlightTicket != null && dtFlightTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightTicket.Rows.Count; i++)
                    {
                        FlightTicketReport model = new FlightTicketReport();
                        model.OrderId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["OrderId"].ToString()) ? dtFlightTicket.Rows[i]["OrderId"].ToString() : string.Empty;
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) : 0;
                        model.TotalBookingCost = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) : 0;
                        model.Sector = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["sector"].ToString()) ? dtFlightTicket.Rows[i]["sector"].ToString() : string.Empty;
                        model.AgentId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentId"].ToString()) ? dtFlightTicket.Rows[i]["AgentId"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.AgentType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentType"].ToString()) ? dtFlightTicket.Rows[i]["AgentType"].ToString() : string.Empty;
                        model.PaxId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaxId"].ToString()) ? Convert.ToInt32(dtFlightTicket.Rows[i]["PaxId"].ToString()) : 0;
                        model.Status = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Status"].ToString()) ? dtFlightTicket.Rows[i]["Status"].ToString() : string.Empty;
                        model.CreateDate = dtFlightTicket.Rows[i]["CreateDate"].ToString();
                        model.VC = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["VC"].ToString()) ? dtFlightTicket.Rows[i]["VC"].ToString() : string.Empty;
                        model.GdsPnr = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["GdsPnr"].ToString()) ? dtFlightTicket.Rows[i]["GdsPnr"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TicketNumber"].ToString()) ? dtFlightTicket.Rows[i]["TicketNumber"].ToString() : string.Empty;
                        model.PaxType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaxType"].ToString()) ? dtFlightTicket.Rows[i]["PaxType"].ToString() : string.Empty;
                        model.PgFName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["FName"].ToString()) ? dtFlightTicket.Rows[i]["FName"].ToString() : string.Empty;
                        model.PgLName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["LName"].ToString()) ? dtFlightTicket.Rows[i]["LName"].ToString() : string.Empty;
                        model.ExecutiveId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["ExecutiveId"].ToString()) ? dtFlightTicket.Rows[i]["ExecutiveId"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Trip"].ToString()) ? dtFlightTicket.Rows[i]["Trip"].ToString() : string.Empty;
                        model.RejectedRemark = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["RejectedRemark"].ToString()) ? dtFlightTicket.Rows[i]["RejectedRemark"].ToString() : string.Empty;
                        model.Provider = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Provider"].ToString()) ? dtFlightTicket.Rows[i]["Provider"].ToString() : string.Empty;
                        model.FareType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["FTYPE"].ToString()) ? dtFlightTicket.Rows[i]["FTYPE"].ToString() : string.Empty;
                        model.PName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PName"].ToString()) ? dtFlightTicket.Rows[i]["PName"].ToString() : string.Empty;
                        model.JourneyDate = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["JourneyDate"].ToString()) ? dtFlightTicket.Rows[i]["JourneyDate"].ToString() : string.Empty;
                        model.PaymentMode = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaymentMode"].ToString()) ? dtFlightTicket.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<FlightTicketReport> GetFlightRefundReports(FlightTicketFilter filter)
        {
            List<FlightTicketReport> result = new List<FlightTicketReport>();
            try
            {
                DataTable dtFlightRefundTicket = FlightDataBase.GetRefundReport(filter);

                if (dtFlightRefundTicket != null && dtFlightRefundTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightRefundTicket.Rows.Count; i++)
                    {
                        FlightTicketReport model = new FlightTicketReport();
                        model.RefundID = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["RefundID"].ToString()) ? dtFlightRefundTicket.Rows[i]["RefundID"].ToString() : string.Empty;
                        model.JourneyDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["departure_date"].ToString()) ? dtFlightRefundTicket.Rows[i]["departure_date"].ToString() : string.Empty;
                        model.CancelStatus = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["CancelStatus"].ToString()) ? dtFlightRefundTicket.Rows[i]["CancelStatus"].ToString() : string.Empty;
                        model.PaymentMode = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PgMode"].ToString()) ? dtFlightRefundTicket.Rows[i]["PgMode"].ToString() : string.Empty;
                        model.Pnr_Locator = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["pnr_locator"].ToString()) ? dtFlightRefundTicket.Rows[i]["pnr_locator"].ToString() : string.Empty;
                        model.Sector = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Sector"].ToString()) ? dtFlightRefundTicket.Rows[i]["Sector"].ToString() : string.Empty;
                        model.CancellationCharge = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["CancellationCharge"].ToString()) ? dtFlightRefundTicket.Rows[i]["CancellationCharge"].ToString() : string.Empty;
                        model.RegardingCancel = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["RegardingCancel"].ToString()) ? dtFlightRefundTicket.Rows[i]["RegardingCancel"].ToString() : string.Empty;
                        model.FlightNo = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["FlightNo"].ToString()) ? dtFlightRefundTicket.Rows[i]["FlightNo"].ToString() : string.Empty;
                        model.Departure = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["departure"].ToString()) ? dtFlightRefundTicket.Rows[i]["departure"].ToString() : string.Empty;
                        model.Destination = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["destination"].ToString()) ? dtFlightRefundTicket.Rows[i]["destination"].ToString() : string.Empty;
                        model.PaxType = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["pax_type"].ToString()) ? dtFlightRefundTicket.Rows[i]["pax_type"].ToString() : string.Empty;
                        model.PgTitle = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Title"].ToString()) ? dtFlightRefundTicket.Rows[i]["Title"].ToString() : string.Empty;
                        model.PgLName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["pax_lname"].ToString()) ? dtFlightRefundTicket.Rows[i]["pax_lname"].ToString() : string.Empty;
                        model.PgFName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["pax_fname"].ToString()) ? dtFlightRefundTicket.Rows[i]["pax_fname"].ToString() : string.Empty;
                        model.CreateDate = dtFlightRefundTicket.Rows[i]["Booking_date"].ToString();
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Agency_Name"].ToString()) ? dtFlightRefundTicket.Rows[i]["Agency_Name"].ToString() : string.Empty;
                        model.StaffId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["UserID"].ToString()) ? dtFlightRefundTicket.Rows[i]["UserID"].ToString() : string.Empty;
                        model.PaxId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PaxID"].ToString()) ? Convert.ToInt32(dtFlightRefundTicket.Rows[i]["PaxID"].ToString()) : 0;
                        model.Status = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Status"].ToString()) ? dtFlightRefundTicket.Rows[i]["Status"].ToString() : string.Empty;
                        model.Block_Issue = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Block_Cancel"].ToString()) ? dtFlightRefundTicket.Rows[i]["Block_Cancel"].ToString() : string.Empty;
                        model.Base_Fare = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Base_Fare"].ToString()) ? dtFlightRefundTicket.Rows[i]["Base_Fare"].ToString() : string.Empty;
                        model.Tax = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Tax"].ToString()) ? dtFlightRefundTicket.Rows[i]["Tax"].ToString() : string.Empty;
                        model.YQ = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["YQ"].ToString()) ? dtFlightRefundTicket.Rows[i]["YQ"].ToString() : string.Empty;
                        model.Service_Tax = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Service_Tax"].ToString()) ? dtFlightRefundTicket.Rows[i]["Service_Tax"].ToString() : string.Empty;
                        model.Discount = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Discount"].ToString()) ? dtFlightRefundTicket.Rows[i]["Discount"].ToString() : string.Empty;
                        model.CB = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["CB"].ToString()) ? dtFlightRefundTicket.Rows[i]["CB"].ToString() : string.Empty;
                        model.TDS = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TDS"].ToString()) ? dtFlightRefundTicket.Rows[i]["TDS"].ToString() : string.Empty;
                        model.TotalBookingCost = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TotalFare"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["TotalFare"].ToString()) : 0;
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TotalFareAfterDiscount"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["TotalFareAfterDiscount"].ToString()) : 0;
                        //model.ResuCharge = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["ReissueCharge"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["ReissueCharge"].ToString()) : 0;
                        model.ResuFareDiff = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["RefundFare"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["RefundFare"].ToString()) : 0;
                        model.PGCharges = dtFlightRefundTicket.Rows[i]["PGCharges"].ToString();
                        model.VC = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["VC"].ToString()) ? dtFlightRefundTicket.Rows[i]["VC"].ToString() : string.Empty;
                        model.SubmitDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["SubmitDate"].ToString()) ? dtFlightRefundTicket.Rows[i]["SubmitDate"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Tkt_No"].ToString()) ? dtFlightRefundTicket.Rows[i]["Tkt_No"].ToString() : string.Empty;
                        model.ExecutiveId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["ExecutiveID"].ToString()) ? dtFlightRefundTicket.Rows[i]["ExecutiveID"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Trip"].ToString()) ? dtFlightRefundTicket.Rows[i]["Trip"].ToString() : string.Empty;
                        model.RejectedRemark = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["RejectComment"].ToString()) ? dtFlightRefundTicket.Rows[i]["RejectComment"].ToString() : string.Empty;
                        model.Remark = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["UpdateRemark"].ToString()) ? dtFlightRefundTicket.Rows[i]["UpdateRemark"].ToString() : string.Empty;
                        model.OrderId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["OrderID"].ToString()) ? dtFlightRefundTicket.Rows[i]["OrderID"].ToString() : string.Empty;
                        model.UpdateDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["UpdateDate"].ToString()) ? dtFlightRefundTicket.Rows[i]["UpdateDate"].ToString() : string.Empty;
                        model.AcceptDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["AcceptDate"].ToString()) ? dtFlightRefundTicket.Rows[i]["AcceptDate"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<FlightTicketReport> GetReIssueDetail(FlightTicketFilter filter)
        {
            List<FlightTicketReport> result = new List<FlightTicketReport>();
            try
            {
                DataTable dtFlightRefundTicket = FlightDataBase.GetReIssueDetail(filter);

                if (dtFlightRefundTicket != null && dtFlightRefundTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightRefundTicket.Rows.Count; i++)
                    {
                        FlightTicketReport model = new FlightTicketReport();
                        model.JourneyDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["departure_date"].ToString()) ? dtFlightRefundTicket.Rows[i]["departure_date"].ToString() : string.Empty;
                        model.PaymentMode = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PaymentMode"].ToString()) ? dtFlightRefundTicket.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        model.Pnr_Locator = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["pnr_locator"].ToString()) ? dtFlightRefundTicket.Rows[i]["pnr_locator"].ToString() : string.Empty;
                        model.Sector = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Sector"].ToString()) ? dtFlightRefundTicket.Rows[i]["Sector"].ToString() : string.Empty;
                        model.FlightNo = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["FlightNo"].ToString()) ? dtFlightRefundTicket.Rows[i]["FlightNo"].ToString() : string.Empty;
                        model.Departure = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["departure"].ToString()) ? dtFlightRefundTicket.Rows[i]["departure"].ToString() : string.Empty;
                        model.Destination = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["destination"].ToString()) ? dtFlightRefundTicket.Rows[i]["destination"].ToString() : string.Empty;
                        model.PaxType = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["pax_type"].ToString()) ? dtFlightRefundTicket.Rows[i]["pax_type"].ToString() : string.Empty;
                        model.PgTitle = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Title"].ToString()) ? dtFlightRefundTicket.Rows[i]["Title"].ToString() : string.Empty;
                        model.PgLName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["pax_lname"].ToString()) ? dtFlightRefundTicket.Rows[i]["pax_lname"].ToString() : string.Empty;
                        model.PgFName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["pax_fname"].ToString()) ? dtFlightRefundTicket.Rows[i]["pax_fname"].ToString() : string.Empty;
                        model.CreateDate = dtFlightRefundTicket.Rows[i]["Booking_date"].ToString();
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Agency_Name"].ToString()) ? dtFlightRefundTicket.Rows[i]["Agency_Name"].ToString() : string.Empty;
                        model.StaffId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["UserID"].ToString()) ? dtFlightRefundTicket.Rows[i]["UserID"].ToString() : string.Empty;
                        model.PaxId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PaxID"].ToString()) ? Convert.ToInt32(dtFlightRefundTicket.Rows[i]["PaxID"].ToString()) : 0;
                        model.Status = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Status"].ToString()) ? dtFlightRefundTicket.Rows[i]["Status"].ToString() : string.Empty;
                        model.Block_Issue = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Block_Issue"].ToString()) ? dtFlightRefundTicket.Rows[i]["Block_Issue"].ToString() : string.Empty;
                        model.Base_Fare = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Base_Fare"].ToString()) ? dtFlightRefundTicket.Rows[i]["Base_Fare"].ToString() : string.Empty;
                        model.Tax = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Tax"].ToString()) ? dtFlightRefundTicket.Rows[i]["Tax"].ToString() : string.Empty;
                        model.YQ = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["YQ"].ToString()) ? dtFlightRefundTicket.Rows[i]["YQ"].ToString() : string.Empty;
                        model.Service_Tax = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Service_Tax"].ToString()) ? dtFlightRefundTicket.Rows[i]["Service_Tax"].ToString() : string.Empty;
                        model.Discount = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Discount"].ToString()) ? dtFlightRefundTicket.Rows[i]["Discount"].ToString() : string.Empty;
                        model.CB = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["CB"].ToString()) ? dtFlightRefundTicket.Rows[i]["CB"].ToString() : string.Empty;
                        model.TDS = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TDS"].ToString()) ? dtFlightRefundTicket.Rows[i]["TDS"].ToString() : string.Empty;
                        model.TotalBookingCost = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TotalFare"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["TotalFare"].ToString()) : 0;
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TotalFareAfterDiscount"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["TotalFareAfterDiscount"].ToString()) : 0;
                        model.ResuCharge = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["ReissueCharge"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["ReissueCharge"].ToString()) : 0;
                        model.ResuFareDiff = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["FareDiff"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["FareDiff"].ToString()) : 0;
                        model.PGCharges = dtFlightRefundTicket.Rows[i]["PGCharges"].ToString();
                        model.VC = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["VC"].ToString()) ? dtFlightRefundTicket.Rows[i]["VC"].ToString() : string.Empty;
                        model.SubmitDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["SubmitDate"].ToString()) ? dtFlightRefundTicket.Rows[i]["SubmitDate"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Tkt_No"].ToString()) ? dtFlightRefundTicket.Rows[i]["Tkt_No"].ToString() : string.Empty;
                        model.ExecutiveId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["ExecutiveID"].ToString()) ? dtFlightRefundTicket.Rows[i]["ExecutiveID"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Trip"].ToString()) ? dtFlightRefundTicket.Rows[i]["Trip"].ToString() : string.Empty;
                        model.RejectedRemark = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["RejectionComment"].ToString()) ? dtFlightRefundTicket.Rows[i]["RejectionComment"].ToString() : string.Empty;
                        model.Remark = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Remark"].ToString()) ? dtFlightRefundTicket.Rows[i]["Remark"].ToString() : string.Empty;
                        model.OrderId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["OrderID"].ToString()) ? dtFlightRefundTicket.Rows[i]["OrderID"].ToString() : string.Empty;
                        model.UpdateDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["UpdateDate"].ToString()) ? dtFlightRefundTicket.Rows[i]["UpdateDate"].ToString() : string.Empty;
                        model.AcceptDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["AcceptDate"].ToString()) ? dtFlightRefundTicket.Rows[i]["AcceptDate"].ToString() : string.Empty;

                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<OfflineRequest> GetProxyBookingReport(FlightTicketFilter filter)
        {
            List<OfflineRequest> result = new List<OfflineRequest>();
            try
            {
                DataTable OflineRequestList = FlightDataBase.GetProxyBookingReport(filter);

                if (OflineRequestList != null && OflineRequestList.Rows.Count > 0)
                {
                    for (int i = 0; i < OflineRequestList.Rows.Count; i++)
                    {
                        OfflineRequest model = new OfflineRequest();
                        model.PaymentMode = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["PaymentMode"].ToString()) ? OflineRequestList.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        model.ProxyID = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["ProxyID"].ToString()) ? Convert.ToInt32(OflineRequestList.Rows[i]["ProxyID"].ToString()) : 0;
                        model.Remark = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Remark"].ToString()) ? OflineRequestList.Rows[i]["Remark"].ToString() : string.Empty;
                        model.TravleType = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["TravelType"].ToString()) ? OflineRequestList.Rows[i]["TravelType"].ToString() : string.Empty;
                        model.ProxyFrom = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["ProxyFrom"].ToString()) ? OflineRequestList.Rows[i]["ProxyFrom"].ToString() : string.Empty;
                        model.ProxyTo = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["ProxyTo"].ToString()) ? OflineRequestList.Rows[i]["ProxyTo"].ToString() : string.Empty;
                        model.DepartDate = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["DepartDate"].ToString()) ? OflineRequestList.Rows[i]["DepartDate"].ToString() : string.Empty;
                        model.ReturnDate = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["ReturnDate"].ToString()) ? OflineRequestList.Rows[i]["ReturnDate"].ToString() : string.Empty;
                        model.DepartTime = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["DepartTime"].ToString()) ? OflineRequestList.Rows[i]["DepartTime"].ToString() : string.Empty;
                        model.ReturnTime = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["ReturnTime"].ToString()) ? OflineRequestList.Rows[i]["ReturnTime"].ToString() : string.Empty;
                        model.Adult = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Adult"].ToString()) ? OflineRequestList.Rows[i]["Adult"].ToString() : string.Empty;
                        model.Child = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Child"].ToString()) ? OflineRequestList.Rows[i]["Child"].ToString() : string.Empty;
                        model.Infant = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Infrant"].ToString()) ? OflineRequestList.Rows[i]["Infrant"].ToString() : string.Empty;
                        model.ClassType = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Class"].ToString()) ? OflineRequestList.Rows[i]["Class"].ToString() : string.Empty;
                        model.PreferedAirlines = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Airlines"].ToString()) ? OflineRequestList.Rows[i]["Airlines"].ToString() : string.Empty;
                        model.Classes = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Classes"].ToString()) ? OflineRequestList.Rows[i]["Classes"].ToString() : string.Empty;
                        model.AgentId = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["AgentID"].ToString()) ? OflineRequestList.Rows[i]["AgentID"].ToString() : string.Empty;
                        model.Agentname = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Ag_Name"].ToString()) ? OflineRequestList.Rows[i]["Ag_Name"].ToString() : string.Empty;
                        model.Status = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Status"].ToString()) ? OflineRequestList.Rows[i]["Status"].ToString() : string.Empty;
                        model.ExecID = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["Exec_ID"].ToString()) ? OflineRequestList.Rows[i]["Exec_ID"].ToString() : string.Empty;
                        model.requestDateTime = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["requestDateTime"].ToString()) ? OflineRequestList.Rows[i]["requestDateTime"].ToString() : string.Empty;
                        model.UpdateDate = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["UpdatedDate"].ToString()) ? OflineRequestList.Rows[i]["UpdatedDate"].ToString() : string.Empty;
                        model.AcceptedDate = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["AcceptedDate"].ToString()) ? OflineRequestList.Rows[i]["AcceptedDate"].ToString() : string.Empty;
                        model.ProxyType = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["ProxyType"].ToString()) ? OflineRequestList.Rows[i]["ProxyType"].ToString() : string.Empty;
                        model.BookingType = !string.IsNullOrEmpty(OflineRequestList.Rows[i]["BookingType"].ToString()) ? OflineRequestList.Rows[i]["BookingType"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<FlightTicketReport> GetHoldPNRReport(FlightTicketFilter filter)
        {
            List<FlightTicketReport> result = new List<FlightTicketReport>();
            try
            {
                DataTable dtFlightRefundTicket = FlightDataBase.GetHoldPNRReport(filter);

                if (dtFlightRefundTicket != null && dtFlightRefundTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightRefundTicket.Rows.Count; i++)
                    {
                        FlightTicketReport model = new FlightTicketReport();
                        model.PaymentMode = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PaymentMode"].ToString()) ? dtFlightRefundTicket.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        model.PGCharges = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PGCharges"].ToString()) ? dtFlightRefundTicket.Rows[i]["PGCharges"].ToString() : string.Empty;
                        model.URL = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["URL"].ToString()) ? dtFlightRefundTicket.Rows[i]["URL"].ToString() : string.Empty;
                        model.HoldTicket = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["HoldTicket"].ToString()) ? dtFlightRefundTicket.Rows[i]["HoldTicket"].ToString() : string.Empty;
                        model.Status = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Status"].ToString()) ? dtFlightRefundTicket.Rows[i]["Status"].ToString() : string.Empty;
                        model.MordifyStatus = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["MordifyStatus"].ToString()) ? dtFlightRefundTicket.Rows[i]["MordifyStatus"].ToString() : string.Empty;
                        model.GdsPnr = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["GdsPnr"].ToString()) ? dtFlightRefundTicket.Rows[i]["GdsPnr"].ToString() : string.Empty;
                        model.VC = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["VC"].ToString()) ? dtFlightRefundTicket.Rows[i]["VC"].ToString() : string.Empty;
                        model.Sector = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["sector"].ToString()) ? dtFlightRefundTicket.Rows[i]["sector"].ToString() : string.Empty;
                        model.OrderId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["OrderId"].ToString()) ? dtFlightRefundTicket.Rows[i]["OrderId"].ToString() : string.Empty;
                        model.AirlinePnr = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["AirlinePnr"].ToString()) ? dtFlightRefundTicket.Rows[i]["AirlinePnr"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Trip"].ToString()) ? dtFlightRefundTicket.Rows[i]["Trip"].ToString() : string.Empty;
                        model.TripType = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TripType"].ToString()) ? dtFlightRefundTicket.Rows[i]["TripType"].ToString() : string.Empty;
                        model.Duration = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Duration"].ToString()) ? dtFlightRefundTicket.Rows[i]["Duration"].ToString() : string.Empty;
                        model.TourCode = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TourCode"].ToString()) ? dtFlightRefundTicket.Rows[i]["TourCode"].ToString() : string.Empty;
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TotalAfterDis"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["TotalAfterDis"].ToString()) : 0;
                        model.TotalBookingCost = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TotalBookingCost"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["TotalBookingCost"].ToString()) : 0;
                        model.AdditionalMarkup = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["AdditionalMarkup"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["AdditionalMarkup"].ToString()) : 0;
                        model.Adult = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Adult"].ToString()) ? Convert.ToInt32(dtFlightRefundTicket.Rows[i]["Adult"].ToString()) : 0;
                        model.Child = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Child"].ToString()) ? Convert.ToInt32(dtFlightRefundTicket.Rows[i]["Child"].ToString()) : 0;
                        model.Infant = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Infant"].ToString()) ? Convert.ToInt32(dtFlightRefundTicket.Rows[i]["Infant"].ToString()) : 0;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightRefundTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.AgentId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["AgentId"].ToString()) ? dtFlightRefundTicket.Rows[i]["AgentId"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightRefundTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.ExecutiveId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["ExecutiveId"].ToString()) ? dtFlightRefundTicket.Rows[i]["ExecutiveId"].ToString() : string.Empty;
                        model.PaymentType = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PaymentType"].ToString()) ? dtFlightRefundTicket.Rows[i]["PaymentType"].ToString() : string.Empty;
                        model.PgTitle = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PgTitle"].ToString()) ? dtFlightRefundTicket.Rows[i]["PgTitle"].ToString() : string.Empty;
                        model.PgFName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PgFName"].ToString()) ? dtFlightRefundTicket.Rows[i]["PgFName"].ToString() : string.Empty;
                        model.PgLName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PgLName"].ToString()) ? dtFlightRefundTicket.Rows[i]["PgLName"].ToString() : string.Empty;
                        model.PgMobile = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PgMobile"].ToString()) ? dtFlightRefundTicket.Rows[i]["PgMobile"].ToString() : string.Empty;
                        model.PgEmail = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PgEmail"].ToString()) ? dtFlightRefundTicket.Rows[i]["PgEmail"].ToString() : string.Empty;
                        model.Remark = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Remark"].ToString()) ? dtFlightRefundTicket.Rows[i]["Remark"].ToString() : string.Empty;
                        model.RejectedRemark = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["RejectedRemark"].ToString()) ? dtFlightRefundTicket.Rows[i]["RejectedRemark"].ToString() : string.Empty;
                        model.PgMobile = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PgMobile"].ToString()) ? dtFlightRefundTicket.Rows[i]["PgMobile"].ToString() : string.Empty;
                        model.PgEmail = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PgEmail"].ToString()) ? dtFlightRefundTicket.Rows[i]["PgEmail"].ToString() : string.Empty;
                        model.CreateDate = dtFlightRefundTicket.Rows[i]["CreateDate"].ToString();
                        model.UpdateDate = dtFlightRefundTicket.Rows[i]["UpdateDate"].ToString();
                        model.IsMobile = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["IsMobile"].ToString()) ? dtFlightRefundTicket.Rows[i]["IsMobile"].ToString() : string.Empty;
                        model.FareType = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["FareType"].ToString()) ? dtFlightRefundTicket.Rows[i]["FareType"].ToString() : string.Empty;
                        model.ReferenceNo = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["ReferenceNo"].ToString()) ? dtFlightRefundTicket.Rows[i]["ReferenceNo"].ToString() : string.Empty;
                        model.PreholdUpdateDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PreholdUpdateDate"].ToString()) ? dtFlightRefundTicket.Rows[i]["PreholdUpdateDate"].ToString() : string.Empty;
                        model.SearchId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["SearchId"].ToString()) ? dtFlightRefundTicket.Rows[i]["SearchId"].ToString() : string.Empty;
                        model.PNRId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PNRId"].ToString()) ? dtFlightRefundTicket.Rows[i]["PNRId"].ToString() : string.Empty;
                        model.TicketId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TicketId"].ToString()) ? dtFlightRefundTicket.Rows[i]["TicketId"].ToString() : string.Empty;
                        model.HoldCharge = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["HoldCharge"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["HoldCharge"].ToString()) : 0;
                        model.IsHoldByAgent = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["IsHoldByAgent"].ToString()) ? dtFlightRefundTicket.Rows[i]["IsHoldByAgent"].ToString() : string.Empty;
                        model.HoldDateTime = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["HoldDateTime"].ToString()) ? dtFlightRefundTicket.Rows[i]["HoldDateTime"].ToString() : string.Empty;
                        model.CreditNode = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["CreditNode"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["CreditNode"].ToString()) : 0;
                        model.DebitNode = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["DebitNode"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["DebitNode"].ToString()) : 0;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<FlightTicketReport> GetTicketStatusReport(FlightTicketFilter filter)
        {
            List<FlightTicketReport> result = new List<FlightTicketReport>();
            try
            {
                DataTable dtFlightRefundTicket = FlightDataBase.GetTicketStatusReport(filter);

                if (dtFlightRefundTicket != null && dtFlightRefundTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightRefundTicket.Rows.Count; i++)
                    {
                        FlightTicketReport model = new FlightTicketReport();
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TotalAfterDis"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["TotalAfterDis"].ToString()) : 0;
                        model.Status = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Status"].ToString()) ? dtFlightRefundTicket.Rows[i]["Status"].ToString() : string.Empty;
                        model.TotalPax = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TotalPax"].ToString()) ? dtFlightRefundTicket.Rows[i]["TotalPax"].ToString() : string.Empty;
                        model.OrderId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["OrderId"].ToString()) ? dtFlightRefundTicket.Rows[i]["OrderId"].ToString() : string.Empty;
                        model.TotalBookingCost = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TotalBookingCost"].ToString()) ? Convert.ToDecimal(dtFlightRefundTicket.Rows[i]["TotalBookingCost"].ToString()) : 0;
                        model.Sector = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["sector"].ToString()) ? dtFlightRefundTicket.Rows[i]["sector"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightRefundTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.AgentId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["AgentId"].ToString()) ? dtFlightRefundTicket.Rows[i]["AgentId"].ToString() : string.Empty;
                        model.PaxId = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PaxId"].ToString()) ? Convert.ToInt32(dtFlightRefundTicket.Rows[i]["PaxId"].ToString()) : 0;
                        model.PaxType = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PaxType"].ToString()) ? dtFlightRefundTicket.Rows[i]["PaxType"].ToString() : string.Empty;
                        model.CreateDate = dtFlightRefundTicket.Rows[i]["CreateDate"].ToString();
                        model.JourneyDate = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["JourneyDate"].ToString()) ? dtFlightRefundTicket.Rows[i]["JourneyDate"].ToString() : string.Empty;
                        model.PaymentMode = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PaymentMode"].ToString()) ? dtFlightRefundTicket.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        model.VC = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["VC"].ToString()) ? dtFlightRefundTicket.Rows[i]["VC"].ToString() : string.Empty;
                        model.GdsPnr = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["GdsPnr"].ToString()) ? dtFlightRefundTicket.Rows[i]["GdsPnr"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["TicketNumber"].ToString()) ? dtFlightRefundTicket.Rows[i]["TicketNumber"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["Trip"].ToString()) ? dtFlightRefundTicket.Rows[i]["Trip"].ToString() : string.Empty;
                        model.RejectedRemark = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["RejectedRemark"].ToString()) ? dtFlightRefundTicket.Rows[i]["RejectedRemark"].ToString() : string.Empty;
                        model.PName = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["PName"].ToString()) ? dtFlightRefundTicket.Rows[i]["PName"].ToString() : string.Empty;
                        model.FareRule = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["FareRule"].ToString()) ? dtFlightRefundTicket.Rows[i]["FareRule"].ToString() : string.Empty;
                        model.IsBagFares = !string.IsNullOrEmpty(dtFlightRefundTicket.Rows[i]["IsBagFares"].ToString()) ? dtFlightRefundTicket.Rows[i]["IsBagFares"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static string CheckTktNo(int paxId, string orderId, string pnr)
        {
            string result = string.Empty;

            try
            {
                DataTable dtCheckTicket = FlightDataBase.CheckTktNo(paxId, orderId, pnr);
                if (dtCheckTicket != null && dtCheckTicket.Rows.Count > 0)
                {
                    result = dtCheckTicket.Rows[0]["Column1"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static ReIssueReFund GetTicketdIntl(int paxId, string paxType)
        {
            ReIssueReFund result = new ReIssueReFund();

            try
            {
                DataTable dtTicket = FlightDataBase.GetTicketdIntl(paxId, paxType);
                if (dtTicket != null && dtTicket.Rows.Count > 0)
                {
                    result.PNR = !string.IsNullOrEmpty(dtTicket.Rows[0]["PNR"].ToString()) ? dtTicket.Rows[0]["PNR"].ToString() : string.Empty;
                    result.Sector = !string.IsNullOrEmpty(dtTicket.Rows[0]["Sector"].ToString()) ? dtTicket.Rows[0]["Sector"].ToString() : string.Empty;
                    result.BookingDate = !string.IsNullOrEmpty(dtTicket.Rows[0]["BookinfDate"].ToString()) ? dtTicket.Rows[0]["BookinfDate"].ToString() : string.Empty;
                    result.TotalBookingCost = !string.IsNullOrEmpty(dtTicket.Rows[0]["TotalBookingCost"].ToString()) ? Convert.ToDecimal(dtTicket.Rows[0]["TotalBookingCost"].ToString()) : 0;
                    result.TotalAfterDis = !string.IsNullOrEmpty(dtTicket.Rows[0]["TotalAfterDis"].ToString()) ? Convert.ToDecimal(dtTicket.Rows[0]["TotalAfterDis"].ToString()) : 0;
                    result.UserID = !string.IsNullOrEmpty(dtTicket.Rows[0]["UserID"].ToString()) ? dtTicket.Rows[0]["UserID"].ToString() : string.Empty;
                    result.AgencyName = !string.IsNullOrEmpty(dtTicket.Rows[0]["AgencyName"].ToString()) ? dtTicket.Rows[0]["AgencyName"].ToString() : string.Empty;
                    result.OrderId = !string.IsNullOrEmpty(dtTicket.Rows[0]["OrderId"].ToString()) ? dtTicket.Rows[0]["OrderId"].ToString() : string.Empty;
                    result.AirlinePNR = !string.IsNullOrEmpty(dtTicket.Rows[0]["AirlinePnr"].ToString()) ? dtTicket.Rows[0]["AirlinePnr"].ToString() : string.Empty;
                    result.ProjectID = !string.IsNullOrEmpty(dtTicket.Rows[0]["ProjectID"].ToString()) ? dtTicket.Rows[0]["ProjectID"].ToString() : string.Empty;
                    result.VC = !string.IsNullOrEmpty(dtTicket.Rows[0]["VC"].ToString()) ? dtTicket.Rows[0]["VC"].ToString() : string.Empty;
                    result.Departure = !string.IsNullOrEmpty(dtTicket.Rows[0]["Departure"].ToString()) ? dtTicket.Rows[0]["Departure"].ToString() : string.Empty;
                    result.Destination = !string.IsNullOrEmpty(dtTicket.Rows[0]["Destination"].ToString()) ? dtTicket.Rows[0]["Destination"].ToString() : string.Empty;
                    result.DepartDate = !string.IsNullOrEmpty(dtTicket.Rows[0]["DepartDate"].ToString()) ? dtTicket.Rows[0]["DepartDate"].ToString() : string.Empty;
                    result.ArrDate = !string.IsNullOrEmpty(dtTicket.Rows[0]["ArrDate"].ToString()) ? dtTicket.Rows[0]["ArrDate"].ToString() : string.Empty;
                    result.FNo = !string.IsNullOrEmpty(dtTicket.Rows[0]["FNo"].ToString()) ? dtTicket.Rows[0]["FNo"].ToString() : string.Empty;
                    result.ArrTime = !string.IsNullOrEmpty(dtTicket.Rows[0]["ArrTime"].ToString()) ? dtTicket.Rows[0]["ArrTime"].ToString() : string.Empty;
                    result.DepTime = !string.IsNullOrEmpty(dtTicket.Rows[0]["DepTime"].ToString()) ? dtTicket.Rows[0]["DepTime"].ToString() : string.Empty;
                    result.TicketNo = !string.IsNullOrEmpty(dtTicket.Rows[0]["TicketNo"].ToString()) ? dtTicket.Rows[0]["TicketNo"].ToString() : string.Empty;
                    result.Title = !string.IsNullOrEmpty(dtTicket.Rows[0]["Title"].ToString()) ? dtTicket.Rows[0]["Title"].ToString() : string.Empty;
                    result.FName = !string.IsNullOrEmpty(dtTicket.Rows[0]["FName"].ToString()) ? dtTicket.Rows[0]["FName"].ToString() : string.Empty;
                    result.LName = !string.IsNullOrEmpty(dtTicket.Rows[0]["LName"].ToString()) ? dtTicket.Rows[0]["LName"].ToString() : string.Empty;
                    result.MName = !string.IsNullOrEmpty(dtTicket.Rows[0]["MName"].ToString()) ? dtTicket.Rows[0]["MName"].ToString() : string.Empty;
                    result.PaxType = !string.IsNullOrEmpty(dtTicket.Rows[0]["PaxType"].ToString()) ? dtTicket.Rows[0]["PaxType"].ToString() : string.Empty;
                    result.BaseFare = !string.IsNullOrEmpty(dtTicket.Rows[0]["BaseFare"].ToString()) ? dtTicket.Rows[0]["BaseFare"].ToString() : string.Empty;
                    result.YQ = !string.IsNullOrEmpty(dtTicket.Rows[0]["YQ"].ToString()) ? dtTicket.Rows[0]["YQ"].ToString() : string.Empty;
                    result.STax = !string.IsNullOrEmpty(dtTicket.Rows[0]["STax"].ToString()) ? dtTicket.Rows[0]["STax"].ToString() : string.Empty;
                    result.TFee = !string.IsNullOrEmpty(dtTicket.Rows[0]["TFee"].ToString()) ? dtTicket.Rows[0]["TFee"].ToString() : string.Empty;
                    result.Dis = !string.IsNullOrEmpty(dtTicket.Rows[0]["Dis"].ToString()) ? dtTicket.Rows[0]["Dis"].ToString() : string.Empty;
                    result.CB = !string.IsNullOrEmpty(dtTicket.Rows[0]["CB"].ToString()) ? dtTicket.Rows[0]["CB"].ToString() : string.Empty;
                    result.TDS = !string.IsNullOrEmpty(dtTicket.Rows[0]["TDS"].ToString()) ? dtTicket.Rows[0]["TDS"].ToString() : string.Empty;
                    result.TotalFare = !string.IsNullOrEmpty(dtTicket.Rows[0]["TotalFare"].ToString()) ? dtTicket.Rows[0]["TotalFare"].ToString() : string.Empty;
                    result.TotalFareAfterDis = !string.IsNullOrEmpty(dtTicket.Rows[0]["TotalFareAfterDis"].ToString()) ? dtTicket.Rows[0]["TotalFareAfterDis"].ToString() : string.Empty;
                    result.MgtFee = !string.IsNullOrEmpty(dtTicket.Rows[0]["MgtFee"].ToString()) ? Convert.ToDecimal(dtTicket.Rows[0]["MgtFee"].ToString()) : 0;
                    result.BillNoCorp = !string.IsNullOrEmpty(dtTicket.Rows[0]["BillNoCorp"].ToString()) ? dtTicket.Rows[0]["BillNoCorp"].ToString() : string.Empty;
                    result.PartnerName = !string.IsNullOrEmpty(dtTicket.Rows[0]["PartnerName"].ToString()) ? dtTicket.Rows[0]["PartnerName"].ToString() : string.Empty;
                    result.ResuID = !string.IsNullOrEmpty(dtTicket.Rows[0]["ResuID"].ToString()) ? dtTicket.Rows[0]["ResuID"].ToString() : string.Empty;
                    result.PName = !string.IsNullOrEmpty(dtTicket.Rows[0]["PName"].ToString()) ? dtTicket.Rows[0]["PName"].ToString() : string.Empty;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static ReIssueReFund OldPaxInfo(string reissueId, string title, string fName, string mName, string lName, string paxType)
        {
            ReIssueReFund result = new ReIssueReFund();

            try
            {
                DataTable dtTicket = FlightDataBase.OldPaxInfo(reissueId, title, fName, mName, lName, paxType);
                if (dtTicket != null && dtTicket.Rows.Count > 0)
                {
                    result.PNR = !string.IsNullOrEmpty(dtTicket.Rows[0]["PNR"].ToString()) ? dtTicket.Rows[0]["PNR"].ToString() : string.Empty;
                    result.Sector = !string.IsNullOrEmpty(dtTicket.Rows[0]["Sector"].ToString()) ? dtTicket.Rows[0]["Sector"].ToString() : string.Empty;
                    result.BookingDate = !string.IsNullOrEmpty(dtTicket.Rows[0]["BookinfDate"].ToString()) ? dtTicket.Rows[0]["BookinfDate"].ToString() : string.Empty;
                    result.TotalBookingCost = !string.IsNullOrEmpty(dtTicket.Rows[0]["TotalBookingCost"].ToString()) ? Convert.ToDecimal(dtTicket.Rows[0]["TotalBookingCost"].ToString()) : 0;
                    result.TotalAfterDis = !string.IsNullOrEmpty(dtTicket.Rows[0]["TotalAfterDis"].ToString()) ? Convert.ToDecimal(dtTicket.Rows[0]["TotalAfterDis"].ToString()) : 0;
                    result.UserID = !string.IsNullOrEmpty(dtTicket.Rows[0]["UserID"].ToString()) ? dtTicket.Rows[0]["UserID"].ToString() : string.Empty;
                    result.AgencyName = !string.IsNullOrEmpty(dtTicket.Rows[0]["AgencyName"].ToString()) ? dtTicket.Rows[0]["AgencyName"].ToString() : string.Empty;
                    result.OrderId = !string.IsNullOrEmpty(dtTicket.Rows[0]["OrderId"].ToString()) ? dtTicket.Rows[0]["OrderId"].ToString() : string.Empty;
                    result.AirlinePNR = !string.IsNullOrEmpty(dtTicket.Rows[0]["AirlinePnr"].ToString()) ? dtTicket.Rows[0]["AirlinePnr"].ToString() : string.Empty;
                    result.ProjectID = !string.IsNullOrEmpty(dtTicket.Rows[0]["ProjectID"].ToString()) ? dtTicket.Rows[0]["ProjectID"].ToString() : string.Empty;
                    result.VC = !string.IsNullOrEmpty(dtTicket.Rows[0]["VC"].ToString()) ? dtTicket.Rows[0]["VC"].ToString() : string.Empty;
                    result.Departure = !string.IsNullOrEmpty(dtTicket.Rows[0]["Departure"].ToString()) ? dtTicket.Rows[0]["Departure"].ToString() : string.Empty;
                    result.Destination = !string.IsNullOrEmpty(dtTicket.Rows[0]["Destination"].ToString()) ? dtTicket.Rows[0]["Destination"].ToString() : string.Empty;
                    result.DepartDate = !string.IsNullOrEmpty(dtTicket.Rows[0]["DepartDate"].ToString()) ? dtTicket.Rows[0]["DepartDate"].ToString() : string.Empty;
                    result.ArrDate = !string.IsNullOrEmpty(dtTicket.Rows[0]["ArrDate"].ToString()) ? dtTicket.Rows[0]["ArrDate"].ToString() : string.Empty;
                    result.FNo = !string.IsNullOrEmpty(dtTicket.Rows[0]["FNo"].ToString()) ? dtTicket.Rows[0]["FNo"].ToString() : string.Empty;
                    result.ArrTime = !string.IsNullOrEmpty(dtTicket.Rows[0]["ArrTime"].ToString()) ? dtTicket.Rows[0]["ArrTime"].ToString() : string.Empty;
                    result.DepTime = !string.IsNullOrEmpty(dtTicket.Rows[0]["DepTime"].ToString()) ? dtTicket.Rows[0]["DepTime"].ToString() : string.Empty;
                    result.TicketNo = !string.IsNullOrEmpty(dtTicket.Rows[0]["TicketNo"].ToString()) ? dtTicket.Rows[0]["TicketNo"].ToString() : string.Empty;
                    result.Title = !string.IsNullOrEmpty(dtTicket.Rows[0]["Title"].ToString()) ? dtTicket.Rows[0]["Title"].ToString() : string.Empty;
                    result.FName = !string.IsNullOrEmpty(dtTicket.Rows[0]["FName"].ToString()) ? dtTicket.Rows[0]["FName"].ToString() : string.Empty;
                    result.LName = !string.IsNullOrEmpty(dtTicket.Rows[0]["LName"].ToString()) ? dtTicket.Rows[0]["LName"].ToString() : string.Empty;
                    result.MName = !string.IsNullOrEmpty(dtTicket.Rows[0]["MName"].ToString()) ? dtTicket.Rows[0]["MName"].ToString() : string.Empty;
                    result.PaxType = !string.IsNullOrEmpty(dtTicket.Rows[0]["PaxType"].ToString()) ? dtTicket.Rows[0]["PaxType"].ToString() : string.Empty;
                    result.BaseFare = !string.IsNullOrEmpty(dtTicket.Rows[0]["BaseFare"].ToString()) ? dtTicket.Rows[0]["BaseFare"].ToString() : string.Empty;
                    result.YQ = !string.IsNullOrEmpty(dtTicket.Rows[0]["YQ"].ToString()) ? dtTicket.Rows[0]["YQ"].ToString() : string.Empty;
                    result.STax = !string.IsNullOrEmpty(dtTicket.Rows[0]["STax"].ToString()) ? dtTicket.Rows[0]["STax"].ToString() : string.Empty;
                    result.TFee = !string.IsNullOrEmpty(dtTicket.Rows[0]["TFree"].ToString()) ? dtTicket.Rows[0]["TFree"].ToString() : string.Empty;
                    result.Dis = !string.IsNullOrEmpty(dtTicket.Rows[0]["Dis"].ToString()) ? dtTicket.Rows[0]["Dis"].ToString() : string.Empty;
                    result.CB = !string.IsNullOrEmpty(dtTicket.Rows[0]["CB"].ToString()) ? dtTicket.Rows[0]["CB"].ToString() : string.Empty;
                    result.TDS = !string.IsNullOrEmpty(dtTicket.Rows[0]["TDS"].ToString()) ? dtTicket.Rows[0]["TDS"].ToString() : string.Empty;
                    result.TotalFare = !string.IsNullOrEmpty(dtTicket.Rows[0]["TotalFare"].ToString()) ? dtTicket.Rows[0]["TotalFare"].ToString() : string.Empty;
                    result.TotalFareAfterDis = !string.IsNullOrEmpty(dtTicket.Rows[0]["TotalFareAfterDis"].ToString()) ? dtTicket.Rows[0]["TotalFareAfterDis"].ToString() : string.Empty;
                    result.MgtFee = !string.IsNullOrEmpty(dtTicket.Rows[0]["MgtFee"].ToString()) ? Convert.ToDecimal(dtTicket.Rows[0]["MgtFee"].ToString()) : 0;
                    result.BillNoCorp = !string.IsNullOrEmpty(dtTicket.Rows[0]["BillNoCorp"].ToString()) ? dtTicket.Rows[0]["BillNoCorp"].ToString() : string.Empty;
                    result.PartnerName = !string.IsNullOrEmpty(dtTicket.Rows[0]["PartnerName"].ToString()) ? dtTicket.Rows[0]["PartnerName"].ToString() : string.Empty;
                    result.ResuID = !string.IsNullOrEmpty(dtTicket.Rows[0]["ResuID"].ToString()) ? dtTicket.Rows[0]["ResuID"].ToString() : string.Empty;
                    result.PName = !string.IsNullOrEmpty(dtTicket.Rows[0]["PName"].ToString()) ? dtTicket.Rows[0]["PName"].ToString() : string.Empty;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool InsertReIssueCancelIntl(ReIssueReFund model)
        {
            return FlightDataBase.InsertReIssueCancelIntl(model);
        }
        public static bool OTPTransactionInsert(string userId, string remark, string otpRefNo, string loginByOTP, string otpId, string serviceType)
        {
            return FlightDataBase.OTPTransactionInsert(userId, remark, otpRefNo, loginByOTP, otpId, serviceType);
        }

        public static bool UpdateTicketTeansCharges(string orderid,string charge, string amount, string AgentId)   
        {
            return FlightDataBase.UpdateTicketTeansCharges(orderid,charge,amount,AgentId);
        }
        public static Dictionary<string, string> GetEmail_Credentilas(string orderId, string cmd_Type, string counter)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            DataTable dtEmail_Credentilas = FlightDataBase.GetEmail_Credentilas(orderId, cmd_Type, counter);
            if (dtEmail_Credentilas != null && dtEmail_Credentilas.Rows.Count > 0)
            {
                result.Add("ModuleType", dtEmail_Credentilas.Rows[0]["ModuleType"].ToString());
                result.Add("ToEmail", dtEmail_Credentilas.Rows[0]["ToEmail"].ToString());
                result.Add("IsActive", dtEmail_Credentilas.Rows[0]["IsActive"].ToString());
            }

            return result;
        }
        public static List<CityAutoSearch> CityAutoSearch(string param)
        {
            List<CityAutoSearch> result = new List<CityAutoSearch>();

            try
            {
                if (!string.IsNullOrEmpty(param.Trim()))
                {
                    DataTable dtCity = FlightDataBase.GetCityList(param);
                    if (dtCity != null && dtCity.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCity.Rows.Count; i++)
                        {
                            CityAutoSearch city = new CityAutoSearch();
                            city.AirportCode = !string.IsNullOrEmpty(dtCity.Rows[i]["AirportCode"].ToString()) ? dtCity.Rows[i]["AirportCode"].ToString() : string.Empty;
                            //city.AirportName = !string.IsNullOrEmpty(dtCity.Rows[i]["AirportName"].ToString()) ? dtCity.Rows[i]["AirportName"].ToString() : string.Empty;
                            city.CityName = !string.IsNullOrEmpty(dtCity.Rows[i]["CityName"].ToString()) ? dtCity.Rows[i]["CityName"].ToString() : string.Empty;
                            //city.CountryName = !string.IsNullOrEmpty(dtCity.Rows[i]["CountryName"].ToString()) ? dtCity.Rows[i]["CountryName"].ToString() : string.Empty;
                            city.CountryCode = !string.IsNullOrEmpty(dtCity.Rows[i]["CountryCode"].ToString()) ? dtCity.Rows[i]["CountryCode"].ToString() : string.Empty;
                            //city.Latitude = !string.IsNullOrEmpty(dtCity.Rows[i]["Latitude"].ToString()) ? dtCity.Rows[i]["Latitude"].ToString() : string.Empty;
                            //city.Longitude = !string.IsNullOrEmpty(dtCity.Rows[i]["Longitude"].ToString()) ? dtCity.Rows[i]["Longitude"].ToString() : string.Empty;
                            //city.WorldAreaCode = !string.IsNullOrEmpty(dtCity.Rows[i]["WorldAreaCode"].ToString()) ? dtCity.Rows[i]["WorldAreaCode"].ToString() : string.Empty;
                            //city.Counter = !string.IsNullOrEmpty(dtCity.Rows[i]["Counter"].ToString()) ? dtCity.Rows[i]["Counter"].ToString() : string.Empty;
                            city.IsOrder = !string.IsNullOrEmpty(dtCity.Rows[i]["isorder"].ToString()) ? dtCity.Rows[i]["isorder"].ToString() : string.Empty;
                            //city.IsActive = !string.IsNullOrEmpty(dtCity.Rows[i]["isactive"].ToString()) ? dtCity.Rows[i]["isactive"].ToString() : string.Empty;
                            result.Add(city);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<AirLineModel> GetAirlinesList(string param)
        {
            List<AirLineModel> result = new List<AirLineModel>();

            try
            {
                if (!string.IsNullOrEmpty(param.Trim()))
                {
                    DataTable dtAirLine = FlightDataBase.GetAirlinesList(param);
                    if (dtAirLine != null && dtAirLine.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtAirLine.Rows.Count; i++)
                        {
                            AirLineModel airline = new AirLineModel();
                            airline.AL_Code = !string.IsNullOrEmpty(dtAirLine.Rows[i]["AL_Code"].ToString()) ? dtAirLine.Rows[i]["AL_Code"].ToString() : string.Empty;
                            airline.AL_Name = !string.IsNullOrEmpty(dtAirLine.Rows[i]["AL_Name"].ToString()) ? dtAirLine.Rows[i]["AL_Name"].ToString() : string.Empty;
                            result.Add(airline);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool InsertProxyDetails(OfflineRequest param)
        {
            return FlightDataBase.InsertProxyDetails(param);
        }
        public static bool InsertProxyPaxDetail(OfflineBookFlightDetail param)
        {
            return FlightDataBase.InsertProxyPaxDetail(param);
        }
        public static List<AdultChildInfrant> GetAdultPassengerDetailList(string proxyid, string tableName)
        {
            List<AdultChildInfrant> passList = new List<AdultChildInfrant>();

            try
            {

                DataTable dtPassDetail = FlightDataBase.GetDetailFromAnyTable("Select * from " + tableName + " where ProxyID=" + proxyid);

                if (dtPassDetail != null && dtPassDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPassDetail.Rows.Count; i++)
                    {
                        AdultChildInfrant pass = new AdultChildInfrant();
                        pass.EmpId = Convert.ToInt32(dtPassDetail.Rows[i]["EmpId"].ToString());
                        pass.ProxyID = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["ProxyID"].ToString()) ? dtPassDetail.Rows[i]["ProxyID"].ToString() : string.Empty;
                        pass.SirName = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["SirName"].ToString()) ? dtPassDetail.Rows[i]["SirName"].ToString() : string.Empty;
                        pass.FirstName = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["FirstName"].ToString()) ? dtPassDetail.Rows[i]["FirstName"].ToString() : string.Empty;
                        pass.LastName = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["LastName"].ToString()) ? dtPassDetail.Rows[i]["LastName"].ToString() : string.Empty;
                        pass.Age = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["Age"].ToString()) ? dtPassDetail.Rows[i]["Age"].ToString() : string.Empty;
                        pass.AgentID = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["AgentID"].ToString()) ? dtPassDetail.Rows[i]["AgentID"].ToString() : string.Empty;
                        pass.FFno = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["FFno"].ToString()) ? dtPassDetail.Rows[i]["FFno"].ToString() : string.Empty;
                        pass.PassportNo = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["PassportNo"].ToString()) ? dtPassDetail.Rows[i]["PassportNo"].ToString() : string.Empty;
                        pass.PPexp = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["PPexp"].ToString()) ? dtPassDetail.Rows[i]["PPexp"].ToString() : string.Empty;
                        pass.VisaDet = !string.IsNullOrEmpty(dtPassDetail.Rows[i]["VisaDet"].ToString()) ? dtPassDetail.Rows[i]["VisaDet"].ToString() : string.Empty;

                        passList.Add(pass);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return passList;
        }
        public static FlightPassengerDetail GetFlightPassengerDetail(string proxyid)
        {
            FlightPassengerDetail result = new FlightPassengerDetail();

            try
            {
                if (!string.IsNullOrEmpty(proxyid))
                {
                    DataTable dtShowProxyByID = FlightDataBase.GetDetailFromAnyTable("Select * from ProxyTicket where ProxyID=" + proxyid);
                    if (dtShowProxyByID != null && dtShowProxyByID.Rows.Count > 0)
                    {
                        AdultChildInfrant pass = new AdultChildInfrant();
                        result.AgentID = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["AgentID"].ToString()) ? dtShowProxyByID.Rows[0]["AgentID"].ToString() : string.Empty;
                        result.BookingType = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["BookingType"].ToString()) ? dtShowProxyByID.Rows[0]["BookingType"].ToString() : string.Empty;
                        result.TravelType = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["TravelType"].ToString()) ? dtShowProxyByID.Rows[0]["TravelType"].ToString() : string.Empty;
                        result.ProxyTo = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["ProxyTo"].ToString()) ? dtShowProxyByID.Rows[0]["ProxyTo"].ToString() : string.Empty;
                        result.ProxyFrom = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["ProxyFrom"].ToString()) ? dtShowProxyByID.Rows[0]["ProxyFrom"].ToString() : string.Empty;
                        result.DepartDate = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["DepartDate"].ToString()) ? dtShowProxyByID.Rows[0]["DepartDate"].ToString() : string.Empty;
                        result.ReturnDate = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["ReturnDate"].ToString()) ? dtShowProxyByID.Rows[0]["ReturnDate"].ToString() : string.Empty;
                        result.DepartTime = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["DepartTime"].ToString()) ? dtShowProxyByID.Rows[0]["DepartTime"].ToString() : string.Empty;
                        result.ReturnTime = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["ReturnTime"].ToString()) ? dtShowProxyByID.Rows[0]["ReturnTime"].ToString() : string.Empty;
                        result.Adult = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Adult"].ToString()) ? dtShowProxyByID.Rows[0]["Adult"].ToString() : string.Empty;
                        result.Child = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Child"].ToString()) ? dtShowProxyByID.Rows[0]["Child"].ToString() : string.Empty;
                        result.Infrant = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Infrant"].ToString()) ? dtShowProxyByID.Rows[0]["Infrant"].ToString() : string.Empty;
                        result.Class = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Class"].ToString()) ? dtShowProxyByID.Rows[0]["Class"].ToString() : string.Empty;
                        result.Airlines = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Airlines"].ToString()) ? dtShowProxyByID.Rows[0]["Airlines"].ToString() : string.Empty;
                        result.Classes = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Classes"].ToString()) ? dtShowProxyByID.Rows[0]["Classes"].ToString() : string.Empty;
                        result.PaymentMode = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["PaymentMode"].ToString()) ? dtShowProxyByID.Rows[0]["PaymentMode"].ToString() : string.Empty;
                        result.Remark = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Remark"].ToString()) ? dtShowProxyByID.Rows[0]["Remark"].ToString() : string.Empty;
                        result.RejectComment = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["RejectComment"].ToString()) ? dtShowProxyByID.Rows[0]["RejectComment"].ToString() : string.Empty;
                    }

                    DataTable dtAgentDetail = FlightDataBase.GetDetailFromAnyTable("SELECT Fname+Lname as Name, Address ,City+','+State+','+Country+','+zipcode as Addr, Phone, Mobile, Email, PanNo,Agency_Name,Crd_Limit FROM New_Regs where User_Id='" + result.AgentID + "'");
                    if (dtAgentDetail != null && dtAgentDetail.Rows.Count > 0)
                    {
                        result.AGName = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Name"].ToString()) ? dtShowProxyByID.Rows[0]["Name"].ToString() : string.Empty;
                        result.AGAddress = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Address"].ToString()) ? dtShowProxyByID.Rows[0]["Address"].ToString() : string.Empty;
                        result.AGAddr = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Addr"].ToString()) ? dtShowProxyByID.Rows[0]["Addr"].ToString() : string.Empty;
                        result.AGMobile = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Mobile"].ToString()) ? dtShowProxyByID.Rows[0]["Mobile"].ToString() : string.Empty;
                        result.AGEmail = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Email"].ToString()) ? dtShowProxyByID.Rows[0]["Email"].ToString() : string.Empty;
                        result.Agency_Name = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Agency_Name"].ToString()) ? dtShowProxyByID.Rows[0]["Agency_Name"].ToString() : string.Empty;
                        result.AGCrd_Limit = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["Crd_Limit"].ToString()) ? dtShowProxyByID.Rows[0]["Crd_Limit"].ToString() : string.Empty;
                    }

                    result.AdultList = GetAdultPassengerDetailList(proxyid, "Adult");
                    result.ChildList = GetAdultPassengerDetailList(proxyid, "Child");
                    result.InfrantList = GetAdultPassengerDetailList(proxyid, "Infrant");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<PaxDetailModel> SelectPaxDetail(string orderid, string transid)
        {
            List<PaxDetailModel> PaxList = new List<PaxDetailModel>();
            try
            {
                string query = "";
                if (!string.IsNullOrEmpty(transid))
                {
                    query = "SELECT PaxId, OrderId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" + orderid + "' and PaxId= '" + transid + "' ";
                }
                else
                {
                    query = "SELECT PaxId, OrderId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" + orderid + "' ";
                }
                DataTable dtPaxDetail = FlightDataBase.GetDetailFromAnyTable(query);
                if (dtPaxDetail != null && dtPaxDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPaxDetail.Rows.Count; i++)
                    {
                        PaxDetailModel pax = new PaxDetailModel();
                        pax.PaxId = Convert.ToInt32(dtPaxDetail.Rows[i]["PaxId"].ToString());
                        pax.OrderId = !string.IsNullOrEmpty(dtPaxDetail.Rows[i]["OrderId"].ToString()) ? dtPaxDetail.Rows[i]["OrderId"].ToString() : string.Empty;
                        pax.Name = !string.IsNullOrEmpty(dtPaxDetail.Rows[i]["Name"].ToString()) ? dtPaxDetail.Rows[i]["Name"].ToString() : string.Empty;
                        pax.PaxType = !string.IsNullOrEmpty(dtPaxDetail.Rows[i]["PaxType"].ToString()) ? dtPaxDetail.Rows[i]["PaxType"].ToString() : string.Empty;
                        pax.TicketNumber = !string.IsNullOrEmpty(dtPaxDetail.Rows[i]["TicketNumber"].ToString()) ? dtPaxDetail.Rows[i]["TicketNumber"].ToString() : string.Empty;
                        pax.DOB = !string.IsNullOrEmpty(dtPaxDetail.Rows[i]["DOB"].ToString()) ? dtPaxDetail.Rows[i]["DOB"].ToString() : string.Empty;
                        pax.FFNumber = !string.IsNullOrEmpty(dtPaxDetail.Rows[i]["FFNumber"].ToString()) ? dtPaxDetail.Rows[i]["FFNumber"].ToString() : string.Empty;
                        pax.FFAirline = !string.IsNullOrEmpty(dtPaxDetail.Rows[i]["FFAirline"].ToString()) ? dtPaxDetail.Rows[i]["FFAirline"].ToString() : string.Empty;
                        pax.MealType = !string.IsNullOrEmpty(dtPaxDetail.Rows[i]["MealType"].ToString()) ? dtPaxDetail.Rows[i]["MealType"].ToString() : string.Empty;
                        pax.SeatType = !string.IsNullOrEmpty(dtPaxDetail.Rows[i]["SeatType"].ToString()) ? dtPaxDetail.Rows[i]["SeatType"].ToString() : string.Empty;
                        PaxList.Add(pax);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return PaxList;
        }
        public static List<FltFareDetailsModel> SelectFareDetail(string orderid, string transid)
        {
            List<FltFareDetailsModel> fareList = new List<FltFareDetailsModel>();
            try
            {
                string query = "";
                if (!string.IsNullOrEmpty(transid))
                {
                    query = "SELECT  FltFareDetails.BaseFare as BaseFare, FltFareDetails.YQ as Fuel,  (FltFareDetails.YR+FltFareDetails.WO+FltFareDetails.OT+FltFareDetails.K3) as Tax,FltFareDetails.ServiceTax as ServiceTax,FltFareDetails.TranFee as TFee, FltFareDetails.AdminMrk+FltFareDetails.AgentMrk+FltFareDetails.DistrMrk as TCharge,TotalAfterDis , ISNULL(MgtFee,0) as MgtFee,ISNULL(FareType,'') as FareType  FROM FltPaxDetails INNER JOIN FltFareDetails ON FltPaxDetails.OrderId = FltFareDetails.OrderId AND FltPaxDetails.PaxType = FltFareDetails.PaxType WHERE FltPaxDetails.PaxId = '" + transid + "' ";
                }
                else
                {
                    query = "select PaxType, BaseFare, YQ as Fuel,(YR+WO+OT+K3+ISNULL(ticketcopymarkupforTAX,0)) as Tax,ServiceTax,TranFee as TFee, (AdminMrk+AgentMrk+DistrMrk) as TCharge,(BaseFare+YQ+YR+WO+OT+ServiceTax+AdminMrk+AgentMrk+DistrMrk+TranFee+K3+ISNULL(ticketcopymarkupforTAX,0)+ISNULL(ticketcopymarkupforTC,0)) as Total,TotalAfterDis , ISNULL(MgtFee,0) as MgtFee,ISNULL(FareType,'') as FareType,ISNULL(ticketcopymarkupforTAX,0) as ticketcopymarkupforTAX , ISNULL(ticketcopymarkupforTC,0) as ticketcopymarkupforTC  FROM FltFareDetails where OrderId='" + orderid + "' ";
                }
                DataTable dtfareDetail = FlightDataBase.GetDetailFromAnyTable(query);
                if (dtfareDetail != null && dtfareDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtfareDetail.Rows.Count; i++)
                    {
                        FltFareDetailsModel fare = new FltFareDetailsModel();
                        fare.PaxType = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["PaxType"].ToString()) ? dtfareDetail.Rows[i]["PaxType"].ToString() : string.Empty;
                        fare.BaseFare = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["BaseFare"].ToString()) ? dtfareDetail.Rows[i]["BaseFare"].ToString() : string.Empty;
                        fare.Fuel = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["Fuel"].ToString()) ? dtfareDetail.Rows[i]["Fuel"].ToString() : string.Empty;
                        fare.Tax = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["Tax"].ToString()) ? dtfareDetail.Rows[i]["Tax"].ToString() : string.Empty;
                        fare.ServiceTax = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["ServiceTax"].ToString()) ? dtfareDetail.Rows[i]["ServiceTax"].ToString() : string.Empty;
                        fare.TFee = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["TFee"].ToString()) ? dtfareDetail.Rows[i]["TFee"].ToString() : string.Empty;
                        fare.TCharge = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["TCharge"].ToString()) ? dtfareDetail.Rows[i]["TCharge"].ToString() : string.Empty;
                        fare.Total = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["Total"].ToString()) ? dtfareDetail.Rows[i]["Total"].ToString() : string.Empty;
                        fare.TotalAfterDis = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["TotalAfterDis"].ToString()) ? dtfareDetail.Rows[i]["TotalAfterDis"].ToString() : string.Empty;
                        fare.MgtFee = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["MgtFee"].ToString()) ? dtfareDetail.Rows[i]["MgtFee"].ToString() : string.Empty;
                        fare.FareType = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["FareType"].ToString()) ? dtfareDetail.Rows[i]["FareType"].ToString() : string.Empty;
                        fare.ticketcopymarkupforTAX = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["ticketcopymarkupforTAX"].ToString()) ? dtfareDetail.Rows[i]["ticketcopymarkupforTAX"].ToString() : string.Empty;
                        fare.ticketcopymarkupforTC = !string.IsNullOrEmpty(dtfareDetail.Rows[i]["ticketcopymarkupforTC"].ToString()) ? dtfareDetail.Rows[i]["ticketcopymarkupforTC"].ToString() : string.Empty;
                        fareList.Add(fare);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return fareList;
        }
        public static List<FlightHeaderModel> SelectHeaderDetail(string orderid)
        {
            List<FlightHeaderModel> FltHeaderList = new List<FlightHeaderModel>();
            try
            {
                DataTable dtHeaderDetail = FlightDataBase.GetDetailFromAnyTable("SELECT * FROM  FltHeader WHERE OrderId = '" + orderid + "' ");
                if (dtHeaderDetail != null && dtHeaderDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtHeaderDetail.Rows.Count; i++)
                    {
                        FlightHeaderModel flightHeader = new FlightHeaderModel();
                        flightHeader.OrderId = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["OrderId"].ToString()) ? dtHeaderDetail.Rows[i]["OrderId"].ToString() : string.Empty;
                        flightHeader.sector = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["sector"].ToString()) ? dtHeaderDetail.Rows[i]["sector"].ToString() : string.Empty;
                        flightHeader.Status = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["Status"].ToString()) ? dtHeaderDetail.Rows[i]["Status"].ToString() : string.Empty;
                        flightHeader.MordifyStatus = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["MordifyStatus"].ToString()) ? dtHeaderDetail.Rows[i]["MordifyStatus"].ToString() : string.Empty;
                        flightHeader.GdsPnr = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["GdsPnr"].ToString()) ? dtHeaderDetail.Rows[i]["GdsPnr"].ToString() : string.Empty;
                        flightHeader.AirlinePnr = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["AirlinePnr"].ToString()) ? dtHeaderDetail.Rows[i]["AirlinePnr"].ToString() : string.Empty;
                        flightHeader.VC = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["VC"].ToString()) ? dtHeaderDetail.Rows[i]["VC"].ToString() : string.Empty;
                        flightHeader.Duration = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["Duration"].ToString()) ? dtHeaderDetail.Rows[i]["Duration"].ToString() : string.Empty;
                        flightHeader.TripType = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["TripType"].ToString()) ? dtHeaderDetail.Rows[i]["TripType"].ToString() : string.Empty;
                        flightHeader.Trip = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["Trip"].ToString()) ? dtHeaderDetail.Rows[i]["Trip"].ToString() : string.Empty;
                        flightHeader.TourCode = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["TourCode"].ToString()) ? dtHeaderDetail.Rows[i]["TourCode"].ToString() : string.Empty;
                        flightHeader.TotalBookingCost = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["TotalBookingCost"].ToString()) ? dtHeaderDetail.Rows[i]["TotalBookingCost"].ToString() : string.Empty;
                        flightHeader.TotalAfterDis = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["TotalAfterDis"].ToString()) ? dtHeaderDetail.Rows[i]["TotalAfterDis"].ToString() : string.Empty;
                        flightHeader.SFDis = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["SFDis"].ToString()) ? dtHeaderDetail.Rows[i]["SFDis"].ToString() : string.Empty;
                        flightHeader.AdditionalMarkup = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["AdditionalMarkup"].ToString()) ? dtHeaderDetail.Rows[i]["AdditionalMarkup"].ToString() : string.Empty;
                        flightHeader.Adult = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["Adult"].ToString()) ? dtHeaderDetail.Rows[i]["Adult"].ToString() : string.Empty;
                        flightHeader.Child = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["Child"].ToString()) ? dtHeaderDetail.Rows[i]["Child"].ToString() : string.Empty;
                        flightHeader.Infant = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["Infant"].ToString()) ? dtHeaderDetail.Rows[i]["Infant"].ToString() : string.Empty;
                        flightHeader.AgentId = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["AgentId"].ToString()) ? dtHeaderDetail.Rows[i]["AgentId"].ToString() : string.Empty;
                        flightHeader.AgencyName = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["AgencyName"].ToString()) ? dtHeaderDetail.Rows[i]["AgencyName"].ToString() : string.Empty;
                        flightHeader.DistrId = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["DistrId"].ToString()) ? dtHeaderDetail.Rows[i]["DistrId"].ToString() : string.Empty;
                        flightHeader.ExecutiveId = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["ExecutiveId"].ToString()) ? dtHeaderDetail.Rows[i]["ExecutiveId"].ToString() : string.Empty;
                        flightHeader.PaymentType = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["PaymentType"].ToString()) ? dtHeaderDetail.Rows[i]["PaymentType"].ToString() : string.Empty;
                        flightHeader.PgTitle = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["PgTitle"].ToString()) ? dtHeaderDetail.Rows[i]["PgTitle"].ToString() : string.Empty;
                        flightHeader.PgFName = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["PgFName"].ToString()) ? dtHeaderDetail.Rows[i]["PgFName"].ToString() : string.Empty;
                        flightHeader.PgLName = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["PgLName"].ToString()) ? dtHeaderDetail.Rows[i]["PgLName"].ToString() : string.Empty;
                        flightHeader.PgMobile = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["PgMobile"].ToString()) ? dtHeaderDetail.Rows[i]["PgMobile"].ToString() : string.Empty;
                        flightHeader.PgEmail = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["PgEmail"].ToString()) ? dtHeaderDetail.Rows[i]["PgEmail"].ToString() : string.Empty;
                        flightHeader.CreateDate = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["CreateDate"].ToString()) ? dtHeaderDetail.Rows[i]["CreateDate"].ToString() : string.Empty;
                        flightHeader.ResuCharge = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["ResuCharge"].ToString()) ? dtHeaderDetail.Rows[i]["ResuCharge"].ToString() : string.Empty;
                        flightHeader.ResuServiseCharge = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["ResuServiseCharge"].ToString()) ? dtHeaderDetail.Rows[i]["ResuServiseCharge"].ToString() : string.Empty;
                        flightHeader.ResuFareDiff = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["ResuFareDiff"].ToString()) ? dtHeaderDetail.Rows[i]["ResuFareDiff"].ToString() : string.Empty;
                        flightHeader.PaxId = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["PaxId"].ToString()) ? dtHeaderDetail.Rows[i]["PaxId"].ToString() : string.Empty;
                        flightHeader.ImportCharge = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["ImportCharge"].ToString()) ? dtHeaderDetail.Rows[i]["ImportCharge"].ToString() : string.Empty;
                        flightHeader.YFLAG = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["YFLAG"].ToString()) ? dtHeaderDetail.Rows[i]["YFLAG"].ToString() : string.Empty;
                        flightHeader.YCRN = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["YCRN"].ToString()) ? dtHeaderDetail.Rows[i]["YCRN"].ToString() : string.Empty;
                        flightHeader.Y_CAN_FARE = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["Y_CAN_FARE"].ToString()) ? dtHeaderDetail.Rows[i]["Y_CAN_FARE"].ToString() : string.Empty;
                        flightHeader.ProjectID = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["ProjectID"].ToString()) ? dtHeaderDetail.Rows[i]["ProjectID"].ToString() : string.Empty;
                        flightHeader.BillNoCorp = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["BillNoCorp"].ToString()) ? dtHeaderDetail.Rows[i]["BillNoCorp"].ToString() : string.Empty;
                        flightHeader.BookedBy = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["BookedBy"].ToString()) ? dtHeaderDetail.Rows[i]["BookedBy"].ToString() : string.Empty;
                        flightHeader.FareType = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["FareType"].ToString()) ? dtHeaderDetail.Rows[i]["FareType"].ToString() : string.Empty;
                        flightHeader.ReferenceNo = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["ReferenceNo"].ToString()) ? dtHeaderDetail.Rows[i]["ReferenceNo"].ToString() : string.Empty;
                        flightHeader.APIID = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["APIID"].ToString()) ? dtHeaderDetail.Rows[i]["APIID"].ToString() : string.Empty;
                        flightHeader.PNRId = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["PNRId"].ToString()) ? dtHeaderDetail.Rows[i]["PNRId"].ToString() : string.Empty;
                        flightHeader.TicketId = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["TicketId"].ToString()) ? dtHeaderDetail.Rows[i]["TicketId"].ToString() : string.Empty;
                        flightHeader.HoldCharge = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["HoldCharge"].ToString()) ? dtHeaderDetail.Rows[i]["HoldCharge"].ToString() : string.Empty;
                        flightHeader.CouponCode = !string.IsNullOrEmpty(dtHeaderDetail.Rows[i]["CouponCode"].ToString()) ? dtHeaderDetail.Rows[i]["CouponCode"].ToString() : string.Empty;
                        FltHeaderList.Add(flightHeader);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return FltHeaderList;
        }
        public static List<AgencyModel> SelectAgencyDetail(string AgentId)
        {
            List<AgencyModel> agencyList = new List<AgencyModel>();
            try
            {
                DataTable dtagencyDetail = FlightDataBase.GetDetailFromAnyTable("SELECT Agency_Name,Email,Mobile,Address,Address,(City+' - '+State+' - '+Country) as Address1,IsCorp from agent_register  WHERE User_Id = '" + AgentId + "' ");
                if (dtagencyDetail != null && dtagencyDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtagencyDetail.Rows.Count; i++)
                    {
                        AgencyModel model = new AgencyModel();
                        model.Agency_Name = !string.IsNullOrEmpty(dtagencyDetail.Rows[i]["Agency_Name"].ToString()) ? dtagencyDetail.Rows[i]["Agency_Name"].ToString() : string.Empty;
                        model.Email = !string.IsNullOrEmpty(dtagencyDetail.Rows[i]["Email"].ToString()) ? dtagencyDetail.Rows[i]["Email"].ToString() : string.Empty;
                        model.Mobile = !string.IsNullOrEmpty(dtagencyDetail.Rows[i]["Mobile"].ToString()) ? dtagencyDetail.Rows[i]["Mobile"].ToString() : string.Empty;
                        model.Address = !string.IsNullOrEmpty(dtagencyDetail.Rows[i]["Address"].ToString()) ? dtagencyDetail.Rows[i]["Address"].ToString() : string.Empty;
                        model.Address1 = !string.IsNullOrEmpty(dtagencyDetail.Rows[i]["Address1"].ToString()) ? dtagencyDetail.Rows[i]["Address1"].ToString() : string.Empty;
                        model.IsCorp = !string.IsNullOrEmpty(dtagencyDetail.Rows[i]["IsCorp"].ToString()) ? dtagencyDetail.Rows[i]["IsCorp"].ToString() : string.Empty;
                        agencyList.Add(model);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return agencyList;
        }
        public static List<FltDetailsModel> SelectFlightDetail(string orderid)
        {
            List<FltDetailsModel> FltList = new List<FltDetailsModel>();
            try
            {
                DataTable dtFltDetail = FlightDataBase.GetDetailFromAnyTable("SELECT FltId, OrderId, DepCityOrAirportCode AS DFrom, DepCityOrAirportName as DepAirName, ArrCityOrAirportCode AS ATo, ArrCityOrAirportName as ArrAirName, DepDate, DepTime, ArrDate, ArrTime, AirlineCode, AirlineName, FltNumber, AirCraft, CreateDate, UpdateDate,ISNULL(AdtRbd,'') as AdtRbd,ISNULL(ChdRbd,'') as ChdRbd FROM FltDetails  WHERE OrderId = '" + orderid + "' Order by FltId ");
                if (dtFltDetail != null && dtFltDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFltDetail.Rows.Count; i++)
                    {
                        FltDetailsModel flt = new FltDetailsModel();
                        flt.OrderId = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["OrderId"].ToString()) ? dtFltDetail.Rows[i]["OrderId"].ToString() : string.Empty;
                        flt.DFrom = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["DFrom"].ToString()) ? dtFltDetail.Rows[i]["DFrom"].ToString() : string.Empty;
                        flt.DepAirName = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["DepAirName"].ToString()) ? dtFltDetail.Rows[i]["DepAirName"].ToString() : string.Empty;
                        flt.ATo = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["ATo"].ToString()) ? dtFltDetail.Rows[i]["ATo"].ToString() : string.Empty;
                        flt.ArrAirName = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["ArrAirName"].ToString()) ? dtFltDetail.Rows[i]["ArrAirName"].ToString() : string.Empty;
                        flt.DepDate = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["DepDate"].ToString()) ? dtFltDetail.Rows[i]["DepDate"].ToString() : string.Empty;
                        flt.DepTime = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["DepTime"].ToString()) ? dtFltDetail.Rows[i]["DepTime"].ToString() : string.Empty;
                        flt.ArrDate = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["ArrDate"].ToString()) ? dtFltDetail.Rows[i]["ArrDate"].ToString() : string.Empty;
                        flt.ArrTime = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["ArrTime"].ToString()) ? dtFltDetail.Rows[i]["ArrTime"].ToString() : string.Empty;
                        flt.AirlineCode = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["AirlineCode"].ToString()) ? dtFltDetail.Rows[i]["AirlineCode"].ToString() : string.Empty;
                        flt.AirlineName = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["AirlineName"].ToString()) ? dtFltDetail.Rows[i]["AirlineName"].ToString() : string.Empty;
                        flt.FltNumber = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["FltNumber"].ToString()) ? dtFltDetail.Rows[i]["FltNumber"].ToString() : string.Empty;
                        flt.AirCraft = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["AirCraft"].ToString()) ? dtFltDetail.Rows[i]["AirCraft"].ToString() : string.Empty;
                        flt.CreateDate = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["CreateDate"].ToString()) ? dtFltDetail.Rows[i]["CreateDate"].ToString() : string.Empty;
                        flt.AdtRbd = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["AdtRbd"].ToString()) ? dtFltDetail.Rows[i]["AdtRbd"].ToString() : string.Empty;
                        flt.ChdRbd = !string.IsNullOrEmpty(dtFltDetail.Rows[i]["ChdRbd"].ToString()) ? dtFltDetail.Rows[i]["ChdRbd"].ToString() : string.Empty;
                        flt.LogoUrl = !string.IsNullOrEmpty(flt.AirlineCode) ? "http://b2brichatravels.in/AirLogo/sm" + flt.AirlineCode + ".gif" : "";
                        FltList.Add(flt);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return FltList;
        }
        public static FlightHeaderModel SelectAgent(string orderid)
        {
            FlightHeaderModel result = new FlightHeaderModel();

            try
            {
                if (!string.IsNullOrEmpty(orderid))
                {
                    DataTable dtShowProxyByID = FlightDataBase.GetDetailFromAnyTable("SELECT AgentId FROM   FltHeader WHERE OrderId = '" + orderid + "' ");
                    if (dtShowProxyByID != null && dtShowProxyByID.Rows.Count > 0)
                    {
                        AdultChildInfrant pass = new AdultChildInfrant();
                        result.AgentId = !string.IsNullOrEmpty(dtShowProxyByID.Rows[0]["AgentId"].ToString()) ? dtShowProxyByID.Rows[0]["AgentId"].ToString() : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<SelectedFlightDetailsGalModel> GetSelectedFltDtls_Gal(string orderid, string agentid)
        {
            List<SelectedFlightDetailsGalModel> result = new List<SelectedFlightDetailsGalModel>();
            try
            {
                DataTable dtselectedTicket = FlightDataBase.GetSelectedFltDtls_Gal(orderid, agentid);

                if (dtselectedTicket != null && dtselectedTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtselectedTicket.Rows.Count; i++)
                    {
                        SelectedFlightDetailsGalModel model = new SelectedFlightDetailsGalModel();
                        model.OrgDestFrom = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.OrgDestTo = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestTo"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestTo"].ToString() : string.Empty;
                        model.DepartureLocation = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["DepartureLocation"].ToString()) ? dtselectedTicket.Rows[i]["DepartureLocation"].ToString() : string.Empty;
                        model.DepartureCityName = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["DepartureCityName"].ToString()) ? dtselectedTicket.Rows[i]["DepartureCityName"].ToString() : string.Empty;
                        model.DepAirportCode = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["DepAirportCode"].ToString()) ? dtselectedTicket.Rows[i]["DepAirportCode"].ToString() : string.Empty;
                        model.DepartureTerminal = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["DepartureTerminal"].ToString()) ? dtselectedTicket.Rows[i]["DepartureTerminal"].ToString() : string.Empty;
                        model.ArrivalLocation = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ArrivalLocation"].ToString()) ? dtselectedTicket.Rows[i]["ArrivalLocation"].ToString() : string.Empty;
                        model.ArrivalCityName = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ArrivalCityName"].ToString()) ? dtselectedTicket.Rows[i]["ArrivalCityName"].ToString() : string.Empty;
                        model.ArrAirportCode = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ArrAirportCode"].ToString()) ? dtselectedTicket.Rows[i]["ArrAirportCode"].ToString() : string.Empty;
                        model.ArrivalTerminal = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ArrivalTerminal"].ToString()) ? dtselectedTicket.Rows[i]["ArrivalTerminal"].ToString() : string.Empty;
                        model.DepartureDate = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["DepartureDate"].ToString()) ? dtselectedTicket.Rows[i]["DepartureDate"].ToString() : string.Empty;
                        model.Departure_Date = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Departure_Date"].ToString()) ? dtselectedTicket.Rows[i]["Departure_Date"].ToString() : string.Empty;
                        model.DepartureTime = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["DepartureTime"].ToString()) ? dtselectedTicket.Rows[i]["DepartureTime"].ToString() : string.Empty;
                        model.ArrivalDate = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ArrivalDate"].ToString()) ? dtselectedTicket.Rows[i]["ArrivalDate"].ToString() : string.Empty;
                        model.Arrival_Date = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Arrival_Date"].ToString()) ? dtselectedTicket.Rows[i]["Arrival_Date"].ToString() : string.Empty;
                        model.ArrivalTime = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ArrivalTime"].ToString()) ? dtselectedTicket.Rows[i]["ArrivalTime"].ToString() : string.Empty;
                        model.Adult = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Adult"].ToString()) ? dtselectedTicket.Rows[i]["Adult"].ToString() : string.Empty;
                        model.Child = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Child"].ToString()) ? dtselectedTicket.Rows[i]["Child"].ToString() : string.Empty;
                        model.Infant = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Infant"].ToString()) ? dtselectedTicket.Rows[i]["Infant"].ToString() : string.Empty;
                        model.TotPax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["TotPax"].ToString()) ? dtselectedTicket.Rows[i]["TotPax"].ToString() : string.Empty;
                        // model.MarketingCarrier = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        // model.OperatingCarrier = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        // model.FlightIdentification = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //  model.ValiDatingCarrier = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.AirLineName = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.AvailableSeats = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.AdtCabin = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.ChdCabin = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.InfCabin = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.AdtRbd = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.ChdRbd = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.InfRbd = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.RBD = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.AdtFare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.AdtBfare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.AdtTax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.ChdFare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.ChdBfare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.ChdTax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.InfFare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.InfBfare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.InfTax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.TotalBfare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.TotFare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.TotalTax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.NetFare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.STax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.TFee = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.DisCount = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.Searchvalue = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.LineItemNumber = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.Leg = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.Flight = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.Provider = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.Tot_Dur = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.TripType = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.EQ = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["EQ"].ToString()) ? dtselectedTicket.Rows[i]["EQ"].ToString() : string.Empty;
                        model.Stops = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Stops"].ToString()) ? dtselectedTicket.Rows[i]["Stops"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Trip"].ToString()) ? dtselectedTicket.Rows[i]["Trip"].ToString() : string.Empty;
                        model.Sector = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Sector"].ToString()) ? dtselectedTicket.Rows[i]["Sector"].ToString() : string.Empty;
                        // model.TripCnt = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.Currency = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Currency"].ToString()) ? dtselectedTicket.Rows[i]["Currency"].ToString() : string.Empty;
                        model.ADTAdminMrk = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ADTAdminMrk"].ToString()) ? dtselectedTicket.Rows[i]["ADTAdminMrk"].ToString() : string.Empty;
                        model.ADTAgentMrk = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ADTAgentMrk"].ToString()) ? dtselectedTicket.Rows[i]["ADTAgentMrk"].ToString() : string.Empty;
                        model.CHDAdminMrk = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.CHDAgentMrk = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["CHDAdminMrk"].ToString()) ? dtselectedTicket.Rows[i]["CHDAdminMrk"].ToString() : string.Empty;
                        model.InfAdminMrk = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["InfAdminMrk"].ToString()) ? dtselectedTicket.Rows[i]["InfAdminMrk"].ToString() : string.Empty;
                        model.InfAgentMrk = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["InfAgentMrk"].ToString()) ? dtselectedTicket.Rows[i]["InfAgentMrk"].ToString() : string.Empty;
                        model.IATAComm = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["AdtFareType"].ToString()) ? dtselectedTicket.Rows[i]["AdtFareType"].ToString() : string.Empty;
                        model.AdtFareType = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["AdtFareType"].ToString()) ? dtselectedTicket.Rows[i]["AdtFareType"].ToString() : string.Empty;
                        model.AdtFarebasis = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["AdtFarebasis"].ToString()) ? dtselectedTicket.Rows[i]["AdtFarebasis"].ToString() : string.Empty;
                        //model.ChdFareType = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.ChdFarebasis = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.InfFareType = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        // model.InfFarebasis = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.FareBasis = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.FBPaxType = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.AdtFSur = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.ChdFSur = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.InfFSur = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.TotalFuelSur = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.sno = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.depdatelcc = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.arrdatelcc = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.OriginalTF = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        //model.OriginalTT = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.Track_id = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Track_id"].ToString()) ? dtselectedTicket.Rows[i]["Track_id"].ToString() : string.Empty;
                        model.FlightStatus = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["FlightStatus"].ToString()) ? dtselectedTicket.Rows[i]["FlightStatus"].ToString() : string.Empty;
                        model.Adt_Tax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Adt_Tax"].ToString()) ? dtselectedTicket.Rows[i]["Adt_Tax"].ToString() : string.Empty;
                        model.AdtOT = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["AdtOT"].ToString()) ? dtselectedTicket.Rows[i]["AdtOT"].ToString() : string.Empty;
                        model.AdtSrvTax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["AdtSrvTax"].ToString()) ? dtselectedTicket.Rows[i]["AdtSrvTax"].ToString() : string.Empty;
                        model.Chd_Tax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Chd_Tax"].ToString()) ? dtselectedTicket.Rows[i]["Chd_Tax"].ToString() : string.Empty;
                        model.ChdOT = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ChdOT"].ToString()) ? dtselectedTicket.Rows[i]["ChdOT"].ToString() : string.Empty;
                        model.ChdSrvTax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ChdSrvTax"].ToString()) ? dtselectedTicket.Rows[i]["ChdSrvTax"].ToString() : string.Empty;
                        model.Inf_Tax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["Inf_Tax"].ToString()) ? dtselectedTicket.Rows[i]["Inf_Tax"].ToString() : string.Empty;
                        model.InfOT = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["InfOT"].ToString()) ? dtselectedTicket.Rows[i]["InfOT"].ToString() : string.Empty;
                        model.InfSrvTax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["OrgDestFrom"].ToString()) ? dtselectedTicket.Rows[i]["OrgDestFrom"].ToString() : string.Empty;
                        model.SrvTax = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["SrvTax"].ToString()) ? dtselectedTicket.Rows[i]["SrvTax"].ToString() : string.Empty;
                        model.AdtCB = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["AdtCB"].ToString()) ? dtselectedTicket.Rows[i]["AdtCB"].ToString() : string.Empty;
                        model.ChdCB = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ChdCB"].ToString()) ? dtselectedTicket.Rows[i]["ChdCB"].ToString() : string.Empty;
                        model.InfCB = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["InfCB"].ToString()) ? dtselectedTicket.Rows[i]["InfCB"].ToString() : string.Empty;
                        model.User_id = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["User_id"].ToString()) ? dtselectedTicket.Rows[i]["User_id"].ToString() : string.Empty;
                        model.AdtMgtFee = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["AdtMgtFee"].ToString()) ? dtselectedTicket.Rows[i]["AdtMgtFee"].ToString() : string.Empty;
                        model.ChdMgtFee = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["ChdMgtFee"].ToString()) ? dtselectedTicket.Rows[i]["ChdMgtFee"].ToString() : string.Empty;
                        model.InfMgtFee = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["InfMgtFee"].ToString()) ? dtselectedTicket.Rows[i]["InfMgtFee"].ToString() : string.Empty;
                        model.TotMgtFee = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["TotMgtFee"].ToString()) ? dtselectedTicket.Rows[i]["TotMgtFee"].ToString() : string.Empty;
                        model.CreatedDate = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["CreatedDate"].ToString()) ? dtselectedTicket.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        model.RESULTTYPE = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["RESULTTYPE"].ToString()) ? dtselectedTicket.Rows[i]["RESULTTYPE"].ToString() : string.Empty;
                        model.SearchId = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["SearchId"].ToString()) ? dtselectedTicket.Rows[i]["SearchId"].ToString() : string.Empty;
                        model.PNRId = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["PNRId"].ToString()) ? dtselectedTicket.Rows[i]["PNRId"].ToString() : string.Empty;
                        model.TicketId = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["TicketId"].ToString()) ? dtselectedTicket.Rows[i]["TicketId"].ToString() : string.Empty;
                        model.IsBagFare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["IsBagFare"].ToString()) ? dtselectedTicket.Rows[i]["IsBagFare"].ToString() : string.Empty;
                        model.STINFCOMM = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["STADTCOMM"].ToString()) ? dtselectedTicket.Rows[i]["STADTCOMM"].ToString() : string.Empty;
                        model.STADTCB = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["STADTCB"].ToString()) ? dtselectedTicket.Rows[i]["STADTCB"].ToString() : string.Empty;
                        model.STCHDCB = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["STCHDCB"].ToString()) ? dtselectedTicket.Rows[i]["STCHDCB"].ToString() : string.Empty;
                        model.STINFCB = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["STINFCB"].ToString()) ? dtselectedTicket.Rows[i]["STINFCB"].ToString() : string.Empty;
                        model.STADTCOMM1 = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["STADTCOMM1"].ToString()) ? dtselectedTicket.Rows[i]["STADTCOMM1"].ToString() : string.Empty;
                        model.STCHDCOMM1 = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["STCHDCOMM1"].ToString()) ? dtselectedTicket.Rows[i]["STCHDCOMM1"].ToString() : string.Empty;
                        model.STIATACOMM = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["STIATACOMM"].ToString()) ? dtselectedTicket.Rows[i]["STIATACOMM"].ToString() : string.Empty;
                        model.FamilyFare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["FamilyFare"].ToString()) ? dtselectedTicket.Rows[i]["FamilyFare"].ToString() : string.Empty;
                        model.IsSMEFare = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["IsSMEFare"].ToString()) ? dtselectedTicket.Rows[i]["IsSMEFare"].ToString() : string.Empty;
                        model.STChdComm = !string.IsNullOrEmpty(dtselectedTicket.Rows[i]["STChdComm"].ToString()) ? dtselectedTicket.Rows[i]["STChdComm"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<T_Flt_Meal_And_Baggage_RequestModel> Get_MEAL_BAG_FareDetails(string orderid, string transid)
        {
            List<T_Flt_Meal_And_Baggage_RequestModel> result = new List<T_Flt_Meal_And_Baggage_RequestModel>();
            try
            {
                DataTable dtGet_MEAL_BAG = FlightDataBase.Get_MEAL_BAG_FareDetails(orderid, transid);

                if (dtGet_MEAL_BAG != null && dtGet_MEAL_BAG.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGet_MEAL_BAG.Rows.Count; i++)
                    {
                        T_Flt_Meal_And_Baggage_RequestModel model = new T_Flt_Meal_And_Baggage_RequestModel();
                        model.Title = !string.IsNullOrEmpty(dtGet_MEAL_BAG.Rows[i]["Title"].ToString()) ? dtGet_MEAL_BAG.Rows[i]["Title"].ToString() : string.Empty;
                        model.FName = !string.IsNullOrEmpty(dtGet_MEAL_BAG.Rows[i]["FName"].ToString()) ? dtGet_MEAL_BAG.Rows[i]["FName"].ToString() : string.Empty;
                        model.MName = !string.IsNullOrEmpty(dtGet_MEAL_BAG.Rows[i]["MName"].ToString()) ? dtGet_MEAL_BAG.Rows[i]["MName"].ToString() : string.Empty;
                        model.LName = !string.IsNullOrEmpty(dtGet_MEAL_BAG.Rows[i]["LName"].ToString()) ? dtGet_MEAL_BAG.Rows[i]["LName"].ToString() : string.Empty;
                        model.MealPrice = !string.IsNullOrEmpty(dtGet_MEAL_BAG.Rows[i]["MealPrice"].ToString()) ? dtGet_MEAL_BAG.Rows[i]["MealPrice"].ToString() : string.Empty;
                        model.TotalPrice = !string.IsNullOrEmpty(dtGet_MEAL_BAG.Rows[i]["TotalPrice"].ToString()) ? dtGet_MEAL_BAG.Rows[i]["TotalPrice"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<BagInfomartionModel> GetBaggageInformation(string Trip, string VC, bool IsBagFare)
        {
            List<BagInfomartionModel> result = new List<BagInfomartionModel>();
            try
            {
                DataTable dtbaggage = FlightDataBase.GetBaggageInformation(Trip, VC, IsBagFare);

                if (dtbaggage != null && dtbaggage.Rows.Count > 0)
                {
                    for (int i = 0; i < dtbaggage.Rows.Count; i++)
                    {
                        BagInfomartionModel model = new BagInfomartionModel();
                        model.BaggageName = !string.IsNullOrEmpty(dtbaggage.Rows[i]["BaggageName"].ToString()) ? dtbaggage.Rows[i]["BaggageName"].ToString() : string.Empty;
                        model.Weight = !string.IsNullOrEmpty(dtbaggage.Rows[i]["Weight"].ToString()) ? dtbaggage.Rows[i]["Weight"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtbaggage.Rows[i]["Trip"].ToString()) ? dtbaggage.Rows[i]["Trip"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }

    }

}