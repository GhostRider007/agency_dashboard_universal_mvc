﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using static ModelLibrary.FlightModel;

namespace DataBaseLibrary
{
    public static class FlightDataBase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }
        public static DataTable GetTicektReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_GetTicketDetail_Intl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AirlinePNR", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTicketDetail_Intl");
                dataTable = dataSet.Tables["GetTicketDetail_Intl"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetRefundReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_GetCancellationDetail_Intl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Airline", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo   ", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ExecID", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PartnerName", (!string.IsNullOrEmpty(filter.PartnerName) ? filter.PartnerName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaymentMode", (!string.IsNullOrEmpty(filter.PaymentMode) ? filter.PaymentMode : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetRefundDetail_Intl");
                dataTable = dataSet.Tables["GetRefundDetail_Intl"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetHoldPNRReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GETHOLDPNRREPORT", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@USERTYPE", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LOGINID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Airline", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo   ", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TRIP", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Action", (!string.IsNullOrEmpty(filter.Actionby) ? filter.Actionby : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@execid", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@distrid", (!string.IsNullOrEmpty(filter.distrid) ? filter.distrid : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GETHOLDPNRREPORT");
                dataTable = dataSet.Tables["GETHOLDPNRREPORT"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetSelectedFltDtls_Gal(string orderid, string agentid)
        {
            try
            {
                sqlCommand = new SqlCommand("GetSelectedFltDtls_Gal", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;               
                sqlCommand.Parameters.Add(new SqlParameter("@TrackId", orderid));
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", agentid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetSelectedFltDtls");
                dataTable = dataSet.Tables["GetSelectedFltDtls"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable Get_MEAL_BAG_FareDetails(string orderid, string transid)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_SELECT_MBAG_DETAILS", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@OID", orderid));
                sqlCommand.Parameters.Add(new SqlParameter("@paxid", transid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "SP_SELECT_MBAG");
                dataTable = dataSet.Tables["SP_SELECT_MBAG"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetBaggageInformation(string Trip, string VS,bool IsBagFare)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GETBAGGAGE_INFO", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TRIP", Trip));
                sqlCommand.Parameters.Add(new SqlParameter("@AIRLINE", VS));
                sqlCommand.Parameters.Add(new SqlParameter("@IsBagFare", IsBagFare));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GETBAGGAGE");
                dataTable = dataSet.Tables["GETBAGGAGE"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetProxyBookingReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("GetProxyBookingReport", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentID", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ExecID", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ProxyTrip", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetProxyBooking");
                dataTable = dataSet.Tables["GetProxyBooking"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetReIssueDetail(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_GetReIssueDetail_Intl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@UserID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Airline", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo   ", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ExecID", (!string.IsNullOrEmpty(filter.ExecID) ? filter.ExecID : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetReIssueDetail_Intl");
                dataTable = dataSet.Tables["GetReIssueDetail_Intl"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetTicketStatusReport(FlightTicketFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_GetTicketDetail_IntlStatus", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AirlinePNR", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo   ", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTicketDetail_IntlStatus");
                dataTable = dataSet.Tables["GetTicketDetail_IntlStatus"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable CheckTktNo(int paxId, string orderId, string pnr)
        {
            try
            {
                sqlCommand = new SqlCommand("CheckTktNo_New", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@PaxId", paxId));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderId", orderId));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", pnr));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTable");
                dataTable = dataSet.Tables["GetTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable GetTicketdIntl(int paxId, string paxType)
        {
            try
            {
                sqlCommand = new SqlCommand("CancelRequestIntl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@PaxId", paxId));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxType", paxType));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTable");
                dataTable = dataSet.Tables["GetTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable OldPaxInfo(string reissueId, string title, string fName, string mName, string lName, string paxType)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GetOldPaxDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@reissueid", reissueId));
                sqlCommand.Parameters.Add(new SqlParameter("@Title", title));
                sqlCommand.Parameters.Add(new SqlParameter("@FName", fName));
                sqlCommand.Parameters.Add(new SqlParameter("@MName", mName));
                sqlCommand.Parameters.Add(new SqlParameter("@LName", lName));
                sqlCommand.Parameters.Add(new SqlParameter("@PaxType", paxType));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTable");
                dataTable = dataSet.Tables["GetTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static bool InsertReIssueCancelIntl(ReIssueReFund model)
        {
            try
            {
                sqlCommand = new SqlCommand("InsReIssueCancelIntl", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PaxID", !string.IsNullOrEmpty(model.PaxID) ? model.PaxID : string.Empty);
                sqlCommand.Parameters.AddWithValue("@PNR", model.PNR);
                sqlCommand.Parameters.AddWithValue("@Tkt_No", model.TicketNo);
                sqlCommand.Parameters.AddWithValue("@Sector", model.Sector);
                sqlCommand.Parameters.AddWithValue("@departure", model.Departure);
                sqlCommand.Parameters.AddWithValue("@destination", model.Destination);
                sqlCommand.Parameters.AddWithValue("@Title", model.Title);
                sqlCommand.Parameters.AddWithValue("@pax_fname", model.FName);
                sqlCommand.Parameters.AddWithValue("@pax_lname", model.LName);
                sqlCommand.Parameters.AddWithValue("@PaxType", model.PaxType);
                sqlCommand.Parameters.AddWithValue("@Booking_date", model.BookingDate);
                sqlCommand.Parameters.AddWithValue("@departure_date", model.DepartDate);
                sqlCommand.Parameters.AddWithValue("@UserID", model.UserID);
                sqlCommand.Parameters.AddWithValue("@Agency_Name", model.AgencyName);
                sqlCommand.Parameters.AddWithValue("@Base_Fare", model.BaseFare);
                sqlCommand.Parameters.AddWithValue("@Tax", !string.IsNullOrEmpty(model.Tax) ? model.Tax : string.Empty);
                sqlCommand.Parameters.AddWithValue("@YQ", model.YQ);
                sqlCommand.Parameters.AddWithValue("@Service_Tax", model.STax);
                sqlCommand.Parameters.AddWithValue("@Tran_Fees", model.TFee);
                sqlCommand.Parameters.AddWithValue("@Discount", model.Dis);
                sqlCommand.Parameters.AddWithValue("@CB", model.CB);
                sqlCommand.Parameters.AddWithValue("@TDS", model.TDS);
                sqlCommand.Parameters.AddWithValue("@TotalFare", model.TotalFare);
                sqlCommand.Parameters.AddWithValue("@TotalFareAfterDiscount", model.TotalFareAfterDis);
                sqlCommand.Parameters.AddWithValue("@OrderID", model.OrderId);
                sqlCommand.Parameters.AddWithValue("@DepTime", model.DepTime);
                sqlCommand.Parameters.AddWithValue("@AirlinePNR", model.AirlinePNR);
                sqlCommand.Parameters.AddWithValue("@FNo", model.FNo);
                sqlCommand.Parameters.AddWithValue("@Remark", model.Remark);
                sqlCommand.Parameters.AddWithValue("@IDs", model.RefundID); // genrate random number//"RISU""8346397933"//RISU8346397933
                sqlCommand.Parameters.AddWithValue("@Status", model.Status);
                sqlCommand.Parameters.AddWithValue("@VC", model.VC);
                sqlCommand.Parameters.AddWithValue("@ReqTyp", model.ReqTyp);
                sqlCommand.Parameters.AddWithValue("@Trip", !string.IsNullOrEmpty(model.Trip) ? model.Trip : string.Empty);
                sqlCommand.Parameters.AddWithValue("@ProjectID", model.ProjectID);//Nothing
                sqlCommand.Parameters.AddWithValue("@MgtFee", model.MgtFee);//0
                sqlCommand.Parameters.AddWithValue("@CancelledBy", model.CancelledBy);
                sqlCommand.Parameters.AddWithValue("@BillNoCorp", model.BillNoCorp);//Nothing
                sqlCommand.Parameters.AddWithValue("@BillNoAir", model.BillNoAir);//Nothing

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool OTPTransactionInsert(string userId, string remark, string otpRefNo, string loginByOTP, string otpId, string serviceType)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_Insert_Transaction_OTP", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserID", userId);
                sqlCommand.Parameters.AddWithValue("@Remark", remark);
                sqlCommand.Parameters.AddWithValue("@OTPRefNo", otpRefNo);
                sqlCommand.Parameters.AddWithValue("@LoginByOTP", loginByOTP);
                sqlCommand.Parameters.AddWithValue("@OTPId", otpId);
                sqlCommand.Parameters.AddWithValue("@ServiceType", serviceType);

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateTicketTeansCharges(string orderid,string charge, string amount,string AgentId)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_UPDATE_TICKETTRANS_CHARGES", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
                sqlCommand.Parameters.AddWithValue("@OrderId", orderid);
                sqlCommand.Parameters.AddWithValue("@Amount", Convert.ToDecimal(amount));
                sqlCommand.Parameters.AddWithValue("@ChargeType", charge);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", AgentId);
                sqlCommand.Parameters.AddWithValue("@IPAddress", ":1");
                sqlCommand.Parameters.AddWithValue("@ActionType", charge);
                sqlCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static DataTable GetEmail_Credentilas(string orderId, string cmd_Type, string counter)
        {
            try
            {
                sqlCommand = new SqlCommand("USP_TICKETSTATUS", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@ORDERID", orderId));
                sqlCommand.Parameters.Add(new SqlParameter("@CMD_TYPE", cmd_Type));
                sqlCommand.Parameters.Add(new SqlParameter("@COUNTER", counter));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTable");
                dataTable = dataSet.Tables["GetTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable GetCityList(string param)
        {
            try
            {
                sqlCommand = new SqlCommand("CityAutoSearch", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@param1", param));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "WorldAirportInfo");
                dataTable = dataSet.Tables["WorldAirportInfo"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataTable GetAirlinesList(string param)
        {
            try
            {
                sqlCommand = new SqlCommand("AilrineList", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@param1", param));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "AirLineNames");
                dataTable = dataSet.Tables["AirLineNames"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static bool InsertProxyDetails(OfflineRequest param)
        {
            bool issuccess = false;
            try
            {
                sqlCommand = new SqlCommand("InsertProxy", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@BookingType", param.BookingType);
                sqlCommand.Parameters.AddWithValue("@TravelType", param.TripType);
                sqlCommand.Parameters.AddWithValue("@ProxyFrom", param.FromCityCode);
                sqlCommand.Parameters.AddWithValue("@ProxyTo", param.ToCityCode);
                sqlCommand.Parameters.AddWithValue("@DepartDate", param.DepartureDate);
                sqlCommand.Parameters.AddWithValue("@ReturnDate", !string.IsNullOrEmpty(param.ReturnDate) ? param.ReturnDate : "");
                sqlCommand.Parameters.AddWithValue("@DepartTime", param.Time);
                sqlCommand.Parameters.AddWithValue("@ReturnTime", !string.IsNullOrEmpty(param.ReturnDate) ? "Anytime" : "");
                sqlCommand.Parameters.AddWithValue("@Adult", param.Adult);
                sqlCommand.Parameters.AddWithValue("@Child", param.Child);
                sqlCommand.Parameters.AddWithValue("@Infrant", param.Infant);
                sqlCommand.Parameters.AddWithValue("@Class", param.ClassType);
                sqlCommand.Parameters.AddWithValue("@Airline", param.PreferedAirlinesCode);
                sqlCommand.Parameters.AddWithValue("@Classes", param.Classes);
                sqlCommand.Parameters.AddWithValue("@PaymentMode", param.PaymentLimit);
                sqlCommand.Parameters.AddWithValue("@Remark", !string.IsNullOrEmpty(param.Remark) ? param.Remark : "");
                sqlCommand.Parameters.AddWithValue("@AgentID", param.AgentUser_Id);
                sqlCommand.Parameters.AddWithValue("@Ag_Name", param.Agentname);
                sqlCommand.Parameters.AddWithValue("@Status", param.Status);
                sqlCommand.Parameters.AddWithValue("@Trip", param.Trip);
                sqlCommand.Parameters.AddWithValue("@Ptype", param.PType);
                sqlCommand.Parameters.AddWithValue("@ProjectID", !string.IsNullOrEmpty(param.ProjectId) ? param.ProjectId : "");
                sqlCommand.Parameters.AddWithValue("@BookedBy", !string.IsNullOrEmpty(param.BookedBy) ? param.BookedBy : "");

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                issuccess = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return issuccess;
        }
        public static bool InsertProxyPaxDetail(OfflineBookFlightDetail param)
        {
            try
            {
                sqlCommand = new SqlCommand("InsertProxyPax_NM", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ProxyID", param.ProxyId);
                sqlCommand.Parameters.AddWithValue("@Title", param.Title);
                sqlCommand.Parameters.AddWithValue("@FirstName", param.FirstName);
                sqlCommand.Parameters.AddWithValue("@LastName", param.LastName);
                sqlCommand.Parameters.AddWithValue("@Age", param.Age);
                sqlCommand.Parameters.AddWithValue("@AgentID", param.AgentID);
                sqlCommand.Parameters.AddWithValue("@PaxType", param.PaxType);
                sqlCommand.Parameters.AddWithValue("@FFNO", !string.IsNullOrEmpty(param.FreqFlyerNO) ? param.FreqFlyerNO : "");
                sqlCommand.Parameters.AddWithValue("@PASSNO", !string.IsNullOrEmpty(param.PassportNo) ? param.PassportNo : "");
                sqlCommand.Parameters.AddWithValue("@PPEXP", !string.IsNullOrEmpty(param.PPExp) ? param.PPExp : "");
                sqlCommand.Parameters.AddWithValue("@VISADET", !string.IsNullOrEmpty(param.VisaDet) ? param.VisaDet : "");

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        //public static DataTable GetAdultPassengerDetailList(string proxyid, string tableName)
        //{
        //    try
        //    {
        //        sqlCommand = new SqlCommand("Select * from " + tableName + " where ProxyID=" + proxyid, ConnectToDataBase.MyAmdDBConnection);

        //        sqlDataAdapter = new SqlDataAdapter();
        //        sqlDataAdapter.SelectCommand = sqlCommand;
        //        dataSet = new DataSet();
        //        sqlDataAdapter.Fill(dataSet, "PassDetail");
        //        dataTable = dataSet.Tables["PassDetail"];
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //    return dataTable;
        //}        
        public static DataTable GetDetailFromAnyTable(string query)
        {
            try
            {
                sqlCommand = new SqlCommand(query, ConnectToDataBase.MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "CommonDetail");
                dataTable = dataSet.Tables["CommonDetail"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
    }
}
