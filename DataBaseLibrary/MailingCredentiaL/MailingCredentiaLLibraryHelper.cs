﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using static ModelLibrary.MailingCredentialModel;

namespace DataBaseLibrary.MailingCredentiaL
{
    public static class MailingCredentialLibraryHelper
    {
        public static MailingCredential GetMailingDetails(string department, string UserID)
        {
            MailingCredential mailCred = new MailingCredential();

            try
            {
                DataTable dtMail = MailingCredentialDataBase.GetMailingDetails(department, UserID);
                if (dtMail.Rows.Count > 0)
                {
                    mailCred.Counter = !string.IsNullOrEmpty(dtMail.Rows[0]["COUNTER"].ToString()) ? Convert.ToInt32(dtMail.Rows[0]["COUNTER"].ToString()) : 0;
                    mailCred.MailingDepartment = !string.IsNullOrEmpty(dtMail.Rows[0]["MAILINGDEPARTMENT"].ToString()) ? dtMail.Rows[0]["MAILINGDEPARTMENT"].ToString() : string.Empty;
                    mailCred.MailFrom = !string.IsNullOrEmpty(dtMail.Rows[0]["MAILFROM"].ToString()) ? dtMail.Rows[0]["MAILFROM"].ToString() : string.Empty;
                    mailCred.MailTo = !string.IsNullOrEmpty(dtMail.Rows[0]["MAILTO"].ToString()) ? dtMail.Rows[0]["MAILTO"].ToString() : string.Empty;
                    mailCred.CC = !string.IsNullOrEmpty(dtMail.Rows[0]["CC"].ToString()) ? dtMail.Rows[0]["CC"].ToString() : string.Empty;
                    mailCred.BCC = !string.IsNullOrEmpty(dtMail.Rows[0]["BCC"].ToString()) ? dtMail.Rows[0]["BCC"].ToString() : string.Empty;
                    mailCred.SmtpClient = !string.IsNullOrEmpty(dtMail.Rows[0]["SMTPCLIENT"].ToString()) ? dtMail.Rows[0]["SMTPCLIENT"].ToString() : string.Empty;
                    mailCred.Subject = !string.IsNullOrEmpty(dtMail.Rows[0]["SUBJECT"].ToString()) ? dtMail.Rows[0]["SUBJECT"].ToString() : string.Empty;
                    mailCred.AgentId = !string.IsNullOrEmpty(dtMail.Rows[0]["AGENTID"].ToString()) ? dtMail.Rows[0]["AGENTID"].ToString() : string.Empty;
                    mailCred.CreatedDate = !string.IsNullOrEmpty(dtMail.Rows[0]["CREATEDDATE"].ToString()) ? dtMail.Rows[0]["CREATEDDATE"].ToString() : string.Empty;
                    mailCred.UserID = !string.IsNullOrEmpty(dtMail.Rows[0]["UserID"].ToString()) ? dtMail.Rows[0]["UserID"].ToString() : string.Empty;
                    mailCred.Pass = !string.IsNullOrEmpty(dtMail.Rows[0]["Pass"].ToString()) ? dtMail.Rows[0]["Pass"].ToString() : string.Empty;
                    mailCred.Body = !string.IsNullOrEmpty(dtMail.Rows[0]["Body"].ToString()) ? dtMail.Rows[0]["Body"].ToString() : string.Empty;
                    mailCred.Status = !string.IsNullOrEmpty(dtMail.Rows[0]["Status"].ToString()) ? (dtMail.Rows[0]["Status"].ToString().Trim().ToLower() == "true" ? true : false) : false;
                    mailCred.Regards = !string.IsNullOrEmpty(dtMail.Rows[0]["REGARDS"].ToString()) ? dtMail.Rows[0]["REGARDS"].ToString() : string.Empty;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return mailCred;
        }
    }
}
