﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataBaseLibrary.MailingCredentiaL
{
    public static class MailingCredentialDataBase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }
        public static DataTable GetMailingDetails(string department, string userID)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GETMAILINGCREDENTIAL_ITZ", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Department", department);
                sqlCommand.Parameters.AddWithValue("@UserID", userID);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "TBL_MAILING_CREDENTIAL_ITZ");
                dataTable = dataSet.Tables["TBL_MAILING_CREDENTIAL_ITZ"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
    }
}
