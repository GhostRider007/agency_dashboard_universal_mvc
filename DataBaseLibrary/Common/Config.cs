﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace DataBaseLibrary.Common
{
    public static class Config
    {
        public static string MyAmdDBConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString; }
        }
        public static string MyConConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["myCon"].ConnectionString; }
        }
        public static string HTLConnStrConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["HTLConnStr"].ConnectionString; }
        }
    }
}
