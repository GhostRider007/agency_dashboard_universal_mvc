﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataBaseLibrary.Agency
{
    public static class AgencyDataBase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static DataTable GetAgencyDetails(string userId)
        {
            try
            {
                sqlCommand = new SqlCommand("AgencyDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("UserId", userId);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "agent_register");
                dataTable = dataSet.Tables["agent_register"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
    }
}
