﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using static ModelLibrary.AgencyModel;

namespace DataBaseLibrary.Agency
{
    public static class AgencyLibraryHelper
    {
        public static Agency_Del GetAgencyDetails(string userId)
        {
            Agency_Del agency = new Agency_Del();

            try
            {
                DataTable dtAgency = AgencyDataBase.GetAgencyDetails(userId);

                if (dtAgency.Rows.Count > 0)
                {
                    agency.Counter = !string.IsNullOrEmpty(dtAgency.Rows[0]["Counter"].ToString()) ? Convert.ToInt32(dtAgency.Rows[0]["Counter"].ToString()) : 0;
                    agency.Timestamp_Create = !string.IsNullOrEmpty(dtAgency.Rows[0]["timestamp_create"].ToString()) ? dtAgency.Rows[0]["timestamp_create"].ToString() : string.Empty;
                    agency.Title = !string.IsNullOrEmpty(dtAgency.Rows[0]["Title"].ToString()) ? dtAgency.Rows[0]["Title"].ToString() : string.Empty;
                    agency.Fname = !string.IsNullOrEmpty(dtAgency.Rows[0]["Fname"].ToString()) ? dtAgency.Rows[0]["Fname"].ToString() : string.Empty;
                    agency.Mname = !string.IsNullOrEmpty(dtAgency.Rows[0]["Mname"].ToString()) ? dtAgency.Rows[0]["Mname"].ToString() : string.Empty;
                    agency.Lname = !string.IsNullOrEmpty(dtAgency.Rows[0]["Lname"].ToString()) ? dtAgency.Rows[0]["Lname"].ToString() : string.Empty;
                    agency.Address = !string.IsNullOrEmpty(dtAgency.Rows[0]["Address"].ToString()) ? dtAgency.Rows[0]["Address"].ToString() : string.Empty;

                    agency.City = !string.IsNullOrEmpty(dtAgency.Rows[0]["City"].ToString()) ? dtAgency.Rows[0]["City"].ToString() : string.Empty;
                    agency.State = !string.IsNullOrEmpty(dtAgency.Rows[0]["State"].ToString()) ? dtAgency.Rows[0]["State"].ToString() : string.Empty;
                    agency.Country = !string.IsNullOrEmpty(dtAgency.Rows[0]["Country"].ToString()) ? dtAgency.Rows[0]["Country"].ToString() : string.Empty;
                    agency.ZipCode = !string.IsNullOrEmpty(dtAgency.Rows[0]["zipcode"].ToString()) ? dtAgency.Rows[0]["zipcode"].ToString() : string.Empty;
                    agency.Phone = !string.IsNullOrEmpty(dtAgency.Rows[0]["Phone"].ToString()) ? dtAgency.Rows[0]["Phone"].ToString() : string.Empty;
                    agency.Mobile = !string.IsNullOrEmpty(dtAgency.Rows[0]["Mobile"].ToString()) ? dtAgency.Rows[0]["Mobile"].ToString() : string.Empty;
                    agency.Email = !string.IsNullOrEmpty(dtAgency.Rows[0]["Email"].ToString()) ? dtAgency.Rows[0]["Email"].ToString() : string.Empty;
                    agency.Alt_Email = !string.IsNullOrEmpty(dtAgency.Rows[0]["Alt_Email"].ToString()) ? dtAgency.Rows[0]["Alt_Email"].ToString() : string.Empty;

                    agency.Fax_no = !string.IsNullOrEmpty(dtAgency.Rows[0]["Fax_no"].ToString()) ? dtAgency.Rows[0]["Fax_no"].ToString() : string.Empty;
                    agency.Agency_Name = !string.IsNullOrEmpty(dtAgency.Rows[0]["Agency_Name"].ToString()) ? dtAgency.Rows[0]["Agency_Name"].ToString() : string.Empty;
                    agency.Website = !string.IsNullOrEmpty(dtAgency.Rows[0]["Website"].ToString()) ? dtAgency.Rows[0]["Website"].ToString() : string.Empty;
                    agency.PanNo = !string.IsNullOrEmpty(dtAgency.Rows[0]["PanNo"].ToString()) ? dtAgency.Rows[0]["PanNo"].ToString() : string.Empty;
                    agency.Status = !string.IsNullOrEmpty(dtAgency.Rows[0]["Status"].ToString()) ? dtAgency.Rows[0]["Status"].ToString() : string.Empty;
                    agency.Stax_no = !string.IsNullOrEmpty(dtAgency.Rows[0]["Stax_no"].ToString()) ? dtAgency.Rows[0]["Stax_no"].ToString() : string.Empty;
                    agency.Remark = !string.IsNullOrEmpty(dtAgency.Rows[0]["Remark"].ToString()) ? dtAgency.Rows[0]["Remark"].ToString() : string.Empty;
                    agency.Sec_Qes = !string.IsNullOrEmpty(dtAgency.Rows[0]["Sec_Qes"].ToString()) ? dtAgency.Rows[0]["Sec_Qes"].ToString() : string.Empty;
                    agency.Sec_Ans = !string.IsNullOrEmpty(dtAgency.Rows[0]["Sec_Ans"].ToString()) ? dtAgency.Rows[0]["Sec_Ans"].ToString() : string.Empty;
                    agency.User_Id = !string.IsNullOrEmpty(dtAgency.Rows[0]["User_Id"].ToString()) ? dtAgency.Rows[0]["User_Id"].ToString() : string.Empty;
                    agency.PWD = !string.IsNullOrEmpty(dtAgency.Rows[0]["PWD"].ToString()) ? dtAgency.Rows[0]["PWD"].ToString() : string.Empty;

                    agency.Agent_Type = !string.IsNullOrEmpty(dtAgency.Rows[0]["Agent_Type"].ToString()) ? dtAgency.Rows[0]["Agent_Type"].ToString() : string.Empty;
                    agency.Crd_Limit = !string.IsNullOrEmpty(dtAgency.Rows[0]["Crd_Limit"].ToString()) ? Convert.ToDecimal(dtAgency.Rows[0]["Crd_Limit"].ToString()) : 0;
                    agency.Crd_Trns_Date = !string.IsNullOrEmpty(dtAgency.Rows[0]["Crd_Trns_Date"].ToString()) ? dtAgency.Rows[0]["Crd_Trns_Date"].ToString() : string.Empty;
                    agency.Distr = !string.IsNullOrEmpty(dtAgency.Rows[0]["Distr"].ToString()) ? dtAgency.Rows[0]["Distr"].ToString() : string.Empty;
                    agency.Ag_Logo = !string.IsNullOrEmpty(dtAgency.Rows[0]["ag_logo"].ToString()) ? dtAgency.Rows[0]["ag_logo"].ToString() : string.Empty;
                    agency.Agent_status = !string.IsNullOrEmpty(dtAgency.Rows[0]["Agent_status"].ToString()) ? dtAgency.Rows[0]["Agent_status"].ToString() : string.Empty;
                    agency.TDS = !string.IsNullOrEmpty(dtAgency.Rows[0]["TDS"].ToString()) ? dtAgency.Rows[0]["TDS"].ToString() : string.Empty;
                    agency.Online_Tkt = !string.IsNullOrEmpty(dtAgency.Rows[0]["Online_Tkt"].ToString()) ? dtAgency.Rows[0]["Online_Tkt"].ToString() : string.Empty;
                    agency.SalesExecID = !string.IsNullOrEmpty(dtAgency.Rows[0]["SalesExecID"].ToString()) ? dtAgency.Rows[0]["SalesExecID"].ToString() : string.Empty;

                    agency.ExmptTDS = !string.IsNullOrEmpty(dtAgency.Rows[0]["ExmptTDS"].ToString()) ? Convert.ToDecimal(dtAgency.Rows[0]["ExmptTDS"].ToString()) : 0;
                    agency.ExmptTdsLimit = !string.IsNullOrEmpty(dtAgency.Rows[0]["ExmptTdsLimit"].ToString()) ? Convert.ToDecimal(dtAgency.Rows[0]["ExmptTdsLimit"].ToString()) : 0;
                    agency.NAV_ID = !string.IsNullOrEmpty(dtAgency.Rows[0]["NAV_ID"].ToString()) ? dtAgency.Rows[0]["NAV_ID"].ToString() : string.Empty;
                    agency.Y_ID = !string.IsNullOrEmpty(dtAgency.Rows[0]["Y_ID"].ToString()) ? dtAgency.Rows[0]["Y_ID"].ToString() : string.Empty;
                    agency.IsCorp = !string.IsNullOrEmpty(dtAgency.Rows[0]["IsCorp"].ToString()) ? (dtAgency.Rows[0]["IsCorp"].ToString().Trim().ToLower() == "true" ? true : false) : false;
                    agency.IsPWD = !string.IsNullOrEmpty(dtAgency.Rows[0]["IsPWD"].ToString()) ? (dtAgency.Rows[0]["IsPWD"].ToString().Trim().ToLower() == "true" ? true : false) : false;
                    agency.Decode_ITZ = !string.IsNullOrEmpty(dtAgency.Rows[0]["Decode_ITZ"].ToString()) ? dtAgency.Rows[0]["Decode_ITZ"].ToString() : string.Empty;
                    agency.MerchantKey_ITZ = !string.IsNullOrEmpty(dtAgency.Rows[0]["MerchantKey_ITZ"].ToString()) ? dtAgency.Rows[0]["MerchantKey_ITZ"].ToString() : string.Empty;

                    agency.Pwd_ITZ = !string.IsNullOrEmpty(dtAgency.Rows[0]["Pwd_ITZ"].ToString()) ? dtAgency.Rows[0]["Pwd_ITZ"].ToString() : string.Empty;
                    agency.LastLogin_ITZ = !string.IsNullOrEmpty(dtAgency.Rows[0]["LastLogin_ITZ"].ToString()) ? dtAgency.Rows[0]["LastLogin_ITZ"].ToString() : string.Empty;
                    agency.ModeType_ITZ = !string.IsNullOrEmpty(dtAgency.Rows[0]["ModeType_ITZ"].ToString()) ? dtAgency.Rows[0]["ModeType_ITZ"].ToString() : string.Empty;
                    agency.SvcType_ITZ = !string.IsNullOrEmpty(dtAgency.Rows[0]["SvcType_ITZ"].ToString()) ? dtAgency.Rows[0]["SvcType_ITZ"].ToString() : string.Empty;
                    agency.District = !string.IsNullOrEmpty(dtAgency.Rows[0]["District"].ToString()) ? dtAgency.Rows[0]["District"].ToString() : string.Empty;
                    agency.StateCode = !string.IsNullOrEmpty(dtAgency.Rows[0]["StateCode"].ToString()) ? dtAgency.Rows[0]["StateCode"].ToString() : string.Empty;
                    agency.GSTNO = !string.IsNullOrEmpty(dtAgency.Rows[0]["GSTNO"].ToString()) ? dtAgency.Rows[0]["GSTNO"].ToString() : string.Empty;
                    agency.GST_Company_Name = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_Company_Name"].ToString()) ? dtAgency.Rows[0]["GST_Company_Name"].ToString() : string.Empty;
                    agency.GST_Company_Address = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_Company_Address"].ToString()) ? dtAgency.Rows[0]["GST_Company_Address"].ToString() : string.Empty;
                    agency.GST_PhoneNo = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_PhoneNo"].ToString()) ? dtAgency.Rows[0]["GST_PhoneNo"].ToString() : string.Empty;
                    agency.GST_Email = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_Email"].ToString()) ? dtAgency.Rows[0]["GST_Email"].ToString() : string.Empty;

                    agency.Is_GST_Apply = !string.IsNullOrEmpty(dtAgency.Rows[0]["Is_GST_Apply"].ToString()) ? (dtAgency.Rows[0]["Is_GST_Apply"].ToString().Trim().ToLower() == "true" ? true : false) : false;
                    agency.GSTRemark = !string.IsNullOrEmpty(dtAgency.Rows[0]["GSTRemark"].ToString()) ? dtAgency.Rows[0]["GSTRemark"].ToString() : string.Empty;
                    agency.GST_UpdateDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_UpdateDate"].ToString()) ? dtAgency.Rows[0]["GST_UpdateDate"].ToString() : string.Empty;
                    agency.GST_City = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_City"].ToString()) ? dtAgency.Rows[0]["GST_City"].ToString() : string.Empty;
                    agency.GST_State = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_State"].ToString()) ? dtAgency.Rows[0]["GST_State"].ToString() : string.Empty;

                    agency.GST_State_Code = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_State_Code"].ToString()) ? dtAgency.Rows[0]["GST_State_Code"].ToString() : string.Empty;
                    agency.GST_Pincode = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_Pincode"].ToString()) ? dtAgency.Rows[0]["GST_Pincode"].ToString() : string.Empty;
                    agency.AgencyId = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgencyId"].ToString()) ? dtAgency.Rows[0]["AgencyId"].ToString() : string.Empty;

                    agency.AgentLimit = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgentLimit"].ToString()) ? Convert.ToDecimal(dtAgency.Rows[0]["AgentLimit"].ToString()) : 0;
                    agency.AgentLimitTrnsDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgentLimitTrnsDate"].ToString()) ? dtAgency.Rows[0]["AgentLimitTrnsDate"].ToString() : string.Empty;
                    agency.DueAmount = !string.IsNullOrEmpty(dtAgency.Rows[0]["DueAmount"].ToString()) ? Convert.ToDecimal(dtAgency.Rows[0]["DueAmount"].ToString()) : 0;
                    agency.DueAmtTrnsDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["DueAmtTrnsDate"].ToString()) ? dtAgency.Rows[0]["DueAmtTrnsDate"].ToString() : string.Empty;
                    agency.VirtualCreditLimit = !string.IsNullOrEmpty(dtAgency.Rows[0]["VirtualCreditLimit"].ToString()) ? Convert.ToDecimal(dtAgency.Rows[0]["VirtualCreditLimit"].ToString()) : 0;
                    agency.VirtualFromDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["VirtualFromDate"].ToString()) ? dtAgency.Rows[0]["VirtualFromDate"].ToString() : string.Empty;
                    agency.VirtualToDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["VirtualToDate"].ToString()) ? dtAgency.Rows[0]["VirtualToDate"].ToString() : string.Empty;
                    agency.VirtualCreditTrnsDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["VirtualCreditTrnsDate"].ToString()) ? dtAgency.Rows[0]["VirtualCreditTrnsDate"].ToString() : string.Empty;
                    agency.IsWhiteLabel = !string.IsNullOrEmpty(dtAgency.Rows[0]["IsWhiteLabel"].ToString()) ? (dtAgency.Rows[0]["IsWhiteLabel"].ToString().Trim().ToLower() == "true" ? true : false) : false;
                    agency.StockistCrd_limit = !string.IsNullOrEmpty(dtAgency.Rows[0]["StockistCrd_limit"].ToString()) ? Convert.ToDecimal(dtAgency.Rows[0]["StockistCrd_limit"].ToString()) : 0;
                    agency.Area = !string.IsNullOrEmpty(dtAgency.Rows[0]["Area"].ToString()) ? dtAgency.Rows[0]["Area"].ToString() : string.Empty;
                    agency.NamePanCard = !string.IsNullOrEmpty(dtAgency.Rows[0]["NamePanCard"].ToString()) ? dtAgency.Rows[0]["NamePanCard"].ToString() : string.Empty;
                    agency.OTP = !string.IsNullOrEmpty(dtAgency.Rows[0]["OTP"].ToString()) ? dtAgency.Rows[0]["OTP"].ToString() : string.Empty;
                    agency.OTPStatus = !string.IsNullOrEmpty(dtAgency.Rows[0]["OTPStatus"].ToString()) ? (dtAgency.Rows[0]["OTPStatus"].ToString().Trim().ToLower() == "true" ? true : false) : false;
                    agency.OTPCreatedDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["OTPCreatedDate"].ToString()) ? dtAgency.Rows[0]["OTPCreatedDate"].ToString() : string.Empty;
                    agency.OTPLoginStatus = !string.IsNullOrEmpty(dtAgency.Rows[0]["OTPLoginStatus"].ToString()) ? (dtAgency.Rows[0]["OTPLoginStatus"].ToString().Trim().ToLower() == "true" ? true : false) : false;
                    agency.PasswordChangeDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["PasswordChangeDate"].ToString()) ? dtAgency.Rows[0]["PasswordChangeDate"].ToString() : string.Empty;
                    agency.PasswordExpMsg = !string.IsNullOrEmpty(dtAgency.Rows[0]["PasswordExpMsg"].ToString()) ? (dtAgency.Rows[0]["PasswordExpMsg"].ToString().Trim().ToLower() == "true" ? true : false) : false;
                    agency.Branch = !string.IsNullOrEmpty(dtAgency.Rows[0]["Branch"].ToString()) ? dtAgency.Rows[0]["Branch"].ToString() : string.Empty;
                    agency.Currency = !string.IsNullOrEmpty(dtAgency.Rows[0]["currency"].ToString()) ? dtAgency.Rows[0]["currency"].ToString() : string.Empty;

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return agency;

        }
    }
}
