﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DataBaseLibrary.Common;

namespace DataBaseLibrary
{
    public static class ConnectToDataBase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(Config.MyAmdDBConnectionString);

        public static SqlConnection MyConDBConnection = new SqlConnection(Config.MyConConnectionString);

        public static SqlConnection HTLConnStrConnection = new SqlConnection(Config.HTLConnStrConnectionString);
        #endregion

        #region [Connection Open and Close]
        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        public static void MyConDBOpenConnection()
        {
            if (MyConDBConnection.State == ConnectionState.Closed)
            {
                MyConDBConnection.Open();
            }
        }
        public static void MyConDBCloseConnection()
        {
            if (MyConDBConnection.State == ConnectionState.Open)
            {
                MyConDBConnection.Close();
            }
        }
        public static void HTLConnStrOpenConnection()
        {
            if (HTLConnStrConnection.State == ConnectionState.Closed)
            {
                HTLConnStrConnection.Open();
            }
        }
        public static void HTLConnStrCloseConnection()
        {
            if (HTLConnStrConnection.State == ConnectionState.Open)
            {
                HTLConnStrConnection.Close();
            }
        }
        #endregion
    }
}
