﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using static ModelLibrary.WalletModel;

namespace DataBaseLibrary.Wallet
{
    public static class WalletLibraryHelper
    {
        public static List<UploadModel> GetUploadDetials(UploadModelFilter filter)
        {
            List<UploadModel> result = new List<UploadModel>();
            try
            {
                DataTable dtuploaddetails = WalletDataBase.GetUploadDetials(filter);

                if (dtuploaddetails != null && dtuploaddetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtuploaddetails.Rows.Count; i++)
                    {
                        UploadModel model = new UploadModel();
                        model.AgencyName = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["AgencyName"].ToString()) ? dtuploaddetails.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.AgencyID = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["AgencyID"].ToString()) ? dtuploaddetails.Rows[i]["AgencyID"].ToString() : string.Empty;
                        model.Amount = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["Amount"].ToString()) ? dtuploaddetails.Rows[i]["Amount"].ToString() : string.Empty;
                        model.ModeOfPayment = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["ModeOfPayment"].ToString()) ? dtuploaddetails.Rows[i]["ModeOfPayment"].ToString() : string.Empty;
                        model.BankName = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["BankName"].ToString()) ? dtuploaddetails.Rows[i]["BankName"].ToString() : string.Empty;
                        //model.BranchName = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["BranchName"].ToString()) ? dtuploaddetails.Rows[i]["BranchName"].ToString() : string.Empty;
                        //model.AccountNo = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["AccountNo"].ToString()) ? dtuploaddetails.Rows[i]["AccountNo"].ToString() : string.Empty;
                        model.ChequeNo = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["ChequeNo"].ToString()) ? dtuploaddetails.Rows[i]["ChequeNo"].ToString() : string.Empty;
                        model.ChequeDate = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["ChequeDate"].ToString()) ? dtuploaddetails.Rows[i]["ChequeDate"].ToString() : string.Empty;
                        model.TransactionID = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["TransactionID"].ToString()) ? dtuploaddetails.Rows[i]["TransactionID"].ToString() : string.Empty;
                        model.BankAreaCode = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["BankAreaCode"].ToString()) ? dtuploaddetails.Rows[i]["BankAreaCode"].ToString() : string.Empty;
                        model.DepositCity = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["DepositCity"].ToString()) ? dtuploaddetails.Rows[i]["DepositCity"].ToString() : string.Empty;
                        model.Remark = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["Remark"].ToString()) ? dtuploaddetails.Rows[i]["Remark"].ToString() : string.Empty;
                        model.RemarkByAccounts = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["RemarkByAccounts"].ToString()) ? dtuploaddetails.Rows[i]["RemarkByAccounts"].ToString() : string.Empty;
                        model.Status = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["Status"].ToString()) ? dtuploaddetails.Rows[i]["Status"].ToString() : string.Empty;
                        model.AccountID = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["AccountID"].ToString()) ? dtuploaddetails.Rows[i]["AccountID"].ToString() : string.Empty;
                        model.Date = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["Date"].ToString()) ? dtuploaddetails.Rows[i]["Date"].ToString() : string.Empty;
                        model.DepositeOffice = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["DepositeOffice"].ToString()) ? dtuploaddetails.Rows[i]["DepositeOffice"].ToString() : string.Empty;
                        model.ConcernPerson = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["ConcernPerson"].ToString()) ? dtuploaddetails.Rows[i]["ConcernPerson"].ToString() : string.Empty;
                        model.RecieptNo = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["RecieptNo"].ToString()) ? dtuploaddetails.Rows[i]["RecieptNo"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }

        public static List<UploadModel> GetCashInflowDetails(UploadModelFilter filter)
        {
            List<UploadModel> result = new List<UploadModel>();
            try
            {
                DataTable dtuploaddetails = WalletDataBase.GetCashInflowDetails(filter);

                if (dtuploaddetails != null && dtuploaddetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtuploaddetails.Rows.Count; i++)
                    {
                        UploadModel model = new UploadModel();
                        model.AgencyName = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["AgencyName"].ToString()) ? dtuploaddetails.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.AgencyID = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["AgencyID"].ToString()) ? dtuploaddetails.Rows[i]["AgencyID"].ToString() : string.Empty;
                        model.Amount = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["amount"].ToString()) ? dtuploaddetails.Rows[i]["amount"].ToString() : string.Empty;
                        model.InvoiceNo = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["InvoiceNo"].ToString()) ? dtuploaddetails.Rows[i]["InvoiceNo"].ToString() : string.Empty;
                        model.UploadType = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["UploadType"].ToString()) ? dtuploaddetails.Rows[i]["UploadType"].ToString() : string.Empty;
                        model.Remark = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["Remark"].ToString()) ? dtuploaddetails.Rows[i]["Remark"].ToString() : string.Empty;
                        model.UpdatedRemark = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["UpdatedRemark"].ToString()) ? dtuploaddetails.Rows[i]["UpdatedRemark"].ToString() : string.Empty;
                        model.CreatedDate = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["CreatedDate"].ToString()) ? dtuploaddetails.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        model.UpdatedDateTime = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["UpdatedDate"].ToString()) ? dtuploaddetails.Rows[i]["UpdatedDate"].ToString() : string.Empty;
                        model.AccountID = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["AccID"].ToString()) ? dtuploaddetails.Rows[i]["AccID"].ToString() : string.Empty;
                        model.AgentType = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["Agent_Type"].ToString()) ? dtuploaddetails.Rows[i]["Agent_Type"].ToString() : string.Empty;
                        model.SalesExecID = !string.IsNullOrEmpty(dtuploaddetails.Rows[i]["SalesExecID"].ToString()) ? dtuploaddetails.Rows[i]["SalesExecID"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<BankModel> GetDistributorBankDetails(BankModel model)
        {
            List<BankModel> result = new List<BankModel>();
            try
            {
                DataTable bankdetails = WalletDataBase.GetDistributorBankDetails(model);

                if (bankdetails != null && bankdetails.Rows.Count > 0)
                {
                    for (int i = 0; i < bankdetails.Rows.Count; i++)
                    {
                        BankModel bank = new BankModel();
                        bank.BankName = !string.IsNullOrEmpty(bankdetails.Rows[i]["BankName"].ToString()) ? bankdetails.Rows[i]["BankName"].ToString() : string.Empty;
                        bank.BranchName = !string.IsNullOrEmpty(bankdetails.Rows[i]["BranchName"].ToString()) ? bankdetails.Rows[i]["BranchName"].ToString() : string.Empty;
                        bank.Area = !string.IsNullOrEmpty(bankdetails.Rows[i]["Area"].ToString()) ? bankdetails.Rows[i]["Area"].ToString() : string.Empty;
                        bank.AccountNumber = !string.IsNullOrEmpty(bankdetails.Rows[i]["AccountNumber"].ToString()) ? bankdetails.Rows[i]["AccountNumber"].ToString() : string.Empty;
                        bank.CreatedDate = !string.IsNullOrEmpty(bankdetails.Rows[i]["CreatedDate"].ToString()) ? bankdetails.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        bank.NEFTCode = !string.IsNullOrEmpty(bankdetails.Rows[i]["NEFTCode"].ToString()) ? bankdetails.Rows[i]["NEFTCode"].ToString() : string.Empty;
                        bank.Counter = Convert.ToInt32(bankdetails.Rows[i]["Counter"]);
                        result.Add(bank);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static bool InsertBankdetials(BankModel model)
        {
            return WalletDataBase.InsertBankdetials(model);
        }
        public static bool InsertDepositDetail(UploadRequest model)
        {
            return WalletDataBase.InsertDepositDetail(model);
        }

        #region [Upload Request]
        public static List<UploadBankInformation> DepositeBankList(string agencyUserId)
        {
            List<UploadBankInformation> result = new List<UploadBankInformation>();

            try
            {
                DataTable dtBankDetails = WalletDataBase.DepositeBankList(agencyUserId);

                if (dtBankDetails != null && dtBankDetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBankDetails.Rows.Count; i++)
                    {
                        UploadBankInformation bank = new UploadBankInformation();

                        bank.BankName = !string.IsNullOrEmpty(dtBankDetails.Rows[i]["BankName"].ToString()) ? dtBankDetails.Rows[i]["BankName"].ToString() : string.Empty;
                        bank.BranchName = !string.IsNullOrEmpty(dtBankDetails.Rows[i]["BranchName"].ToString()) ? dtBankDetails.Rows[i]["BranchName"].ToString() : string.Empty;
                        bank.Area = !string.IsNullOrEmpty(dtBankDetails.Rows[i]["Area"].ToString()) ? dtBankDetails.Rows[i]["Area"].ToString() : string.Empty;
                        bank.AccountNumber = !string.IsNullOrEmpty(dtBankDetails.Rows[i]["AccountNumber"].ToString()) ? dtBankDetails.Rows[i]["AccountNumber"].ToString() : string.Empty;
                        bank.NEFTCode = !string.IsNullOrEmpty(dtBankDetails.Rows[i]["NEFTCode"].ToString()) ? dtBankDetails.Rows[i]["NEFTCode"].ToString() : string.Empty;

                        result.Add(bank);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static List<string> DepositeOfficeList(string agencyUserId)
        {
            List<string> result = new List<string>();

            try
            {
                DataTable dtOffice = WalletDataBase.DepositeOfficeList(agencyUserId);

                if (dtOffice != null && dtOffice.Rows.Count > 0)
                {
                    for (int i = 0; i < dtOffice.Rows.Count; i++)
                    {
                        result.Add(dtOffice.Rows[i]["OFFICE"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static List<string> DepositeAccountAndBranch(string actionType, string bank = null, string type = null)
        {
            List<string> result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(bank) && !string.IsNullOrEmpty(type))
                {
                    DataTable dtBankDetails = WalletDataBase.DepositeAccountAndBranch(bank, type);

                    if (dtBankDetails != null && dtBankDetails.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtBankDetails.Rows.Count; i++)
                        {
                            if (actionType == "branch")
                            {
                                result.Add(dtBankDetails.Rows[i]["BranchName"].ToString());
                            }
                            else
                            {
                                result.Add(dtBankDetails.Rows[i]["AccountNumber"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        #endregion
    }
}
