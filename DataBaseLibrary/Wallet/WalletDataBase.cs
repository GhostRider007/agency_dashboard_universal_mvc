﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using static ModelLibrary.WalletModel;

namespace DataBaseLibrary.Wallet
{
    public static class WalletDataBase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static DataTable GetUploadDetials(UploadModelFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("GetDepositDetailsWithdate", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FromDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PaymentMode", (!string.IsNullOrEmpty(filter.PaymentType) ? filter.PaymentType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentID", (!string.IsNullOrEmpty(filter.AgentID) ? filter.AgentID : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@SearchType", (!string.IsNullOrEmpty(filter.SearchType) ? filter.SearchType : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetDepositDetails");
                dataTable = dataSet.Tables["GetDepositDetails"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetCashInflowDetails(UploadModelFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("NewCashInflowDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;              
                sqlCommand.Parameters.Add(new SqlParameter("@FromDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : null)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : null)));
                sqlCommand.Parameters.Add(new SqlParameter("@agencyname", (!string.IsNullOrEmpty(filter.AgencyName) ? filter.AgencyName : null)));
                sqlCommand.Parameters.Add(new SqlParameter("@uploadtype", (!string.IsNullOrEmpty(filter.UploadType) ? filter.UploadType : null)));
                sqlCommand.Parameters.Add(new SqlParameter("@flowtype", (!string.IsNullOrEmpty(filter.flowType) ? filter.flowType : null)));
                sqlCommand.Parameters.Add(new SqlParameter("@accid", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@searchtype", (!string.IsNullOrEmpty(filter.SearchType) ? filter.SearchType : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetNewCashInflow");
                dataTable = dataSet.Tables["GetNewCashInflow"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetDistributorBankDetails(BankModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_DISTR_GETBANKDETAILSBYSTOCKIEST", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@STOCKIESTID", (!string.IsNullOrEmpty(model.Agentid) ? model.Agentid : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GETBANKDETAILS");
                dataTable = dataSet.Tables["GETBANKDETAILS"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static bool InsertBankdetials(BankModel model)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_INSERT_BANKDETIALS", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@BANKNAME", (!string.IsNullOrEmpty(model.BankName) ? model.BankName : string.Empty));
                sqlCommand.Parameters.AddWithValue("@BRANCHNAME", (!string.IsNullOrEmpty(model.BranchName) ? model.BranchName : string.Empty));
                sqlCommand.Parameters.AddWithValue("@AREA", (!string.IsNullOrEmpty(model.Area) ? model.Area : string.Empty));
                sqlCommand.Parameters.AddWithValue("@ACCNO", (!string.IsNullOrEmpty(model.AccountNumber) ? model.AccountNumber : string.Empty));
                sqlCommand.Parameters.AddWithValue("@IFSCCODE", (!string.IsNullOrEmpty(model.NEFTCode) ? model.NEFTCode : string.Empty));
                sqlCommand.Parameters.AddWithValue("@DISTRID", (!string.IsNullOrEmpty(model.Agentid) ? model.Agentid : string.Empty));
                sqlCommand.Parameters.AddWithValue("@OPERATION", (!string.IsNullOrEmpty(model.Operation) ? model.Operation : string.Empty));
                sqlCommand.Parameters.AddWithValue("@COUNTER", model.Counter);
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

       

        #region [Upload Request]
        public static DataTable DepositeBankList(string agencyId)
        {
            try
            {
                sqlCommand = new SqlCommand("BankInformation", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", agencyId));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "BankDetails");
                dataTable = dataSet.Tables["BankDetails"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable DepositeOfficeList(string agencyId)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_DEPOSITEOFFICE", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", agencyId));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "TBL_DEPOSITEOFFICE");
                dataTable = dataSet.Tables["TBL_DEPOSITEOFFICE"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable DepositeAccountAndBranch(string bank, string type)
        {
            try
            {
                sqlCommand = new SqlCommand("GetBranchAccount", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Bank", bank));
                sqlCommand.Parameters.Add(new SqlParameter("@Type", type));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "BankDetails");
                dataTable = dataSet.Tables["BankDetails"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static bool InsertDepositDetail(UploadRequest model)
        {
            try
            {
                sqlCommand = new SqlCommand("InsertDepositDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AgencyName", (!string.IsNullOrEmpty(model.AgencyName) ? model.AgencyName : string.Empty));
                sqlCommand.Parameters.AddWithValue("@AgencyID", (!string.IsNullOrEmpty(model.AgencyID) ? model.AgencyID : string.Empty));
                sqlCommand.Parameters.AddWithValue("@Amount", (!string.IsNullOrEmpty(model.Amount) ? model.Amount : string.Empty));
                sqlCommand.Parameters.AddWithValue("@ModeOfPayment", (!string.IsNullOrEmpty(model.PaymentMode) ? model.PaymentMode : string.Empty));
                sqlCommand.Parameters.AddWithValue("@BankName", (!string.IsNullOrEmpty(model.BankName) ? model.BankName : string.Empty));
                sqlCommand.Parameters.AddWithValue("@BranchName", (!string.IsNullOrEmpty(model.DepositeBranch) ? model.DepositeBranch : string.Empty));
                sqlCommand.Parameters.AddWithValue("@AccountNo", (!string.IsNullOrEmpty(model.DepositeAccountNo) ? model.DepositeAccountNo : string.Empty));
                sqlCommand.Parameters.AddWithValue("@ChequeNo", (!string.IsNullOrEmpty(model.ChequeNumber) ? model.ChequeNumber : string.Empty));
                sqlCommand.Parameters.AddWithValue("@ChequeDate", (!string.IsNullOrEmpty(model.ChequeDate) ? model.ChequeDate : string.Empty));
                sqlCommand.Parameters.AddWithValue("@TransactionID", (!string.IsNullOrEmpty(model.TransactionID) ? model.TransactionID : string.Empty));
                sqlCommand.Parameters.AddWithValue("@BankAreaCode", (!string.IsNullOrEmpty(model.ReferenceNumber) ? model.ReferenceNumber : string.Empty));
                sqlCommand.Parameters.AddWithValue("@DepositCity", (!string.IsNullOrEmpty(model.DepositCity) ? model.DepositCity : string.Empty));
                sqlCommand.Parameters.AddWithValue("@DepositeDate", (!string.IsNullOrEmpty(model.DepositDate) ? model.DepositDate : string.Empty));
                sqlCommand.Parameters.AddWithValue("@Remark", (!string.IsNullOrEmpty(model.Remark) ? model.Remark : string.Empty));
                sqlCommand.Parameters.AddWithValue("@Status", (!string.IsNullOrEmpty(model.Status) ? model.Status : string.Empty));
                sqlCommand.Parameters.AddWithValue("@UploadType", (!string.IsNullOrEmpty(model.UploadeRequestType) ? model.UploadeRequestType : string.Empty));
                sqlCommand.Parameters.AddWithValue("@DepositeOffice", (!string.IsNullOrEmpty(model.DepositeOffice) ? model.DepositeOffice : string.Empty));
                sqlCommand.Parameters.AddWithValue("@ConcernPerson", (!string.IsNullOrEmpty(model.ConcernPerson) ? model.ConcernPerson : string.Empty));
                sqlCommand.Parameters.AddWithValue("@RecieptNo", (!string.IsNullOrEmpty(model.ReceiptNumber) ? model.ReceiptNumber : string.Empty));
                sqlCommand.Parameters.AddWithValue("@BranchCode", (!string.IsNullOrEmpty(model.BranchCode) ? model.BranchCode : string.Empty));
                sqlCommand.Parameters.AddWithValue("@AgentBankName", (!string.IsNullOrEmpty(model.AgentBankName) ? model.AgentBankName : string.Empty));
                sqlCommand.Parameters.AddWithValue("@SalesExecID", (!string.IsNullOrEmpty(model.SalesExecID) ? model.SalesExecID : string.Empty));
                sqlCommand.Parameters.AddWithValue("@AgentType", (!string.IsNullOrEmpty(model.AgentType) ? model.AgentType : string.Empty));
                sqlCommand.Parameters.AddWithValue("@OTPRefNo", (!string.IsNullOrEmpty(model.OTPRefNo) ? model.OTPRefNo : string.Empty));
                sqlCommand.Parameters.AddWithValue("@LoginByOTP", (!string.IsNullOrEmpty(model.LoginByOTP) ? model.LoginByOTP : string.Empty));
                sqlCommand.Parameters.AddWithValue("@OTPId", (!string.IsNullOrEmpty(model.OTPId) ? model.OTPId : string.Empty));
                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        #endregion
    }
}
