﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using static ModelLibrary.AccountModel;

namespace DataBaseLibrary.Account
{
    public static class AccountLibraryHelper
    {
        public static List<AccountLedgerReport> GetLedgerDetail(AccountLedgerFilter filter)
        {
            List<AccountLedgerReport> result = new List<AccountLedgerReport>();
            try
            {
                DataTable dtledgerdetails = AccountDataBase.GetLedgerDetail(filter);

                if (dtledgerdetails != null && dtledgerdetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtledgerdetails.Rows.Count; i++)
                    {
                        AccountLedgerReport model = new AccountLedgerReport();
                        model.AgencyID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgencyID"].ToString()) ? dtledgerdetails.Rows[i]["AgencyID"].ToString() : string.Empty;
                        model.InvoicecopyLink = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["InvoiceLink"].ToString()) ? dtledgerdetails.Rows[i]["InvoiceLink"].ToString() : string.Empty;
                        model.TicketcopyLink = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Link"].ToString()) ? dtledgerdetails.Rows[i]["Link"].ToString() : string.Empty;
                        model.AgentID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgentID"].ToString()) ? dtledgerdetails.Rows[i]["AgentID"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgencyName"].ToString()) ? dtledgerdetails.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.InvoiceNo = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["InvoiceNo"].ToString()) ? dtledgerdetails.Rows[i]["InvoiceNo"].ToString() : string.Empty;
                        model.PnrNo = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["PnrNo"].ToString()) ? dtledgerdetails.Rows[i]["PnrNo"].ToString() : string.Empty;
                        model.Aircode = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Aircode"].ToString()) ? dtledgerdetails.Rows[i]["Aircode"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TicketNo"].ToString()) ? dtledgerdetails.Rows[i]["TicketNo"].ToString() : string.Empty;
                        model.TicketingCarrier = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TicketingCarrier"].ToString()) ? dtledgerdetails.Rows[i]["TicketingCarrier"].ToString() : string.Empty;
                        model.YatraAccountID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["YatraAccountID"].ToString()) ? dtledgerdetails.Rows[i]["YatraAccountID"].ToString() : string.Empty;
                        model.AccountID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AccountID"].ToString()) ? dtledgerdetails.Rows[i]["AccountID"].ToString() : string.Empty;
                        model.ExecutiveID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["ExecutiveID"].ToString()) ? dtledgerdetails.Rows[i]["ExecutiveID"].ToString() : string.Empty;
                        model.Debit = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Debit"].ToString()) ? dtledgerdetails.Rows[i]["Debit"].ToString() : string.Empty;
                        model.Credit = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Credit"].ToString()) ? dtledgerdetails.Rows[i]["Credit"].ToString() : string.Empty;
                        model.Aval_Balance = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Aval_Balance"].ToString()) ? dtledgerdetails.Rows[i]["Aval_Balance"].ToString() : string.Empty;
                        model.CreateDate = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["CreatedDate"].ToString()) ? dtledgerdetails.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        model.BookingType = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["BookingType"].ToString()) ? dtledgerdetails.Rows[i]["BookingType"].ToString() : string.Empty;
                        model.Remark = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Remark"].ToString()) ? dtledgerdetails.Rows[i]["Remark"].ToString() : string.Empty;
                        model.C = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["C"].ToString()) ? dtledgerdetails.Rows[i]["C"].ToString() : string.Empty;
                        model.D = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["D"].ToString()) ? dtledgerdetails.Rows[i]["D"].ToString() : string.Empty;
                        model.PaymentMode = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["PaymentMode"].ToString()) ? dtledgerdetails.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        model.DueAmount = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["DueAmount"].ToString()) ? dtledgerdetails.Rows[i]["DueAmount"].ToString() : string.Empty;
                        model.CreditLimit = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["CreditLimit"].ToString()) ? dtledgerdetails.Rows[i]["CreditLimit"].ToString() : string.Empty;
                        model.TransType = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TransType"].ToString()) ? dtledgerdetails.Rows[i]["TransType"].ToString() : string.Empty;
                        model.DistrAgencyID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["DistrAgencyID"].ToString()) ? dtledgerdetails.Rows[i]["DistrAgencyID"].ToString() : string.Empty;
                        model.PaidStatus = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["PaidStatus"].ToString()) ? dtledgerdetails.Rows[i]["PaidStatus"].ToString() : string.Empty;
                        model.LogoUrl = !string.IsNullOrEmpty(model.TicketingCarrier) ? "http://b2brichatravels.in/AirLogo/sm" + model.TicketingCarrier + ".gif" : "";
                        result.Add(model);
                    }
                }
                //    DataTable closingbalance = AccountDataBase.GetLedgerDetail(filter);
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static AccountLedgerReport GetClosingbal(AccountLedgerFilter filter)
        {
            AccountLedgerReport result = new AccountLedgerReport();

            try
            {
                filter.SearchType = "CLOSINGBAL";
                DataTable dtledgerdetails = AccountDataBase.GetLedgerDetail(filter);
                if (dtledgerdetails != null && dtledgerdetails.Rows.Count > 0)
                {
                    result.ClosingBalance = !string.IsNullOrEmpty(dtledgerdetails.Rows[0]["ClosingBalance"].ToString()) ? dtledgerdetails.Rows[0]["ClosingBalance"].ToString() : string.Empty;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static AccountLedgerReport GetInvoice(string orderid)
        {
            AccountLedgerReport result = new AccountLedgerReport();
            string AddressType = "";
            try
            {
                DataSet dsResult = AccountDataBase.GetInvoice(orderid);
                if (dsResult != null)
                {
                    DataTable dtInvoicdetails = dsResult.Tables[0];
                    DataTable dtflt = new DataTable();
                    if (dsResult.Tables.Count > 1)
                    {
                        dtflt = dsResult.Tables[1];
                    }

                    result.OrderId = orderid;
                    result.PaxType = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["PaxType"].ToString()) ? dtInvoicdetails.Rows[0]["PaxType"].ToString() : string.Empty;
                    result.Title = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["Title"].ToString()) ? dtInvoicdetails.Rows[0]["Title"].ToString() : string.Empty;
                    result.FName = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["FName"].ToString()) ? dtInvoicdetails.Rows[0]["FName"].ToString() : string.Empty;
                    result.LName = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["LName"].ToString()) ? dtInvoicdetails.Rows[0]["LName"].ToString() : string.Empty;
                    result.TicketNumber = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["TicketNumber"].ToString()) ? dtInvoicdetails.Rows[0]["TicketNumber"].ToString() : string.Empty;
                    result.GdsPnr = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["GdsPnr"].ToString()) ? dtInvoicdetails.Rows[0]["GdsPnr"].ToString() : string.Empty;
                    result.Aircode = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["AirlinePnr"].ToString()) ? dtInvoicdetails.Rows[0]["AirlinePnr"].ToString() : string.Empty;
                    result.Sector = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["sector"].ToString()) ? dtInvoicdetails.Rows[0]["sector"].ToString() : string.Empty;
                    result.CreateDate = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["CreateDate"].ToString()) ? dtInvoicdetails.Rows[0]["CreateDate"].ToString() : string.Empty;
                    result.ResuID = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ResuID"].ToString()) ? dtInvoicdetails.Rows[0]["ResuID"].ToString() : string.Empty;
                    result.ResuCharge = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ResuCharge"].ToString()) ? dtInvoicdetails.Rows[0]["ResuCharge"].ToString() : string.Empty;
                    result.ResuServiseCharge = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ResuServiseCharge"].ToString()) ? dtInvoicdetails.Rows[0]["ResuServiseCharge"].ToString() : string.Empty;
                    result.ResuFareDiff = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ResuFareDiff"].ToString()) ? dtInvoicdetails.Rows[0]["ResuFareDiff"].ToString() : string.Empty;
                    result.BaseFare = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["BaseFare"].ToString()) ? dtInvoicdetails.Rows[0]["BaseFare"].ToString() : string.Empty;
                    result.TotalTax = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["TotalTax"].ToString()) ? dtInvoicdetails.Rows[0]["TotalTax"].ToString() : string.Empty;
                    result.TotalFare = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["TotalFare"].ToString()) ? dtInvoicdetails.Rows[0]["TotalFare"].ToString() : string.Empty;
                    result.ServiceTax = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ServiceTax"].ToString()) ? dtInvoicdetails.Rows[0]["ServiceTax"].ToString() : string.Empty;
                    result.Tds = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["Tds"].ToString()) ? dtInvoicdetails.Rows[0]["Tds"].ToString() : string.Empty;
                    result.AdminMrk = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["AdminMrk"].ToString()) ? dtInvoicdetails.Rows[0]["AdminMrk"].ToString() : string.Empty;
                    result.AgentMrk = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["AgentMrk"].ToString()) ? dtInvoicdetails.Rows[0]["AgentMrk"].ToString() : string.Empty;
                    result.CashBack = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["CashBack"].ToString()) ? dtInvoicdetails.Rows[0]["CashBack"].ToString() : string.Empty;
                    result.TotalDiscount = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["TotalDiscount"].ToString()) ? dtInvoicdetails.Rows[0]["TotalDiscount"].ToString() : string.Empty;
                    result.Discount = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["Discount"].ToString()) ? dtInvoicdetails.Rows[0]["Discount"].ToString() : string.Empty;
                    result.TranFee = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["TranFee"].ToString()) ? dtInvoicdetails.Rows[0]["TranFee"].ToString() : string.Empty;
                    result.ProjectID = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ProjectID"].ToString()) ? dtInvoicdetails.Rows[0]["ProjectID"].ToString() : string.Empty;
                    result.MgtFee = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["MgtFee"].ToString()) ? dtInvoicdetails.Rows[0]["MgtFee"].ToString() : string.Empty;
                    result.VC = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["VC"].ToString()) ? dtInvoicdetails.Rows[0]["VC"].ToString() : string.Empty;
                    result.BillNoCorp = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["BillNoCorp"].ToString()) ? dtInvoicdetails.Rows[0]["BillNoCorp"].ToString() : string.Empty;
                    result.BookedBy = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["BookedBy"].ToString()) ? dtInvoicdetails.Rows[0]["BookedBy"].ToString() : string.Empty;
                    result.AgentID = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["AgentId"].ToString()) ? dtInvoicdetails.Rows[0]["AgentId"].ToString() : string.Empty;
                    result.SCSRVTAX = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["SCSRVTAX"].ToString()) ? dtInvoicdetails.Rows[0]["SCSRVTAX"].ToString() : string.Empty;
                    if (dtflt != null && dtflt.Rows.Count > 0)
                    {
                        result.DepDate = !string.IsNullOrEmpty(dtflt.Rows[0]["DepDate"].ToString()) ? dtflt.Rows[0]["DepDate"].ToString() : string.Empty;
                        result.FltNumber = !string.IsNullOrEmpty(dtflt.Rows[0]["FltNumber"].ToString()) ? dtflt.Rows[0]["FltNumber"].ToString() : string.Empty;
                    }
                }
                DataTable dtAgecnydetails = AccountDataBase.GetAgencyDetails(result.AgentID);
                if (dtAgecnydetails != null && dtAgecnydetails.Rows.Count > 0)
                {
                    result.AgencyName = !string.IsNullOrEmpty(dtAgecnydetails.Rows[0]["Agency_Name"].ToString()) ? dtAgecnydetails.Rows[0]["Agency_Name"].ToString() : string.Empty;
                    result.AgencyAddress = !string.IsNullOrEmpty(dtAgecnydetails.Rows[0]["Address"].ToString()) ? dtAgecnydetails.Rows[0]["Address"].ToString() : string.Empty;
                    result.AgencyCity = !string.IsNullOrEmpty(dtAgecnydetails.Rows[0]["City"].ToString()) ? dtAgecnydetails.Rows[0]["City"].ToString() : string.Empty;
                    result.AgencyZipcode = !string.IsNullOrEmpty(dtAgecnydetails.Rows[0]["Zipcode"].ToString()) ? dtAgecnydetails.Rows[0]["Zipcode"].ToString() : string.Empty;
                    result.AgencyState = !string.IsNullOrEmpty(dtAgecnydetails.Rows[0]["State"].ToString()) ? dtAgecnydetails.Rows[0]["State"].ToString() : string.Empty;
                    result.AgencyCountry = !string.IsNullOrEmpty(dtAgecnydetails.Rows[0]["Country"].ToString()) ? dtAgecnydetails.Rows[0]["Country"].ToString() : string.Empty;
                    result.IsCorp = !string.IsNullOrEmpty(dtAgecnydetails.Rows[0]["IsCorp"].ToString()) ? dtAgecnydetails.Rows[0]["IsCorp"].ToString() : string.Empty;
                }
                if (!string.IsNullOrEmpty(result.IsCorp))
                {
                    AddressType = "CORP";
                }
                else
                {
                    AddressType = "FWU";
                }
                DataTable dtcompneydetails = AccountDataBase.GetCompanyAddress(AddressType);
                if (dtcompneydetails != null && dtcompneydetails.Rows.Count > 0)
                {
                    result.COMPANYGST = !string.IsNullOrEmpty(dtcompneydetails.Rows[0]["GST"].ToString()) ? dtcompneydetails.Rows[0]["PaxType"].ToString() : string.Empty;
                    result.COMPANYNAME = !string.IsNullOrEmpty(dtcompneydetails.Rows[0]["COMPANYNAME"].ToString()) ? dtcompneydetails.Rows[0]["COMPANYNAME"].ToString() : string.Empty;
                    result.COMPANYADDRESS = !string.IsNullOrEmpty(dtcompneydetails.Rows[0]["COMPANYADDRESS"].ToString()) ? dtcompneydetails.Rows[0]["COMPANYADDRESS"].ToString() : string.Empty;
                    result.PHONE = !string.IsNullOrEmpty(dtcompneydetails.Rows[0]["PHONENO"].ToString()) ? dtcompneydetails.Rows[0]["PHONENO"].ToString() : string.Empty;
                    result.MOBILENO = !string.IsNullOrEmpty(dtcompneydetails.Rows[0]["MOBILENO"].ToString()) ? dtcompneydetails.Rows[0]["MOBILENO"].ToString() : string.Empty;
                    result.COMPANYEMAIL = !string.IsNullOrEmpty(dtcompneydetails.Rows[0]["EMAIL"].ToString()) ? dtcompneydetails.Rows[0]["EMAIL"].ToString() : string.Empty;
                    result.PANNO = !string.IsNullOrEmpty(dtcompneydetails.Rows[0]["PANNO"].ToString()) ? dtcompneydetails.Rows[0]["PANNO"].ToString() : string.Empty;
                    result.FAX = !string.IsNullOrEmpty(dtcompneydetails.Rows[0]["FAX"].ToString()) ? dtcompneydetails.Rows[0]["FAX"].ToString() : string.Empty;

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static AccountLedgerReport GetAgencyDetails(string userId)
        {
            AccountLedgerReport result = new AccountLedgerReport();

            try
            {


            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static AccountLedgerReport GetCompanyAddress(string AddressType)
        {
            AccountLedgerReport result = new AccountLedgerReport();

            try
            {
                DataTable dtInvoicdetails = AccountDataBase.GetCompanyAddress(AddressType);
                //if (dtInvoicdetails != null && dtInvoicdetails.Rows.Count > 0)
                //{
                //    result.PaxType = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["PaxType"].ToString()) ? dtInvoicdetails.Rows[0]["PaxType"].ToString() : string.Empty;
                //    result.Title = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["Title"].ToString()) ? dtInvoicdetails.Rows[0]["Title"].ToString() : string.Empty;
                //    result.FName = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["FName"].ToString()) ? dtInvoicdetails.Rows[0]["FName"].ToString() : string.Empty;
                //    result.LName = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["LName"].ToString()) ? dtInvoicdetails.Rows[0]["LName"].ToString() : string.Empty;
                //    result.TicketNumber = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["TicketNumber"].ToString()) ? dtInvoicdetails.Rows[0]["TicketNumber"].ToString() : string.Empty;
                //    result.GdsPnr = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["GdsPnr"].ToString()) ? dtInvoicdetails.Rows[0]["GdsPnr"].ToString() : string.Empty;
                //    result.Aircode = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["AirlinePnr"].ToString()) ? dtInvoicdetails.Rows[0]["AirlinePnr"].ToString() : string.Empty;
                //    result.Sector = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["sector"].ToString()) ? dtInvoicdetails.Rows[0]["sector"].ToString() : string.Empty;
                //    result.CreateDate = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["CreateDate"].ToString()) ? dtInvoicdetails.Rows[0]["CreateDate"].ToString() : string.Empty;
                //    result.ResuID = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ResuID"].ToString()) ? dtInvoicdetails.Rows[0]["ResuID"].ToString() : string.Empty;
                //    result.ResuCharge = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ResuCharge"].ToString()) ? dtInvoicdetails.Rows[0]["ResuCharge"].ToString() : string.Empty;
                //    result.ResuServiseCharge = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ResuServiseCharge"].ToString()) ? dtInvoicdetails.Rows[0]["ResuServiseCharge"].ToString() : string.Empty;
                //    result.ResuFareDiff = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["ResuFareDiff"].ToString()) ? dtInvoicdetails.Rows[0]["ResuFareDiff"].ToString() : string.Empty;
                //    result.BaseFare = !string.IsNullOrEmpty(dtInvoicdetails.Rows[0]["BaseFare"].ToString()) ? dtInvoicdetails.Rows[0]["BaseFare"].ToString() : string.Empty;                
                //}

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<AccountLedgerReport> GetStaffTransaction(AccountLedgerFilter filter)
        {
            List<AccountLedgerReport> result = new List<AccountLedgerReport>();
            try
            {
                DataTable dtledgerdetails = AccountDataBase.GetStaffTransaction(filter);

                if (dtledgerdetails != null && dtledgerdetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtledgerdetails.Rows.Count; i++)
                    {
                        AccountLedgerReport model = new AccountLedgerReport();
                        model.Id = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Id"].ToString()) ? dtledgerdetails.Rows[i]["Id"].ToString() : string.Empty;
                        model.OrderId = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["OrderId"].ToString()) ? dtledgerdetails.Rows[i]["OrderId"].ToString() : string.Empty;
                        model.CreatedBy = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["CreatedBy"].ToString()) ? dtledgerdetails.Rows[i]["CreatedBy"].ToString() : string.Empty;
                        model.ServiceType = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["ServiceType"].ToString()) ? dtledgerdetails.Rows[i]["ServiceType"].ToString() : string.Empty;
                        model.TransAmount = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TransAmount"].ToString()) ? dtledgerdetails.Rows[i]["TransAmount"].ToString() : string.Empty;
                        model.Debit = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Debit"].ToString()) ? dtledgerdetails.Rows[i]["Debit"].ToString() : string.Empty;
                        model.Credit = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Credit"].ToString()) ? dtledgerdetails.Rows[i]["Credit"].ToString() : string.Empty;
                        model.AvalBal = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AvalBal"].ToString()) ? dtledgerdetails.Rows[i]["AvalBal"].ToString() : string.Empty;
                        model.StaffUserId = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["StaffUserId"].ToString()) ? dtledgerdetails.Rows[i]["StaffUserId"].ToString() : string.Empty;
                        model.StaffId = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["StaffId"].ToString()) ? dtledgerdetails.Rows[i]["StaffId"].ToString() : string.Empty;
                        model.AgentLimit = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgentLimit"].ToString()) ? dtledgerdetails.Rows[i]["AgentLimit"].ToString() : string.Empty;
                        model.DueAmount = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["DueAmount"].ToString()) ? dtledgerdetails.Rows[i]["DueAmount"].ToString() : string.Empty;
                        model.OwnerId = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["OwnerId"].ToString()) ? dtledgerdetails.Rows[i]["OwnerId"].ToString() : string.Empty;
                        model.AgencyID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgencyId"].ToString()) ? dtledgerdetails.Rows[i]["AgencyId"].ToString() : string.Empty;
                        model.DistrID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["DistrID"].ToString()) ? dtledgerdetails.Rows[i]["DistrID"].ToString() : string.Empty;
                        model.Mobile = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Mobile"].ToString()) ? dtledgerdetails.Rows[i]["Mobile"].ToString() : string.Empty;
                        model.Email = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Email"].ToString()) ? dtledgerdetails.Rows[i]["Email"].ToString() : string.Empty;
                        model.AgentMobile = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgentMobile"].ToString()) ? dtledgerdetails.Rows[i]["AgentMobile"].ToString() : string.Empty;
                        model.Remark = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Remark"].ToString()) ? dtledgerdetails.Rows[i]["Remark"].ToString() : string.Empty;
                        model.CreatedDate = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["CreatedDate"].ToString()) ? dtledgerdetails.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        model.UserType = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["UserType"].ToString()) ? dtledgerdetails.Rows[i]["UserType"].ToString() : string.Empty;
                        model.Module = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Module"].ToString()) ? dtledgerdetails.Rows[i]["Module"].ToString() : string.Empty;
                        model.PaymentLog = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["PaymentLog"].ToString()) ? dtledgerdetails.Rows[i]["PaymentLog"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<AccountLedgerReport> IntGetInvoice(AccountLedgerFilter filter)
        {
            List<AccountLedgerReport> result = new List<AccountLedgerReport>();
            try
            {
                DataTable dtledgerdetails = AccountDataBase.IntGetInvoice(filter);

                if (dtledgerdetails != null && dtledgerdetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtledgerdetails.Rows.Count; i++)
                    {
                        AccountLedgerReport model = new AccountLedgerReport();
                        model.EasyID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["EasyID"].ToString()) ? dtledgerdetails.Rows[i]["EasyID"].ToString() : string.Empty;
                        model.EasyTransID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["EasyTransNo"].ToString()) ? dtledgerdetails.Rows[i]["EasyTransNo"].ToString() : string.Empty;
                        model.AgentType = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgentType"].ToString()) ? dtledgerdetails.Rows[i]["AgentType"].ToString() : string.Empty;
                        model.GdsPnr = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["GdsPnr"].ToString()) ? dtledgerdetails.Rows[i]["GdsPnr"].ToString() : string.Empty;
                        model.AirLinePnr = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AirLinePnr"].ToString()) ? dtledgerdetails.Rows[i]["AirLinePnr"].ToString() : string.Empty;
                        model.MgtFee = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["MgtFee"].ToString()) ? dtledgerdetails.Rows[i]["MgtFee"].ToString() : string.Empty;
                        model.JourneyDate = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["JourneyDate"].ToString()) ? dtledgerdetails.Rows[i]["JourneyDate"].ToString() : string.Empty;
                        model.Date = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Date"].ToString()) ? dtledgerdetails.Rows[i]["Date"].ToString() : string.Empty;
                        model.Time = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Time"].ToString()) ? dtledgerdetails.Rows[i]["Time"].ToString() : string.Empty;
                        model.Sector = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["sector"].ToString()) ? dtledgerdetails.Rows[i]["sector"].ToString() : string.Empty;
                        model.VC = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["VC"].ToString()) ? dtledgerdetails.Rows[i]["VC"].ToString() : string.Empty;
                        model.CreateDate = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["CreateDate"].ToString()) ? dtledgerdetails.Rows[i]["CreateDate"].ToString() : string.Empty;
                        model.Title = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Title"].ToString()) ? dtledgerdetails.Rows[i]["Title"].ToString() : string.Empty;
                        model.FName = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["FName"].ToString()) ? dtledgerdetails.Rows[i]["FName"].ToString() : string.Empty;
                        model.LName = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["LName"].ToString()) ? dtledgerdetails.Rows[i]["LName"].ToString() : string.Empty;
                        model.PaxType = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["PaxType"].ToString()) ? dtledgerdetails.Rows[i]["PaxType"].ToString() : string.Empty;
                        model.AirLine = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AirLine"].ToString()) ? dtledgerdetails.Rows[i]["AirLine"].ToString() : string.Empty;
                        model.TicketNumber = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TicketNumber"].ToString()) ? dtledgerdetails.Rows[i]["TicketNumber"].ToString() : string.Empty;
                        model.BaseFare = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["BaseFare"].ToString()) ? dtledgerdetails.Rows[i]["BaseFare"].ToString() : string.Empty;
                        model.YQ = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["YQ"].ToString()) ? dtledgerdetails.Rows[i]["YQ"].ToString() : string.Empty;
                        model.TotalTax = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TotalTax"].ToString()) ? dtledgerdetails.Rows[i]["TotalTax"].ToString() : string.Empty;
                        model.ServiceTax = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["ServiceTax"].ToString()) ? dtledgerdetails.Rows[i]["ServiceTax"].ToString() : string.Empty;
                        model.TranFee = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TranFee"].ToString()) ? dtledgerdetails.Rows[i]["TranFee"].ToString() : string.Empty;
                        model.Discount = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Discount"].ToString()) ? dtledgerdetails.Rows[i]["Discount"].ToString() : string.Empty;
                        model.TotalDiscount = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TotalDiscount"].ToString()) ? dtledgerdetails.Rows[i]["TotalDiscount"].ToString() : string.Empty;
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TotalAfterDis"].ToString()) ? dtledgerdetails.Rows[i]["TotalAfterDis"].ToString() : string.Empty;
                        model.Tds = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["Tds"].ToString()) ? dtledgerdetails.Rows[i]["Tds"].ToString() : string.Empty;
                        model.ORIGINALDISCOUNT = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["ORIGINALDISCOUNT"].ToString()) ? dtledgerdetails.Rows[i]["ORIGINALDISCOUNT"].ToString() : string.Empty;
                        model.GST = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["GST"].ToString()) ? dtledgerdetails.Rows[i]["GST"].ToString() : string.Empty;
                        model.OrderId = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["OrderId"].ToString()) ? dtledgerdetails.Rows[i]["OrderId"].ToString() : string.Empty;
                        model.AgentID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgentID"].ToString()) ? dtledgerdetails.Rows[i]["AgentID"].ToString() : string.Empty;
                        model.AgencyID = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgencyId"].ToString()) ? dtledgerdetails.Rows[i]["AgencyId"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["AgencyName"].ToString()) ? dtledgerdetails.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.PaymentMode = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["PaymentMode"].ToString()) ? dtledgerdetails.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        model.TotalFare = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["TotalFare"].ToString()) ? dtledgerdetails.Rows[i]["TotalFare"].ToString() : string.Empty;
                        model.PaxType = !string.IsNullOrEmpty(dtledgerdetails.Rows[i]["PaxType"].ToString()) ? dtledgerdetails.Rows[i]["PaxType"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<State> StateList(string country)
        {
            List<State> stateList = new List<State>();

            try
            {
                DataTable dtState = AccountDataBase.StateList(country);
                if (dtState != null && dtState.Rows.Count > 0)
                {
                    for (int i = 0; i < dtState.Rows.Count; i++)
                    {
                        State state = new State();
                        state.StateCode = dtState.Rows[i]["Code"].ToString();
                        state.StateName = dtState.Rows[i]["Name"].ToString();
                        stateList.Add(state);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return stateList;
        }
        public static List<City> CityList(string stateID)
        {
            List<City> cityList = new List<City>();

            try
            {
                DataTable dtCity = AccountDataBase.CityList(stateID);
                if (dtCity != null && dtCity.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCity.Rows.Count; i++)
                    {
                        City city = new City();
                        city.CityCode = dtCity.Rows[i]["CITY"].ToString();
                        city.CityName = dtCity.Rows[i]["CITY"].ToString();
                        cityList.Add(city);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return cityList;
        }
    }

}
