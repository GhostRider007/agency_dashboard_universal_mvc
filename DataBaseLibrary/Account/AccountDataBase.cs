﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using static ModelLibrary.AccountModel;

namespace DataBaseLibrary.Account
{
    public static class AccountDataBase
    {
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static DataTable AgentStaffLogin(AgentStaffLogin login, ref string loginType)
        {
            try
            {
                //IPAddress = UserControl.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                //if (IPAddress == "" || IPAddress == null)
                //    IPAddress = UserControl.Request.ServerVariables["REMOTE_ADDR"];


                sqlCommand = new SqlCommand("SP_AGENT_STAFFLOGIN", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", login.UserId);
                sqlCommand.Parameters.AddWithValue("@Password", login.Password);
                sqlCommand.Parameters.AddWithValue("@IPAddress", login.IpAddress);
                sqlCommand.Parameters.AddWithValue("@ActionType", "GETUSERTYPE");
                sqlCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                sqlCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "AgentStaffRegister");
                dataTable = dataSet.Tables["AgentStaffRegister"];

                loginType = Convert.ToString(sqlCommand.Parameters["@Msg"].Value).ToUpper();
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
        public static DataSet User_Auth(string uid, string passwd)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlDataAdapter = new SqlDataAdapter("UserLoginNew", ConnectToDataBase.MyAmdDBConnection);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@uid", uid);
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@pwd", passwd);

                sqlDataAdapter.Fill(ds);
            }
            catch (Exception ex)
            {
                //clsErrorLog.LogInfo(ex);
                ex.ToString();
            }

            return ds;
        }
        public static DataTable GetAgencyDetails(string userId)
        {
            try
            {
                sqlCommand = new SqlCommand("AgencyDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("UserId", userId);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "agent_register");
                dataTable = dataSet.Tables["agent_register"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetCompanyAddress(string AddressType)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_GETADDRESS", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TYPE", AddressType);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "GETADDRESS");
                dataTable = dataSet.Tables["GETADDRESS"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataSet LastLoginTime(string uid)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlDataAdapter = new SqlDataAdapter("Sp_Tbl_UserLoginTime", ConnectToDataBase.MyAmdDBConnection);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@userID", uid);

                sqlDataAdapter.Fill(ds);
            }
            catch (Exception ex)
            {
                ex.ToString();
                //clsErrorLog.LogInfo(ex);
            }

            return ds;
        }
        public static bool InsertLoginTime(string uid, string ipAddress)
        {
            try
            {
                sqlCommand = new SqlCommand("Sp_Tbl_UserLoginTime_Insert", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@userID", uid);
                sqlCommand.Parameters.AddWithValue("@IPAdress", ipAddress);

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static DataTable ForgetPassword(ForgetPassword forget)
        {
            try
            {
                sqlCommand = new SqlCommand("CheckForgotPassword", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UID", forget.UserId);
                sqlCommand.Parameters.AddWithValue("@Email", forget.Email);
                sqlCommand.Parameters.AddWithValue("@Mobile", forget.Mobile);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();

                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataSet, "agent_register");
                dataTable = dataSet.Tables["agent_register"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable GetLedgerDetail(AccountLedgerFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("GetLedgerDetail", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@BookingType", (!string.IsNullOrEmpty(filter.BookingType) ? filter.BookingType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@SearchType", (!string.IsNullOrEmpty(filter.SearchType) ? filter.SearchType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Price", (!string.IsNullOrEmpty(filter.Price) ? filter.Price : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetLedger");
                dataTable = dataSet.Tables["GetLedger"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataSet GetInvoice(string orderid)
        {
            dataSet = new DataSet();
            try
            {
                sqlCommand = new SqlCommand("GetInvoiceDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@orderid", orderid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                sqlDataAdapter.Fill(dataSet, "GetInvoice");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }
        public static DataTable GetStaffTransaction(AccountLedgerFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("GetStaffTransaction", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserType", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@StaffUserId", (!string.IsNullOrEmpty(filter.StaffUserId) ? filter.StaffUserId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Module", (!string.IsNullOrEmpty(filter.Module) ? filter.Module : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ServiceType", (!string.IsNullOrEmpty(filter.ServiceType) ? filter.ServiceType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@SearchType", (!string.IsNullOrEmpty(filter.SearchType) ? filter.SearchType : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "GetTransaction");
                dataTable = dataSet.Tables["GetTransaction"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataTable IntGetInvoice(AccountLedgerFilter filter)
        {
            try
            {
                sqlCommand = new SqlCommand("IntSelectInvoiceDetails", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@TicketNo", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Airline", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ProjectID", (!string.IsNullOrEmpty(filter.ProjectID) ? filter.ProjectID : string.Empty)));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "SelectInvoice");
                dataTable = dataSet.Tables["SelectInvoice"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static bool UpdateAgentProfileDetails(AgentUpdateProfile profile)
        {
            try
            {
                sqlCommand = new SqlCommand("UpdateAgentProfile", ConnectToDataBase.MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Type", profile.Type);
                sqlCommand.Parameters.AddWithValue("@UID", profile.AgentId);
                sqlCommand.Parameters.AddWithValue("@pwd", profile.Password);
                sqlCommand.Parameters.AddWithValue("@AEmail", profile.AgentEmail);
                sqlCommand.Parameters.AddWithValue("@Landline", profile.LandLine);
                sqlCommand.Parameters.AddWithValue("@Fax", profile.Fax);
                sqlCommand.Parameters.AddWithValue("@Address", profile.Address);
                sqlCommand.Parameters.AddWithValue("@City", profile.City.Trim().ToLower() != "0" ? profile.City : "");
                sqlCommand.Parameters.AddWithValue("@State", profile.State.Trim().ToLower() != "-- select state --" ? profile.State : "");
                sqlCommand.Parameters.AddWithValue("@Country", profile.Country);
                sqlCommand.Parameters.AddWithValue("@StateCode", !string.IsNullOrEmpty(profile.StateCode) ? profile.StateCode : "");

                //GST
                sqlCommand.Parameters.AddWithValue("@GSTNO", !string.IsNullOrEmpty(profile.GSTNO) ? profile.GSTNO : "");
                sqlCommand.Parameters.AddWithValue("@GSTCompanyName", !string.IsNullOrEmpty(profile.GSTCompanyName) ? profile.GSTCompanyName : "");
                sqlCommand.Parameters.AddWithValue("@GSTCompanyAddress", !string.IsNullOrEmpty(profile.GSTCompanyAddress) ? profile.GSTCompanyAddress : "");
                sqlCommand.Parameters.AddWithValue("@GSTPhoneNo", !string.IsNullOrEmpty(profile.GSTPhoneNo) ? profile.GSTPhoneNo : "");
                sqlCommand.Parameters.AddWithValue("@GSTEmail", !string.IsNullOrEmpty(profile.GSTEmail) ? profile.GSTEmail : "");
                sqlCommand.Parameters.AddWithValue("@IsGSTApply", profile.IsGSTApply);
                sqlCommand.Parameters.AddWithValue("@GSTRemark", !string.IsNullOrEmpty(profile.GSTRemark) ? profile.GSTRemark : "");
                sqlCommand.Parameters.AddWithValue("@GST_Pincode", !string.IsNullOrEmpty(profile.GstPinCode) ? profile.GstPinCode : "");

                ConnectToDataBase.MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                ConnectToDataBase.MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static DataTable StateList(string country)
        {
            try
            {
                sqlCommand = new SqlCommand("select STATEID as Code,STATE as Name from Tbl_STATE where COUNTRY='" + country + "' order by STATE", ConnectToDataBase.MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "Tbl_STATE");
                dataTable = dataSet.Tables["Tbl_STATE"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }

        public static DataTable CityList(string stateId)
        {
            try
            {
                sqlCommand = new SqlCommand("select CITY, STATEID from TBL_CITY where STATEID='" + stateId + "' order by CITY", ConnectToDataBase.MyAmdDBConnection);

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "TBL_CITY");
                dataTable = dataSet.Tables["TBL_CITY"];
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dataTable;
        }
    }
}
