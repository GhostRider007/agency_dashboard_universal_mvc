﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace UtilityLibrary
{
    public static class Mail
    {
        public static bool SendMail(string toEMail, string from, string bcc, string cc, string smtpClient, string userID, string pass, string body, string subject, string AttachmentFile = null)
        {
            try
            {
                SmtpClient objMail = new SmtpClient();
                MailMessage msgMail = new MailMessage();
                msgMail.To.Clear();
                msgMail.To.Add(new MailAddress(toEMail));
                msgMail.From = new MailAddress(from);

                if (!string.IsNullOrEmpty(bcc))
                {
                    msgMail.Bcc.Add(new MailAddress(bcc));
                }

                if (!string.IsNullOrEmpty(cc))
                {
                    msgMail.CC.Add(new MailAddress(cc));
                }

                if (!string.IsNullOrEmpty(AttachmentFile))
                {
                    msgMail.Attachments.Add(new Attachment(AttachmentFile));
                }

                msgMail.Subject = subject;
                msgMail.IsBodyHtml = true;
                msgMail.Body = body;

                objMail.Credentials = new System.Net.NetworkCredential(userID, pass);
                objMail.Host = smtpClient;
                objMail.Send(msgMail);
                return true;
            }
            catch (Exception ex)
            {
                //clsErrorLog.LogInfo(ex);

            }

            return false;
        }
    }
}
