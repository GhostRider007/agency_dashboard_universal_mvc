﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace richatravels
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(name: "Backend_Dashboard", url: "dashboard", defaults: new { controller = "Dashboard", action = "Dashboard" });
            routes.MapRoute(name: "TicketReport", url: "flight/ticket-copy", defaults: new { controller = "FlightReport", action = "TiceketCopy" });
            routes.MapRoute(name: "InvoiceDetails", url: "IntInvoiceDetails", defaults: new { controller = "Account", action = "IntInvoiceDetails" });

            routes.MapRoute(name: "AgentMarkupDom", url: "setting/agent-markup-dom", defaults: new { controller = "Setting", action = "Agent_markupDom" });
            routes.MapRoute(name: "AgentMarkupDomdelte", url: "setting/delete-markup-dom", defaults: new { controller = "Setting", action = "DeletedomMarkup" });
            routes.MapRoute(name: "AgentMarkupDomEdit", url: "setting/edit-markup-dom", defaults: new { controller = "Setting", action = "EditdomMarkup" });

            routes.MapRoute(name: "AgentMarkupInt", url: "setting/agent-markup-int", defaults: new { controller = "Setting", action = "IntAgent_markupDom" });
            routes.MapRoute(name: "AgentMarkupIntdelte", url: "setting/delete-markup-int", defaults: new { controller = "Setting", action = "IntDeletedomMarkup" });
            routes.MapRoute(name: "AgentMarkupIntEdit", url: "setting/edit-markup-int", defaults: new { controller = "Setting", action = "IntEditdomMarkup" });

            routes.MapRoute(name: "ADDSTAFF", url: "setting/add-satff", defaults: new { controller = "Setting", action = "NewExecutive" });
            routes.MapRoute(name: "UPDATESTAFF", url: "setting/edit-satff", defaults: new { controller = "Setting", action = "EditExecutive" });
            routes.MapRoute(name: "CreditDebit", url: "setting/credit-debit", defaults: new { controller = "Setting", action = "StaffCreditDebit" });

            routes.MapRoute(name: "AddbankList", url: "upload/add-bank", defaults: new { controller = "Wallet", action = "AddEditbankList" });
            routes.MapRoute(name: "Editbank", url: "upload/edit-bank", defaults: new { controller = "Wallet", action = "EditBank" });
            routes.MapRoute(name: "Deletebank", url: "upload/delete-bank", defaults: new { controller = "Wallet", action = "deletebank" });

            routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}", defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional });
        }
    }
}

