﻿using richatravels.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ModelLibrary.Common;

namespace richatravels.Controllers
{
    public class AutoCompleteController : Controller
    {
        public JsonResult GetDepArrCity(string param)
        {
            var cityList = AutoCompleteService.CityAutoSearch(param).Take(10).ToList();
            return Json(cityList);
        }

        public JsonResult GetPreferedAirlines(string param)
        {
            var airlinesList = AutoCompleteService.GetPreferedAirlines(param).Take(10).ToList();
            return Json(airlinesList);
        }
    }
}