﻿using richatravels.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ModelLibrary.SettingModel;
using static richatravels.Models.Account;

namespace richatravels.Controllers
{
    public class SettingController : Controller
    {
        // GET: Setting
        public ActionResult Agent_markupDom(MarkupModel model)
        {
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    model.user_id = lu.AgentRegisterSession.UID;
                }
                if (!string.IsNullOrEmpty(model.airline) && !string.IsNullOrEmpty(model.Markup) && !string.IsNullOrEmpty(model.markup_type))
                {
                    if (SettingService.InsertDomMarkup(model))
                    {
                        TempData["Msg"] = "Record Added Successfully ...";
                    }
                    else
                    {
                        TempData["Msg"] = "MarkUp For This AirLine Is Already Exist, Please Update..";
                    }
                }
                model.DomMarkupList = SettingService.GetDomMarkupDetials(model);
                model.Totalcount = model.DomMarkupList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        public ActionResult DeletedomMarkup(string counter)
        {
            if (!string.IsNullOrEmpty(counter))
            {
                if (SettingService.DeleteDomMarkupBycounter(counter))
                {
                    TempData["Msg"] = "markup Delete successfully..";
                }
            }
            return Redirect("/setting/agent-markup-dom");
        }

        public ActionResult EditdomMarkup(string counter, string markup, string markuptype)
        {
            MarkupModel model = new MarkupModel();
            if (!string.IsNullOrEmpty(counter) && !string.IsNullOrEmpty(markup) && !string.IsNullOrEmpty(markuptype))
            {
                model.Markup = markup;
                model.markup_type = markuptype;
                model.Counter = Convert.ToInt32(counter);
            }
            return View(model); ;
        }
        [HttpPost]
        public ActionResult EditdomMarkup(MarkupModel model)
        {

            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                model.user_id = lu.AgentRegisterSession.UID;
                if (SettingService.UpdateDomMarkup(model))
                {
                    TempData["Message"] = "Markup Updated successfully..";
                    // return Redirect("/setting/agent-markup-dom");
                }
            }
            return View(model);
        }

        //International
        public ActionResult IntAgent_markupDom(MarkupModel model)
        {
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    model.user_id = lu.AgentRegisterSession.UID;
                }
                if (!string.IsNullOrEmpty(model.airline) && !string.IsNullOrEmpty(model.Markup) && !string.IsNullOrEmpty(model.markup_type))
                {
                    if (SettingService.InsertIntMarkup(model))
                    {
                        TempData["Msg"] = "Record Added Successfully ...";
                    }
                    else
                    {
                        TempData["Msg"] = "MarkUp For This AirLine Is Already Exist, Please Update..";
                    }
                }
                model.DomMarkupList = SettingService.GetIntMarkupDetials(model);
                model.Totalcount = model.DomMarkupList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        public ActionResult IntDeletedomMarkup(string counter)
        {
            if (!string.IsNullOrEmpty(counter))
            {
                if (SettingService.DeleteIntMarkupBycounter(counter))
                {
                    TempData["Msg"] = "markup Delete successfully..";
                }
            }
            return Redirect("/setting/agent-markup-int");
        }

        public ActionResult IntEditdomMarkup(string counter, string markup, string markuptype)
        {
            MarkupModel model = new MarkupModel();
            if (!string.IsNullOrEmpty(counter) && !string.IsNullOrEmpty(markup) && !string.IsNullOrEmpty(markuptype))
            {
                model.Markup = markup;
                model.markup_type = markuptype;
                model.Counter = Convert.ToInt32(counter);
            }
            return View(model); ;
        }
        [HttpPost]
        public ActionResult IntEditdomMarkup(MarkupModel model)
        {

            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                model.user_id = lu.AgentRegisterSession.UID;
                if (SettingService.UpdateIntMarkup(model))
                {
                    TempData["Message"] = "Markup Updated successfully..";
                    // return Redirect("/setting/agent-markup-dom");
                }
            }
            return View(model);
        }

        //Add Staff
        public ActionResult NewExecutive(StaffModel model)
        {
            LoginSessionDetails lu = new LoginSessionDetails();
            try
            {
                if (AccountService.GetLoginSession(ref lu))
                {
                    model.OwnerId = lu.AgentRegisterSession.UID;
                    model.AgencyId = lu.AgentRegisterSession.UID;
                    model.UserType = "STAFF";
                    model.RoleType = "EXEC";
                    model.ActionType = "insert";
                    model.CheckBalance = "false";
                    model.CreatedBy = lu.AgentRegisterSession.UID;
                    if (!string.IsNullOrEmpty(model.Name) && !string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Mobile) && !string.IsNullOrEmpty(model.Password))
                    {
                        if (SettingService.InsertAgentStaff(model))
                        {
                            TempData["Msg"] = "Staff Added Successfully ...";
                        }
                    }
                    model.StaffList = SettingService.GetStaffList(model);
                    model.Totalcount = model.StaffList.Count;
                    model.Name = "";
                    model.Email = "";
                    model.Mobile = "";
                    model.Password = "";
                    model.Address = "";
                    model.Status = false;
                    model.Flight = false;
                    model.Bus = false;
                    model.Rail = false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult EditExecutive (string UserId, string Id, string agencyid, string Name,string mobileno, string Email,string address,string password,string status,string flight,string hotel,string bus,string rail)
        {
            StaffModel model = new StaffModel();
            model.UserId = UserId;
            model.Id = Convert.ToInt32(Id);
            model.AgencyId = agencyid;
            model.Name = Name;
            model.Mobile = mobileno;
            model.Email = Email;
            model.Address = address;
            model.Password = password;
            model.Status = Convert.ToBoolean(status);
            model.Flight = Convert.ToBoolean(flight);
            model.Hotel = Convert.ToBoolean(hotel);
            model.Bus = Convert.ToBoolean(bus);
            model.Rail = Convert.ToBoolean(rail);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditExecutive(StaffModel model)
        {
            if (!string.IsNullOrEmpty(model.Name) && !string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Mobile) && !string.IsNullOrEmpty(model.Password))
            {
                model.ActionType = "GRIDUPDATE";
                if (SettingService.InsertAgentStaff(model))
                {
                    TempData["Msg"] = "Upadted sucessfully..";
                }
            }
            return View(model);
        }

        public ActionResult StaffCreditDebit(string UserId, string Id, string Name, string agencyid, string type, string balance, string mobileno, string Email)
        {
            StaffModel model = new StaffModel();
            if (!string.IsNullOrEmpty(UserId) && !string.IsNullOrEmpty(Id))
            {
                model.UserId = UserId;
                model.StaffId = Id;
                model.Name = Name;
                model.AgencyId = agencyid;
                model.RoleType = type;
                model.CreditLimit = balance;
                model.Mobile = mobileno;
                model.Email = Email;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult StaffCreditDebit(StaffModel model)
        {
            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                model.OwnerId = lu.AgentRegisterSession.UID;
                string ReferenceNo = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                string InvoiceNo = "SFT" + ReferenceNo.Substring(4, 16);
                model.OrderId = InvoiceNo;
                if (model.TransType == "CREDIT")
                {
                    model.ModuleType = "FUNDTRANSFER";
                }
                else
                {
                    model.ModuleType = "FUNDREVERSAL";
                }

                model.Remarks = model.Remarks + "_AgencyId_" + model.AgencyId + "_To_StaffUserId_" + model.UserId + "_Amount_" + model.Amount + "_RefNo_" + model.OrderId;
                if (SettingService.StaffTransaction(model))
                {
                    if (model.TransType == "CREDIT")
                    {
                        TempData["Msg"] = "Amount credited sucessfully..";
                    }
                    else
                    {
                        TempData["Msg"] = "Amount debited sucessfully..";
                    }
                }
            }

            return View(model);
        }
    }

}
