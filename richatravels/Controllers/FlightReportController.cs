﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ModelLibrary.FlightModel;
using static richatravels.Models.Account;
using richatravels.Service;
using UtilityLibrary;
using System.Text;
using static ModelLibrary.MailingCredentialModel;
using DataBaseLibrary.MailingCredentiaL;
using static ModelLibrary.AgencyModel;
using DataBaseLibrary.Agency;
using richatravels.Models;
using ModelLibrary;
using richatravels.Models.Common;

namespace richatravels.Controllers
{
    public class FlightReportController : Controller
    {
        public ActionResult TicketReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    //filter.FromDate = "01/08/2020 12:00:00 AM";
                    filter.Status = FlightStatus.Ticketed.ToString();
                }

                model.FlightTicketReportList = FlightReportService.GetFlightTicketReports(filter);
                model.Totalcount = model.FlightTicketReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult ReIssueReFund(ReIssueReFund model, string paxid, string paxname, string orderid, string gdspnr, string paxtype, string actiontype)
        {
            try
            {
                if (!string.IsNullOrEmpty(paxid) && !string.IsNullOrEmpty(actiontype))
                {
                    if (actiontype == "reissue")
                    {
                        string firstStatus = FlightReportService.CheckTktNo(Convert.ToInt32(paxid), orderid, gdspnr);

                        model = FlightReportService.GetTicketdIntl(Convert.ToInt32(paxid), paxtype);

                        string newpaxid = paxid;

                        if (!string.IsNullOrEmpty(model.ResuID))
                        {
                            ReIssueReFund Paxds = FlightReportService.OldPaxInfo(model.ResuID, model.Title, model.FName, model.MName, model.LName, model.PaxType);
                            //newpaxid = Paxds.PaxId.ToString();
                        }

                        string secondStatus = FlightReportService.CheckTktNo(Convert.ToInt32(newpaxid.Trim()), model.OrderId, model.PNR);

                        if (secondStatus == "0")
                        {
                        }
                        else if (secondStatus == "Reissue request can not be accepted for past departure date." && actiontype == "refund")
                        {
                        }
                        else if ((secondStatus == "0" || secondStatus == "Reissue request can not be accepted for past departure date." || secondStatus == "Given ticket number is already ReIssued") || firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && actiontype == "refund")
                        {

                        }
                        else if (firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && actiontype == "refund")
                        {
                        }
                        else
                        {
                            TempData["Message"] = secondStatus;
                        }
                    }
                    else
                    {
                    }

                    model.ActionType = actiontype.ToUpper();
                }

                model.PaxName = paxname;
            }
            catch (Exception ex)
            {
                ex.ToString();
                //clsErrorLog.LogInfo(ex)
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult ReIssueReFund(ReIssueReFund model, string actiontype)
        {
            try
            {
                if (model.ActionType.ToLower() == "refund")
                {
                }
                else
                {
                    if (model.ActionType.ToLower() == "reissue")
                    {
                        ReIssueReFund fltds = FlightReportService.GetTicketdIntl(Convert.ToInt32(model.PaxID), model.PaxType);
                        if (fltds != null && !string.IsNullOrEmpty(fltds.UserID))
                        {
                            string status = FlightReportService.CheckTktNo(Convert.ToInt32(model.PaxID), model.OrderId, model.PNR);

                            if (status == "0")
                            {
                                fltds.Remark = model.Remark;
                                fltds.PaxID = model.PaxID;
                                fltds.ProjectID = !string.IsNullOrEmpty(model.ProjectID) ? model.ProjectID : "Nothing";
                                fltds.BillNoAir = !string.IsNullOrEmpty(model.BillNoAir) ? model.BillNoAir : "Nothing";
                                fltds.BillNoCorp = "Nothing";
                                fltds.ReqTyp = "R";
                                fltds.Status = FlightStatus.Pending.ToString();
                                fltds.RefundID = UtilityModel.GenrateRandomTransactionId("RISU", 10, "1234567890");

                                if (model.ActionType.ToLower() == "reissue")
                                {
                                    bool isInsert = FlightReportService.InsertReIssueCancelIntl(fltds);

                                    LoginSessionDetails lu = new LoginSessionDetails();
                                    if (AccountService.GetLoginSession(ref lu))
                                    {
                                        if (!string.IsNullOrEmpty(lu.AgentRegisterSession.LoginByOTP) && lu.AgentRegisterSession.LoginByOTP.ToLower().Trim() == "true")
                                        {
                                            string UserId = lu.AgentRegisterSession.UID;
                                            string Remark = model.Remark;
                                            string OTPRefNo = fltds.OrderId;
                                            string LoginByOTP = lu.AgentRegisterSession.LoginByOTP;
                                            string OTPId = "Nothing"; //Not Clear
                                            string ServiceType = "FLIGHT-TICKET" + model.ActionType.ToLower().Trim();
                                            bool flag = FlightReportService.OTPTransactionInsert(UserId, Remark, OTPRefNo, LoginByOTP, OTPId, ServiceType);
                                        }

                                        Dictionary<string, string> dicEmailCrets = FlightReportService.GetEmail_Credentilas(fltds.OrderId, "ReIssue_REJECTED", model.PaxID);

                                        MailingCredential mailCred = MailingCredentialLibraryHelper.GetMailingDetails(enumMAILING.AIR_PNRSUMMARY.ToString(), lu.AgentRegisterSession.UID);

                                        AgencyLoginSession agency = new AgencyLoginSession();
                                        if (AccountService.IsAgencyLogin(ref agency))
                                        {
                                            if (mailCred != null && mailCred.Counter > 0)
                                            {
                                                string strBody = EmailFormate.EmailFormateReissueRequest(fltds);

                                                bool isMailSent = Mail.SendMail(agency.Email, mailCred.MailFrom, mailCred.BCC, mailCred.CC, mailCred.SmtpClient, mailCred.UserID, mailCred.Pass, strBody, "Ticket Reissue Request");
                                            }
                                        }
                                    }

                                    if (isInsert)
                                    {
                                        TempData["Message"] = "Remark Submitted Succesfully";
                                    }
                                }
                            }
                            else
                            {
                                //TempData["Message"] = secondStatus;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult RefundReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    filter.Status = FlightStatus.Cancelled.ToString();
                }

                model.FlightTicketReportList = FlightReportService.GetFlightRefundReports(filter);
                model.Totalcount = model.FlightTicketReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult ReissuesReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                }

                model.FlightTicketReportList = FlightReportService.GetReIssueDetail(filter);
                model.Totalcount = model.FlightTicketReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult TicketStatusReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                }

                model.FlightTicketReportList = FlightReportService.GetTicketStatusReport(filter);
                model.Totalcount = model.FlightTicketReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult HoldPNRReport(FlightTicketFilter filter)
        {
            FlightTicketReport model = new FlightTicketReport();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                }

                model.FlightTicketReportList = FlightReportService.GetHoldPNRReport(filter);
                model.Totalcount = model.FlightTicketReportList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult OfflineRequest()
        {
            return View();
        }
        public ActionResult FlightPassengerDetail(string proxyid)
        {
            FlightPassengerDetail model = new FlightPassengerDetail();
            try
            {
                model = FlightReportService.GetFlightPassengerDetail(proxyid);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult OfflineRequestList(FlightTicketFilter filter)
        {
            OfflineRequest model = new OfflineRequest();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                }

                model.OfflineRequestList = FlightReportService.GetProxyBookingReport(filter);
                model.Totalcount = model.OfflineRequestList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult FlightTravelCalender()
        {
            return View();
        }

        public ActionResult TiceketCopy(string orderid, string transid,string charge,string amount)
        {
            FlightHeaderModel model = new FlightHeaderModel();
            bool IsBagFare = false;
            try
            {
                if(!string.IsNullOrEmpty(orderid)&&!string.IsNullOrEmpty(charge) && !string.IsNullOrEmpty(amount))
                {
                    LoginSessionDetails lu = new LoginSessionDetails();
                    if (AccountService.GetLoginSession(ref lu))
                        FlightReportService.UpdateTicketTeansCharges(orderid, charge, amount, lu.AgentRegisterSession.UID);     
                }
                model = FlightReportService.SelectAgent(orderid);
                model.PaxList = FlightReportService.SelectPaxDetail(orderid, transid);
                model.FltHeaderList = FlightReportService.SelectHeaderDetail(orderid);
                model.FltDetailsList = FlightReportService.SelectFlightDetail(orderid);
                model.SelectedFlightDetailsList = FlightReportService.GetSelectedFltDtls_Gal(orderid, model.AgentId);
                model.SelectedFlightDetailsList[0].ArrivalDate = comon.GetSpilitedDateFormate(model.SelectedFlightDetailsList[0].ArrivalDate);
                model.SelectedFlightDetailsList[0].DepartureDate = comon.GetSpilitedDateFormate(model.SelectedFlightDetailsList[0].DepartureDate);
                model.BagInfomartionList = FlightReportService.GetBaggageInformation(model.FltHeaderList.FirstOrDefault().Trip, model.FltHeaderList.FirstOrDefault().VC, IsBagFare);
                model.AgencyList = FlightReportService.SelectAgencyDetail(model.AgentId);
                model.FltFareDetailList = FlightReportService.SelectFareDetail(orderid, transid);
                model.MealAndBaggageList = FlightReportService.Get_MEAL_BAG_FareDetails(orderid, transid);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        #region [Json Secton]
        public JsonResult InsertOfflineRequest(OfflineRequest offline)
        {
            List<string> result = new List<string>();

            try
            {
                AgencyLoginSession agency = new AgencyLoginSession();
                if (AccountService.IsAgencyLogin(ref agency))
                {
                    offline.AgentId = agency.AgencyId;
                    offline.AgentUser_Id = agency.User_Id;
                    offline.Agentname = agency.Agency_Name;

                    //if (offline.TripType == "oneway")
                    //{
                    if (FlightReportService.InsertProxyDetails(offline))
                    {
                        result.Add("true");
                        result.Add("Proxy Request Sent Successfully.");
                    }
                    //}
                    //else
                    //{
                    //}
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }

        //public JsonResult ReIssueReFund(ReIssueReFund model)
        //{
        //    List<string> result = new List<string>();

        //    try
        //    {
        //        if (!string.IsNullOrEmpty(model.PaxID) && !string.IsNullOrEmpty(model.ActionType))
        //        {
        //            if (model.ActionType == "reissue")
        //            {
        //                string firstStatus = FlightReportService.CheckTktNo(Convert.ToInt32(model.PaxID), model.OrderId, model.PNR);

        //                model = FlightReportService.GetTicketdIntl(Convert.ToInt32(model.PaxID), model.PaxType);

        //                string newpaxid = model.PaxID;

        //                if (!string.IsNullOrEmpty(model.ResuID))
        //                {
        //                    ReIssueReFund Paxds = FlightReportService.OldPaxInfo(model.ResuID, model.Title, model.FName, model.MName, model.LName, model.PaxType);
        //                    //newpaxid = Paxds.PaxId.ToString();
        //                }

        //                string secondStatus = FlightReportService.CheckTktNo(Convert.ToInt32(newpaxid.Trim()), model.OrderId, model.PNR);

        //                if (secondStatus == "0")
        //                {
        //                }
        //                else if (secondStatus == "Reissue request can not be accepted for past departure date." && model.ActionType == "refund")
        //                {
        //                }
        //                else if ((secondStatus == "0" || secondStatus == "Reissue request can not be accepted for past departure date." || secondStatus == "Given ticket number is already ReIssued") || firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && model.ActionType == "refund")
        //                {

        //                }
        //                else if (firstStatus == "Refund/Reissue request can not be accepted upto 4 hour prior to departure date. Please contact to airline directly." && model.ActionType == "refund")
        //                {
        //                }
        //                else
        //                {
        //                    //TempData["Message"] = secondStatus;
        //                    result.Add("false");
        //                    result.Add(secondStatus);
        //                }
        //            }
        //            else
        //            {
        //            }

        //            //TempData["type"] = model.ActionType.ToUpper();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        //clsErrorLog.LogInfo(ex)
        //    }

        //    return Json(result);
        //}
        #endregion
    }
}