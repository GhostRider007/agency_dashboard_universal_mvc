﻿using richatravels.Models.Common;
using richatravels.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ModelLibrary.WalletModel;
using static richatravels.Models.Account;

namespace richatravels.Controllers
{
    public class WalletController : Controller
    {
        #region [Upload Request]
        public ActionResult UploadRequest(UploadRequest model)
        {            
            AgencyLoginSession lu = new AgencyLoginSession();
            try
            {
                if (AccountService.IsAgencyLogin(ref lu))
                {
                    string distr = lu.Distr;
                    if (distr.ToUpper().Trim() == "FWU")
                    {
                        model.IsShowReqType = true;
                    }

                    model.DepositeBankList = comon.PopulateDepositeBank(WalletService.DepositeBankList(lu.User_Id));
                    model.DepositeOfficeList = comon.PopulateDepositeOffice(WalletService.DepositeOfficeList(lu.User_Id));
                    model.DepositeBranchList = comon.PopulateAccountAndBranch(WalletService.DepositeAccountAndBranch(null, null));
                    model.DepositeAccountNoList = comon.PopulateAccountAndBranch(WalletService.DepositeAccountAndBranch(null, null));
                    if (!string.IsNullOrEmpty(model.Amount) && !string.IsNullOrEmpty(model.PaymentMode))
                    {
                        model.AgencyName = lu.Agency_Name;
                        model.AgencyID = lu.User_Id;
                        model.UploadeRequestType = "CA";
                        model.AgentType = lu.Agent_Type;
                        model.Status = "Pending";
                        if (WalletService.InsertDepositDetail(model))
                        {
                            TempData["Msg"] = "Upload Request Sent Sucessfully ...";
                        }
                    }
                    ModelState.Clear();
                    model.PaymentMode = "";
                    model.Amount = "";
                    model.Remark = "";
                    model.ReferenceNumber = "";
                    model.ReceiptNumber = "";
                }

            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public JsonResult BindDepositeBranch(string bank)
        {
            List<string> branchList = new List<string>();

            AgencyLoginSession lu = new AgencyLoginSession();
            if (AccountService.IsAgencyLogin(ref lu))
            {
                branchList = WalletService.DepositeAccountAndBranch("branch", bank, "Branch" + lu.User_Id);
            }

            return Json(branchList);
        }
        public JsonResult BindDepositeAccountNo(string bank)
        {
            List<string> accountList = new List<string>();

            AgencyLoginSession lu = new AgencyLoginSession();
            if (AccountService.IsAgencyLogin(ref lu))
            {
                accountList = WalletService.DepositeAccountAndBranch("office", bank, "Acc" + lu.User_Id);
            }

            return Json(accountList);
        }
        #endregion


        public ActionResult UploadRequestStatus(UploadModelFilter filter)
        {
            UploadModel model = new UploadModel();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    filter.SearchType = "Own";
                }

                model.UploadDetialsList = WalletService.GetUploadDetials(filter);
                model.Totalcount = model.UploadDetialsList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult Bankdetails()
        {
            return View();
        }

        public ActionResult CashInFlow(UploadModelFilter filter)
        {
            UploadModel model = new UploadModel();
            try
            {
                LoginSessionDetails lu = new LoginSessionDetails();
                if (AccountService.GetLoginSession(ref lu))
                {
                    filter.UserType = lu.AgentRegisterSession.User_Type;
                    filter.Loginid = lu.AgentRegisterSession.UID;
                    filter.SearchType = "Own";
                }

                model.UploadDetialsList = WalletService.GetCashInflowDetails(filter);
                model.Totalcount = model.UploadDetialsList.Count;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult AddEditbankList(BankModel model)
        {
            LoginSessionDetails lu = new LoginSessionDetails();
            try
            {
                if (AccountService.GetLoginSession(ref lu))
                {
                    model.Agentid = lu.AgentRegisterSession.UID;
                    if (!string.IsNullOrEmpty(model.BankName) && !string.IsNullOrEmpty(model.BranchName) && !string.IsNullOrEmpty(model.AccountNumber) && !string.IsNullOrEmpty(model.NEFTCode))
                    {
                        model.Operation = "INSERT";
                        if (WalletService.InsertBankdetials(model))
                        {
                            TempData["Msg"] = "bank Added Successfully ...";
                        }
                    }
                    model.Banklist = WalletService.GetDistributorBankDetails(model);
                    model.Totalcount = model.Banklist.Count;
                    ModelState.Clear();
                    model.BankName = "";
                    model.BranchName = "";
                    model.NEFTCode = "";
                    model.Area = "";
                    model.AccountNumber = "";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult EditBank(string counter, string bankname, string branchname, string area, string neftcode, string accno)
        {
            BankModel model = new BankModel();
            model.Counter = Convert.ToInt32(counter);
            model.BankName = bankname;
            model.BranchName = branchname;
            model.Area = area;
            model.NEFTCode = neftcode;
            model.AccountNumber = accno;
            return View(model);
        }
        [HttpPost]
        public ActionResult EditBank(BankModel model)
        {

            LoginSessionDetails lu = new LoginSessionDetails();
            if (AccountService.GetLoginSession(ref lu))
            {
                model.Operation = "UPDATE";
                model.Agentid = lu.AgentRegisterSession.UID;
                if (WalletService.InsertBankdetials(model))
                {
                    TempData["Message"] = "bank updated successfully..";
                }
            }
            return View(model);
        }
        public ActionResult deletebank(string counter)
        {
            BankModel model = new BankModel();
            try
            {
                model.Operation = "DELETE";
                model.Counter = Convert.ToInt32(counter);
                if (WalletService.InsertBankdetials(model))
                {
                    TempData["Msg"] = "bank deleted Successfully ...";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Redirect("/upload/add-bank");
        }
    }
}