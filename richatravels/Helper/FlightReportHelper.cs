﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DataBaseLibrary;
using static ModelLibrary.FlightModel;

namespace richatravels.Helper
{
    public static class FlightReportHelper
    {
        public static List<FlightTicketReport> GetFlightTicketReports(FlightTicketFilter filter, ref int totalCount)
        {
            List<FlightTicketReport> result = new List<FlightTicketReport>();
            try
            {
                DataTable dtFlightTicket = FlightDataBase.GetTicektReport(filter);                

                totalCount = dtFlightTicket.Rows.Count;                
                if (dtFlightTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightTicket.Rows.Count; i++)
                    {
                        FlightTicketReport model = new FlightTicketReport();
                        model.OrderId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["OrderId"].ToString()) ? dtFlightTicket.Rows[i]["OrderId"].ToString() : string.Empty;
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) : 0;
                        model.TotalBookingCost = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) : 0;
                        model.sector = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["sector"].ToString()) ? dtFlightTicket.Rows[i]["sector"].ToString() : string.Empty;
                        model.AgentId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentId"].ToString()) ? dtFlightTicket.Rows[i]["AgentId"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.AgentType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentType"].ToString()) ? dtFlightTicket.Rows[i]["AgentType"].ToString() : string.Empty;
                        model.PaxId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaxId"].ToString()) ? Convert.ToInt32(dtFlightTicket.Rows[i]["PaxId"].ToString()) : 0;
                        model.Status = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Status"].ToString()) ? dtFlightTicket.Rows[i]["Status"].ToString() : string.Empty;
                        model.CreateDate = dtFlightTicket.Rows[i]["CreateDate"].ToString();
                        model.VC = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["VC"].ToString()) ? dtFlightTicket.Rows[i]["VC"].ToString() : string.Empty;
                        model.GdsPnr = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["GdsPnr"].ToString()) ? dtFlightTicket.Rows[i]["GdsPnr"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TicketNumber"].ToString()) ? dtFlightTicket.Rows[i]["TicketNumber"].ToString() : string.Empty;
                        model.PaxType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaxType"].ToString()) ? dtFlightTicket.Rows[i]["PaxType"].ToString() : string.Empty;
                        model.PgFName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["FName"].ToString()) ? dtFlightTicket.Rows[i]["FName"].ToString() : string.Empty;
                        model.PgLName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["LName"].ToString()) ? dtFlightTicket.Rows[i]["LName"].ToString() : string.Empty;
                        model.ExecutiveId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["ExecutiveId"].ToString()) ? dtFlightTicket.Rows[i]["ExecutiveId"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Trip"].ToString()) ? dtFlightTicket.Rows[i]["Trip"].ToString() : string.Empty;
                        model.RejectedRemark = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["RejectedRemark"].ToString()) ? dtFlightTicket.Rows[i]["RejectedRemark"].ToString() : string.Empty;
                        model.Provider = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Provider"].ToString()) ? dtFlightTicket.Rows[i]["Provider"].ToString() : string.Empty;
                        model.FareType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["FTYPE"].ToString()) ? dtFlightTicket.Rows[i]["FTYPE"].ToString() : string.Empty;
                        model.PName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PName"].ToString()) ? dtFlightTicket.Rows[i]["PName"].ToString() : string.Empty;
                        model.JourneyDate = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["JourneyDate"].ToString()) ? dtFlightTicket.Rows[i]["JourneyDate"].ToString() : string.Empty;
                        model.PaymentMode = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaymentMode"].ToString()) ? dtFlightTicket.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
    }
}