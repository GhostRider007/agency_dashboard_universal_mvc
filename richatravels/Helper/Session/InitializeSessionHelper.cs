﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static richatravels.Models.Account;

namespace richatravels.Helper.Session
{
    public static class InitializeSessionHelper
    {
        public static void InitializeLoginSession(LoginSessionDetails session)
        {
            LoginSessionDetails olu = new LoginSessionDetails();

            //AgentStaffSession agentStaff = new AgentStaffSession();
            //agentStaff.StaffUserId = session.AgentStaffSession.StaffUserId;
            //agentStaff.FlightActive = session.AgentStaffSession.FlightActive;
            //agentStaff.HotelActive = session.AgentStaffSession.HotelActive;
            //agentStaff.BusActive = session.AgentStaffSession.BusActive;
            //agentStaff.RailActive = session.AgentStaffSession.RailActive;

            //AgentRegisterSession agentReg = new AgentRegisterSession();
            //agentReg.UID_USER = session.AgentRegisterSession.UID_USER;
            //agentReg.LastLoginTime = session.AgentRegisterSession.UID_USER;
            //agentReg.AgencyId = session.AgentRegisterSession.UID_USER;
            //agentReg.AgencyName = session.AgentRegisterSession.UID_USER;
            //agentReg.LoginByOTP = session.AgentRegisterSession.UID_USER;
            //agentReg.FirstNameITZ = session.AgentRegisterSession.UID_USER;
            //agentReg.UID = session.AgentRegisterSession.UID_USER;
            //agentReg.UserType = session.AgentRegisterSession.UID_USER;
            //agentReg.TypeID = session.AgentRegisterSession.UID_USER;
            //agentReg.IsCorp = session.AgentRegisterSession.UID_USER;
            //agentReg.AGTY = session.AgentRegisterSession.UID_USER;
            //agentReg.Agent_Type = session.AgentRegisterSession.UID_USER;
            //agentReg.User_Type = session.AgentRegisterSession.UID_USER;
            //agentReg.LoginByStaff = session.AgentRegisterSession.UID_USER;
            //agentReg.LoginType = session.AgentRegisterSession.UID_USER;
            //agentReg.ADMINLogin = session.AgentRegisterSession.UID_USER;
            //agentReg.TripExec = session.AgentRegisterSession.UID_USER;

            olu.AgentStaffSession = session.AgentStaffSession;
            olu.AgentRegisterSession = session.AgentRegisterSession;

            List<LoginSessionDetails> lu = new List<LoginSessionDetails>();
            lu.Add(olu);

            HttpContext.Current.Session["loginsession"] = lu;
        }
        public static void InitializeAgencyLoginSession(DataTable dtAgency)
        {
            try
            {
                AgencyLoginSession olu = new AgencyLoginSession();
                if (dtAgency.Rows.Count > 0)
                {
                    olu.Counter = !string.IsNullOrEmpty(dtAgency.Rows[0]["Counter"].ToString()) ? dtAgency.Rows[0]["Counter"].ToString() : string.Empty;
                    olu.Title = !string.IsNullOrEmpty(dtAgency.Rows[0]["Title"].ToString()) ? dtAgency.Rows[0]["Title"].ToString() : string.Empty;
                    olu.FName = !string.IsNullOrEmpty(dtAgency.Rows[0]["Fname"].ToString()) ? dtAgency.Rows[0]["Fname"].ToString() : string.Empty;
                    olu.LName = !string.IsNullOrEmpty(dtAgency.Rows[0]["Lname"].ToString()) ? dtAgency.Rows[0]["Lname"].ToString() : string.Empty;
                    olu.Name = !string.IsNullOrEmpty(dtAgency.Rows[0]["Name"].ToString()) ? dtAgency.Rows[0]["Name"].ToString() : string.Empty;
                    olu.Address = !string.IsNullOrEmpty(dtAgency.Rows[0]["Address"].ToString()) ? dtAgency.Rows[0]["Address"].ToString() : string.Empty;
                    olu.City = !string.IsNullOrEmpty(dtAgency.Rows[0]["City"].ToString()) ? dtAgency.Rows[0]["City"].ToString() : string.Empty;
                    olu.District = !string.IsNullOrEmpty(dtAgency.Rows[0]["District"].ToString()) ? dtAgency.Rows[0]["District"].ToString() : string.Empty;
                    olu.State = !string.IsNullOrEmpty(dtAgency.Rows[0]["State"].ToString()) ? dtAgency.Rows[0]["State"].ToString() : string.Empty;
                    olu.Country = !string.IsNullOrEmpty(dtAgency.Rows[0]["Country"].ToString()) ? dtAgency.Rows[0]["Country"].ToString() : string.Empty;
                    olu.ZipCode = !string.IsNullOrEmpty(dtAgency.Rows[0]["zipcode"].ToString()) ? dtAgency.Rows[0]["zipcode"].ToString() : string.Empty;
                    olu.Phone = !string.IsNullOrEmpty(dtAgency.Rows[0]["Phone"].ToString()) ? dtAgency.Rows[0]["Phone"].ToString() : string.Empty;
                    olu.Mobile = !string.IsNullOrEmpty(dtAgency.Rows[0]["Mobile"].ToString()) ? dtAgency.Rows[0]["Mobile"].ToString() : string.Empty;
                    olu.Email = !string.IsNullOrEmpty(dtAgency.Rows[0]["Email"].ToString()) ? dtAgency.Rows[0]["Email"].ToString() : string.Empty;
                    olu.Agency_Name = !string.IsNullOrEmpty(dtAgency.Rows[0]["Agency_Name"].ToString()) ? dtAgency.Rows[0]["Agency_Name"].ToString() : string.Empty;
                    olu.Website = !string.IsNullOrEmpty(dtAgency.Rows[0]["Website"].ToString()) ? dtAgency.Rows[0]["Website"].ToString() : string.Empty;
                    olu.NamePanCard = !string.IsNullOrEmpty(dtAgency.Rows[0]["NamePanCard"].ToString()) ? dtAgency.Rows[0]["NamePanCard"].ToString() : string.Empty;
                    olu.PanNo = !string.IsNullOrEmpty(dtAgency.Rows[0]["PanNo"].ToString()) ? dtAgency.Rows[0]["PanNo"].ToString() : string.Empty;
                    olu.StateCode = !string.IsNullOrEmpty(dtAgency.Rows[0]["StateCode"].ToString()) ? dtAgency.Rows[0]["StateCode"].ToString() : string.Empty;
                    olu.Status = !string.IsNullOrEmpty(dtAgency.Rows[0]["Status"].ToString()) ? dtAgency.Rows[0]["Status"].ToString() : string.Empty;
                    olu.Sec_Qes = !string.IsNullOrEmpty(dtAgency.Rows[0]["Sec_Qes"].ToString()) ? dtAgency.Rows[0]["Sec_Qes"].ToString() : string.Empty;
                    olu.Sec_Ans = !string.IsNullOrEmpty(dtAgency.Rows[0]["Sec_Ans"].ToString()) ? dtAgency.Rows[0]["Sec_Ans"].ToString() : string.Empty;
                    olu.User_Id = !string.IsNullOrEmpty(dtAgency.Rows[0]["User_Id"].ToString()) ? dtAgency.Rows[0]["User_Id"].ToString() : string.Empty;
                    olu.Password = !string.IsNullOrEmpty(dtAgency.Rows[0]["PWD"].ToString()) ? dtAgency.Rows[0]["PWD"].ToString() : string.Empty;
                    olu.Agent_Type = !string.IsNullOrEmpty(dtAgency.Rows[0]["Agent_Type"].ToString()) ? dtAgency.Rows[0]["Agent_Type"].ToString() : string.Empty;
                    olu.Crd_Limit = !string.IsNullOrEmpty(dtAgency.Rows[0]["Crd_Limit"].ToString()) ? String.Format("{0:0.00}", Convert.ToDecimal(dtAgency.Rows[0]["Crd_Limit"].ToString())) : string.Empty;
                    olu.Crd_Trns_Date = !string.IsNullOrEmpty(dtAgency.Rows[0]["Crd_Trns_Date"].ToString()) ? dtAgency.Rows[0]["Crd_Trns_Date"].ToString() : string.Empty;
                    olu.Distr = !string.IsNullOrEmpty(dtAgency.Rows[0]["Distr"].ToString()) ? dtAgency.Rows[0]["Distr"].ToString() : string.Empty;
                    olu.Agent_Status = !string.IsNullOrEmpty(dtAgency.Rows[0]["Agent_status"].ToString()) ? dtAgency.Rows[0]["Agent_status"].ToString() : string.Empty;
                    olu.TDS = !string.IsNullOrEmpty(dtAgency.Rows[0]["TDS"].ToString()) ? dtAgency.Rows[0]["TDS"].ToString() : string.Empty;
                    olu.Online_Tkt = !string.IsNullOrEmpty(dtAgency.Rows[0]["Online_Tkt"].ToString()) ? dtAgency.Rows[0]["Online_Tkt"].ToString() : string.Empty;
                    olu.SalesExecID = !string.IsNullOrEmpty(dtAgency.Rows[0]["SalesExecID"].ToString()) ? dtAgency.Rows[0]["SalesExecID"].ToString() : string.Empty;
                    olu.IsPWD = !string.IsNullOrEmpty(dtAgency.Rows[0]["IsPWD"].ToString()) ? dtAgency.Rows[0]["IsPWD"].ToString() : string.Empty;
                    olu.GSTNo = !string.IsNullOrEmpty(dtAgency.Rows[0]["GSTNO"].ToString()) ? dtAgency.Rows[0]["GSTNO"].ToString() : string.Empty;
                    olu.GST_Company_Name = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_Company_Name"].ToString()) ? dtAgency.Rows[0]["GST_Company_Name"].ToString() : string.Empty;
                    olu.GST_Company_Address = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_Company_Address"].ToString()) ? dtAgency.Rows[0]["GST_Company_Address"].ToString() : string.Empty;
                    olu.GST_PhoneNo = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_PhoneNo"].ToString()) ? dtAgency.Rows[0]["GST_PhoneNo"].ToString() : string.Empty;
                    olu.GST_Email = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_Email"].ToString()) ? dtAgency.Rows[0]["GST_Email"].ToString() : string.Empty;
                    olu.Is_GST_Apply = !string.IsNullOrEmpty(dtAgency.Rows[0]["Is_GST_Apply"].ToString()) ? dtAgency.Rows[0]["Is_GST_Apply"].ToString() : string.Empty;
                    olu.GST_Remark = !string.IsNullOrEmpty(dtAgency.Rows[0]["GSTRemark"].ToString()) ? dtAgency.Rows[0]["GSTRemark"].ToString() : string.Empty;
                    olu.GST_City = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_City"].ToString()) ? dtAgency.Rows[0]["GST_City"].ToString() : string.Empty;
                    olu.GST_Pincode = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_Pincode"].ToString()) ? dtAgency.Rows[0]["GST_Pincode"].ToString() : string.Empty;
                    olu.GST_Address = !string.IsNullOrEmpty(dtAgency.Rows[0]["GSTAddress"].ToString()) ? dtAgency.Rows[0]["GSTAddress"].ToString() : string.Empty;
                    olu.GST_StateCode = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_State_Code"].ToString()) ? dtAgency.Rows[0]["GST_State_Code"].ToString() : string.Empty;
                    olu.GST_State = !string.IsNullOrEmpty(dtAgency.Rows[0]["GST_State"].ToString()) ? dtAgency.Rows[0]["GST_State"].ToString() : string.Empty;
                    olu.AgentLimit = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgentLimit"].ToString()) ? dtAgency.Rows[0]["AgentLimit"].ToString() : string.Empty;
                    olu.DueAmount = !string.IsNullOrEmpty(dtAgency.Rows[0]["DueAmount"].ToString()) ? String.Format("{0:0.00}", Convert.ToDecimal(dtAgency.Rows[0]["DueAmount"].ToString())) : string.Empty;
                    olu.VirtualCreditLimit = !string.IsNullOrEmpty(dtAgency.Rows[0]["VirtualCreditLimit"].ToString()) ? dtAgency.Rows[0]["VirtualCreditLimit"].ToString() : string.Empty;
                    olu.AgencyId = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgencyId"].ToString()) ? dtAgency.Rows[0]["AgencyId"].ToString() : string.Empty;
                    olu.IsWhiteLabel = !string.IsNullOrEmpty(dtAgency.Rows[0]["IsWhiteLabel"].ToString()) ? dtAgency.Rows[0]["IsWhiteLabel"].ToString() : string.Empty;
                    olu.Branch = !string.IsNullOrEmpty(dtAgency.Rows[0]["Branch"].ToString()) ? dtAgency.Rows[0]["Branch"].ToString() : string.Empty;
                    olu.Currency = !string.IsNullOrEmpty(dtAgency.Rows[0]["currency"].ToString()) ? dtAgency.Rows[0]["currency"].ToString() : string.Empty;
                    olu.Balance = !string.IsNullOrEmpty(dtAgency.Rows[0]["Balance"].ToString()) ? String.Format("{0:0.00}", Convert.ToDecimal(dtAgency.Rows[0]["Balance"].ToString())) : string.Empty;
                }
                List<AgencyLoginSession> lu = new List<AgencyLoginSession>();
                lu.Add(olu);

                HttpContext.Current.Session["agencysession"] = lu;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}