﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static richatravels.Models.Account;
using richatravels.Models.Common;
using richatravels.Helper.Session;
using UtilityLibrary;
using DataBaseLibrary.Account;
using static ModelLibrary.AccountModel;
using static ModelLibrary.AgencyModel;
using DataBaseLibrary.Agency;
using DataBaseLibrary.MailingCredentiaL;
using static ModelLibrary.MailingCredentialModel;

namespace richatravels.Helper
{
    public static class AccountHelper
    {
        public static bool LoginProcess(AgentStaffLogin login, ref string msg, ref string redirectUrl)
        {
            bool isLogin = false;

            try
            {
                if (!string.IsNullOrEmpty(login.UserId) && !string.IsNullOrEmpty(login.Password))
                {
                    LoginSessionDetails session = new LoginSessionDetails();

                    string ipAddress = string.Empty;
                    string agentStaffLoginType = string.Empty;
                    string agentStaffUserId = string.Empty;
                    string loginByStaff = string.Empty;

                    string agentUserId = string.Empty;
                    string agentPassword = string.Empty;

                    string agencyName = string.Empty;

                    #region [For AgentStaffRegister]                
                    DataTable dtStaff = AccountDataBase.AgentStaffLogin(login, ref agentStaffLoginType);
                    if ((!string.IsNullOrEmpty(agentStaffLoginType) && agentStaffLoginType.Trim().ToUpper() == "STAFF"))
                    {
                        if ((dtStaff != null && dtStaff.Rows.Count > 0))
                        {
                            if (Convert.ToBoolean(dtStaff.Rows[0]["Status"]) == true && Convert.ToString(dtStaff.Rows[0]["Agent_status"]) == "ACTIVE")
                            {
                                agentStaffUserId = Convert.ToString(dtStaff.Rows[0]["UserId"]); // userid UserId
                                agentUserId = Convert.ToString(dtStaff.Rows[0]["AgentUserId"]);
                                agentPassword = Convert.ToString(dtStaff.Rows[0]["AgentPassword"]);

                                loginByStaff = "true";

                                AgentStaffSession aslSession = new AgentStaffSession();
                                aslSession.StaffUserId = agentStaffUserId;
                                aslSession.FlightActive = dtStaff.Rows[0]["Flight"].ToString();
                                aslSession.HotelActive = dtStaff.Rows[0]["Hotel"].ToString();
                                aslSession.BusActive = dtStaff.Rows[0]["Bus"].ToString();
                                aslSession.RailActive = dtStaff.Rows[0]["Rail"].ToString();

                                session.AgentStaffSession = aslSession;
                            }
                            else if (Convert.ToBoolean(dtStaff.Rows[0]["Status"]) == false)
                            {
                                msg = "UserId is not active.";
                                return false;
                            }
                            else if (dtStaff.Rows[0]["Agent_status"].ToString().Trim().ToLower() != "active")
                            {
                                msg = "Please contact your admin.";
                                return false;
                            }
                            else
                            {
                                msg = "Please contact your admin.";
                                return false;
                            }
                        }
                        else
                        {
                            msg = "Please contact your admin.";
                            return false;
                        }
                    }
                    #endregion

                    #region [For UserIDMapping]
                    DataSet dsUserIDMapping = AccountDataBase.User_Auth(login.UserId, login.Password);
                    if (dsUserIDMapping.Tables[0].Rows[0][0].ToString() == "Not a Valid ID")
                    {
                        msg = "Your userid seems to be incorrect";
                    }
                    else if (dsUserIDMapping.Tables[0].Rows[0][0].ToString() == "incorrect password")
                    {
                        msg = "Your password seems to be incorrect";
                    }
                    else
                    {
                        #region [For agent_register]
                        DataTable dtAgency = AccountDataBase.GetAgencyDetails(login.UserId);

                        if (dtAgency.Rows.Count > 0)
                        {
                            InitializeSessionHelper.InitializeAgencyLoginSession(dtAgency);

                            if ((Convert.ToBoolean(dtAgency.Rows[0]["IsWhiteLabel"].ToString()) == true))
                            {
                                if ((agentStaffLoginType != "STAFF"))
                                {
                                    if ((dtAgency.Rows[0]["Branch"].ToString().ToUpper() == "MUMBAI"))
                                    {
                                        redirectUrl = "https://www.Richa Travel.co/mumbai/AccessLogin.aspx?UserID=" + login.UserId + "&Code=" + login.Password;
                                    }
                                    if ((dtAgency.Rows[0]["Branch"].ToString().ToUpper() == "PUNJAB"))
                                    {
                                    }
                                }
                            }
                            else
                            {
                                AgentRegisterSession agentRegSession = new AgentRegisterSession();

                                int passexp = 0;

                                bool otpBaseLogin = Convert.ToBoolean(dtAgency.Rows[0]["OTPLoginStatus"].ToString());
                                bool pwdCondition = Convert.ToBoolean(dtAgency.Rows[0]["PasswordExpMsg"].ToString());
                                try
                                {
                                    passexp = Convert.ToInt32(dtAgency.Rows[0]["PasswordChangeDate"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    ex.ToString();
                                    passexp = 0;
                                }

                                if ((passexp <= 0 & pwdCondition == false))
                                {
                                    agentRegSession.UID_USER = dsUserIDMapping.Tables[0].Rows[0]["UID"].ToString();
                                    redirectUrl = "/PasswordRedirect.aspx";
                                }
                                else if ((otpBaseLogin == true))
                                {
                                    //EMAILDT = dt.Rows[0]["Email"].ToString();
                                    //MobileDT = dt.Rows[0]["Mobile"].ToString();
                                    //AgencyDT = dt.Rows[0]["AgencyId"].ToString();
                                    //AgencyNameDT = dt.Rows[0]["Agency_Name"].ToString();

                                    //flag1 = sendOTP(userid, AgencyDT, AgencyNameDT, MobileDT, EMAILDT);
                                }
                                else
                                {
                                    string id = dsUserIDMapping.Tables[0].Rows[0]["UID"].ToString();
                                    string usertype = dsUserIDMapping.Tables[0].Rows[0]["UserType"].ToString();
                                    string typeid = dsUserIDMapping.Tables[0].Rows[0]["TypeID"].ToString();
                                    string User = dsUserIDMapping.Tables[0].Rows[0]["Name"].ToString();

                                    try
                                    {
                                        DataSet lastLogin = new DataSet();
                                        lastLogin = AccountDataBase.LastLoginTime(id);
                                        agentRegSession.LastLoginTime = lastLogin.Tables[0].Rows[0]["LoginTime"].ToString();
                                        string strIPAddress = string.Empty; //UserControl.Request.UserHostAddress;

                                        #region [For Tbl_UserLoginTime]
                                        bool isinserted = AccountDataBase.InsertLoginTime(id, strIPAddress);
                                        #endregion
                                    }
                                    catch (Exception ex)
                                    {
                                        ex.ToString();
                                    }

                                    if (usertype == "TA")
                                    {
                                        agencyName = dsUserIDMapping.Tables[0].Rows[0]["AgencyName"].ToString();
                                        agentRegSession.AgencyId = dsUserIDMapping.Tables[0].Rows[0]["AgencyId"].ToString();
                                    }

                                    agentRegSession.LoginByOTP = "";
                                    agentRegSession.FirstNameITZ = login.UserId;
                                    agentRegSession.AgencyName = agencyName;
                                    agentRegSession.UID = id; // 'dset.Tables(0).Rows(0)("UID").ToString()
                                    agentRegSession.UserType = usertype; // ' "TA"
                                    agentRegSession.TypeID = typeid; // '"TA1"
                                    agentRegSession.IsCorp = false;
                                    agentRegSession.AGTY = dsUserIDMapping.Tables[0].Rows[0]["Agent_Type"].ToString(); // '"TYPE1"
                                    agentRegSession.Agent_Type = dsUserIDMapping.Tables[0].Rows[0]["Agent_Type"].ToString(); // '"TYPE1"
                                    agentRegSession.User_Type = User;
                                    agentRegSession.LoginByStaff = loginByStaff;
                                    agentRegSession.LoginType = agentStaffLoginType;
                                    //FormsAuthentication.RedirectFromLoginPage(userid, false);

                                    if (User == "ACC")
                                    {
                                        agentRegSession.UID = dsUserIDMapping.Tables[0].Rows[0]["UID"].ToString();
                                        agentRegSession.UserType = "AC";
                                        redirectUrl = "/SprReports/Accounts/Ledger.aspx";
                                        //UserControl.Session["UID"] = dset.Tables[0].Rows[0]["UID"].ToString();
                                        //UserControl.Session["UserType"] = "AC";
                                        //UserControl.Response.Redirect("SprReports/Accounts/Ledger.aspx", false);
                                    }
                                    else if (User == "ADMIN")
                                    {
                                        agentRegSession.ADMINLogin = login.UserId;
                                        agentRegSession.UID = dsUserIDMapping.Tables[0].Rows[0]["UID"].ToString();
                                        agentRegSession.UserType = "AD";
                                        redirectUrl = "/Search.aspx";
                                        //UserControl.Session["ADMINLogin"] = userid;
                                        //UserControl.Session["UID"] = dset.Tables[0].Rows[0]["UID"].ToString();
                                        //UserControl.Session["UserType"] = "AD";
                                        //UserControl.Response.Redirect("Search.aspx", false);
                                    }
                                    else if (User == "EXEC")
                                    {
                                        agentRegSession.User_Type = "EXEC";
                                        agentRegSession.TripExec = dsUserIDMapping.Tables[0].Rows[0]["Trip"].ToString();
                                        agentRegSession.UID = dsUserIDMapping.Tables[0].Rows[0]["UID"].ToString();
                                        agentRegSession.UserType = "EC";
                                        redirectUrl = "/SprReports/admin/profile.aspx";
                                        //UserControl.Session["User_Type"] = "EXEC";
                                        //UserControl.Session["TripExec"] = dset.Tables[0].Rows[0]["Trip"].ToString();
                                        //UserControl.Session["UID"] = dset.Tables[0].Rows[0]["UID"].ToString();
                                        //UserControl.Session["UserType"] = "EC";
                                        //UserControl.Response.Redirect("SprReports/admin/profile.aspx", false);
                                    }
                                    else if (User == "AGENT" & typeid == "TA1")
                                    {
                                        if ((dsUserIDMapping.Tables[0].Rows[0]["IsCorp"].ToString() != "" && dsUserIDMapping.Tables[0].Rows[0]["IsCorp"].ToString() != null))
                                        {
                                            agentRegSession.IsCorp = Convert.ToBoolean(dsUserIDMapping.Tables[0].Rows[0]["IsCorp"]);
                                            //UserControl.Session["IsCorp"] = Convert.ToBoolean(dset.Tables[0].Rows[0]["IsCorp"]);
                                        }
                                        redirectUrl = "/dashboard";
                                        //UserControl.Response.Redirect("Search.aspx", false);

                                        //return true;
                                    }
                                    else if (User == "AGENT" & typeid == "TA2")
                                    {
                                        if ((dsUserIDMapping.Tables[0].Rows[0]["IsCorp"].ToString() != "" && dsUserIDMapping.Tables[0].Rows[0]["IsCorp"].ToString() != null))
                                        {
                                            agentRegSession.IsCorp = Convert.ToBoolean(dsUserIDMapping.Tables[0].Rows[0]["IsCorp"].ToString());
                                        }
                                        redirectUrl = "/SprReports/Accounts/Ledger.aspx";

                                        //if ((dset.Tables[0].Rows[0]["IsCorp"].ToString() != "" && dset.Tables[0].Rows[0]["IsCorp"].ToString() != null))
                                        //    UserControl.Session["IsCorp"] = Convert.ToBoolean(dset.Tables[0].Rows[0]["IsCorp"].ToString());
                                        //UserControl.Response.Redirect("SprReports/Accounts/Ledger.aspx", false);
                                    }
                                    else if (usertype == "DI")
                                    {
                                        agentRegSession.AgencyId = dsUserIDMapping.Tables[0].Rows[0]["AgencyId"].ToString();
                                        agentRegSession.AgencyName = dsUserIDMapping.Tables[0].Rows[0]["AgencyName"].ToString();
                                        redirectUrl = "/SprReports/Accounts/Ledger.aspx";

                                        //UserControl.Session["AgencyId"] = dset.Tables[0].Rows[0]["AgencyId"].ToString();
                                        //UserControl.Session["AgencyName"] = dset.Tables[0].Rows[0]["AgencyName"].ToString();
                                        //UserControl.Response.Redirect("SprReports/Accounts/Ledger.aspx", false);
                                    }
                                }

                                session.AgentRegisterSession = agentRegSession;

                                InitializeSessionHelper.InitializeLoginSession(session);

                                return true;
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return isLogin;
        }
        public static string ForgetPassword(ForgetPassword forget)
        {
            string msg = string.Empty;
            try
            {
                DataTable dtForgetPass = AccountDataBase.ForgetPassword(forget);
                if (dtForgetPass.Rows.Count > 0)
                {
                    msg = !string.IsNullOrEmpty(dtForgetPass.Rows[0]["Messagetigive"].ToString()) ? dtForgetPass.Rows[0]["Messagetigive"].ToString() : string.Empty;

                    if (dtForgetPass.Rows[0]["Messagetigive"].ToString().ToLower().Trim().Contains("successfully"))
                    {
                        Agency_Del agency = AgencyLibraryHelper.GetAgencyDetails(forget.UserId);

                        if (agency != null && agency.Counter > 0)
                        {
                            string strBody = string.Empty;

                            MailingCredential mailCred = MailingCredentialLibraryHelper.GetMailingDetails(enumMAILING.RWT_REGISTRATION.ToString(), string.Empty);

                            if (mailCred != null && mailCred.Counter > 0)
                            {
                                string divbody = mailCred.Body;

                                string[] body = divbody.Split('}');
                                strBody = body[0];
                                strBody = strBody + "<tr><td><p><font face=\"Verdana, Arial, Helvetica, sans-serif\">Dear " + agency.Fname + " " + agency.Lname + "</p>";
                                strBody = strBody + "<p><font face=\"Verdana, Arial, Helvetica, sans-serif\">Your Password : " + agency.PWD + "</strong></td></tr>";
                                if (body.Count() > 1)
                                {
                                    strBody = strBody + body[2].ToString();
                                }

                                if (mailCred.Status)
                                {
                                    string subject = "Your password - " + agency.PWD;

                                    if (Mail.SendMail(agency.Email, mailCred.MailFrom, mailCred.BCC, mailCred.CC, mailCred.SmtpClient, mailCred.UserID, mailCred.Pass, strBody, subject))
                                    {
                                        msg = dtForgetPass.Rows[0]["Messagetigive"].ToString();
                                    }
                                    else
                                    {
                                        msg = "Failed! try again.";
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        msg = dtForgetPass.Rows[0]["Messagetigive"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }

            return msg;
        }
        public static bool UpdateAgentProfileDetails(AgentUpdateProfile profile)
        {
            try
            {
                return AccountDataBase.UpdateAgentProfileDetails(profile);
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        #region [Session Section]
        public static bool IsAgencyLogin(ref AgencyLoginSession lu)
        {
            try
            {
                if (HttpContext.Current.Session["agencysession"] != null)
                {
                    List<AgencyLoginSession> agency = (List<AgencyLoginSession>)HttpContext.Current.Session["agencysession"];
                    lu = agency[0];
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool GetLoginSession(ref LoginSessionDetails lu)
        {
            try
            {
                if (HttpContext.Current.Session["loginsession"] != null)
                {
                    List<LoginSessionDetails> session = (List<LoginSessionDetails>)HttpContext.Current.Session["loginsession"];
                    lu = session[0];
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool LogoutBackendLogin()
        {
            HttpContext.Current.Session.Remove("agencysession");
            HttpContext.Current.Session.Remove("loginsession");

            return true;
        }
        #endregion

        #region [Update Profile]
        public static void ReBindAgencyDetails(string userid)
        {
            DataTable dtAgency = AccountDataBase.GetAgencyDetails(userid);
            InitializeSessionHelper.InitializeAgencyLoginSession(dtAgency);
        }
        #endregion
    }
}