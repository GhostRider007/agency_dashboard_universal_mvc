﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ModelLibrary.FlightModel;
using DataBaseLibrary.Flight;
using richatravels.Models.Common;

namespace richatravels.Service
{
    public static class FlightReportService
    {
        public static List<FlightTicketReport> GetFlightTicketReports(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }

            return FlightLibraryHelper.GetFlightTicketReports(filter);
        }
        public static List<FlightTicketReport> GetFlightRefundReports(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }

            return FlightLibraryHelper.GetFlightRefundReports(filter);
        }
        public static List<OfflineRequest> GetProxyBookingReport(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }
            return FlightLibraryHelper.GetProxyBookingReport(filter);
        }
        public static List<FlightTicketReport> GetHoldPNRReport(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }
            filter.Actionby = "REPORT";
            return FlightLibraryHelper.GetHoldPNRReport(filter);
        }
        public static List<FlightTicketReport> GetTicketStatusReport(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }

            return FlightLibraryHelper.GetTicketStatusReport(filter);
        }
        public static List<FlightTicketReport> GetReIssueDetail(FlightTicketFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }

            return FlightLibraryHelper.GetReIssueDetail(filter);
        }
        public static string ConvertStringDateToStringDateFormate(string date)
        {
            DateTime dtDate = new DateTime();

            if (!string.IsNullOrEmpty(date))
            {
                dtDate = DateTime.Parse(date);
                return dtDate.ToString("MM/dd/yyyy hh:mm tt");
            }

            return string.Empty;
        }

        public static string CheckTktNo(int paxId, string orderId, string gdspnr)
        {
            return FlightLibraryHelper.CheckTktNo(paxId, orderId, gdspnr);
        }
        public static ReIssueReFund GetTicketdIntl(int paxId, string paxType)
        {
            return FlightLibraryHelper.GetTicketdIntl(paxId, paxType);
        }
        public static ReIssueReFund OldPaxInfo(string reissueId, string title, string fName, string mName, string lName, string paxType)
        {
            return FlightLibraryHelper.OldPaxInfo(reissueId, title, fName, mName, lName, paxType);
        }

        public static bool InsertReIssueCancelIntl(ReIssueReFund model)
        {
            return FlightLibraryHelper.InsertReIssueCancelIntl(model);
        }
        public static bool OTPTransactionInsert(string userId, string remark, string otpRefNo, string loginByOTP, string otpId, string serviceType)
        {
            return FlightLibraryHelper.OTPTransactionInsert(userId, remark, otpRefNo, loginByOTP, otpId, serviceType);
        }
        public static bool UpdateTicketTeansCharges(string orderid,string charge, string amount, string AgentId)
        {
            return FlightLibraryHelper.UpdateTicketTeansCharges(orderid,charge, amount, AgentId);
        }
        public static Dictionary<string, string> GetEmail_Credentilas(string orderId, string cmd_Type, string counter)
        {
            return FlightLibraryHelper.GetEmail_Credentilas(orderId, cmd_Type, counter);
        }
        public static bool InsertProxyDetails(OfflineRequest param)
        {
            bool isSuccess = false;
            try
            {
                if (param.TripType == "oneway")
                {
                    param.TripType = "One Way";
                    param.Trip = "O";
                }
                else
                {
                    param.TripType = "Round Trip";
                    param.Trip = "R";
                }

                param.FromCityCode = comon.GetSpilitByCost(param.FromCity);//"DEL"
                param.ToCityCode = comon.GetSpilitByCost(param.DestinationCity);//PAT

                string[] deptDate = param.DepartureDate.Split('/');
                if (deptDate.Length > 0)
                {
                    param.DepartDay = deptDate[0];
                    param.DepartMonth = deptDate[1];
                    param.DepartYear = deptDate[2];
                }

                if (param.FromCountry.Trim().ToLower() == "in" && param.ToCountry.Trim().ToLower() == "in")
                {
                    param.PType = "D";
                }
                else
                {
                    param.PType = "I";
                }

                param.ProjectId = "Nothing";
                param.BookedBy = "Nothing";
                param.Status = "Pending";

                if (FlightLibraryHelper.InsertProxyDetails(param))
                {
                    isSuccess = InsertProxyPaxDetail(param);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return isSuccess;
        }

        public static bool InsertProxyPaxDetail(OfflineRequest param)
        {
            bool issuccess = false;
            try
            {
                if (Convert.ToInt32(param.Adult) > 0)
                {
                    foreach (var guest in param.AdultDetails)
                    {
                        OfflineBookFlightDetail model = new OfflineBookFlightDetail();
                        model.AgentID = param.AgentId;
                        model.ProxyId = !string.IsNullOrEmpty(guest.ProxyId) ? guest.ProxyId : "0";
                        model.Title = guest.Title;
                        model.FirstName = guest.FirstName;
                        model.LastName = guest.LastName;
                        model.Age = guest.Age;
                        model.PaxType = guest.PaxType;
                        model.FreqFlyerNO = !string.IsNullOrEmpty(guest.FreqFlyerNO) ? guest.FreqFlyerNO : "";
                        model.PassportNo = !string.IsNullOrEmpty(guest.PassportNo) ? guest.PassportNo : "";
                        model.PPExp = !string.IsNullOrEmpty(guest.PPExp) ? guest.PPExp : "";
                        model.VisaDet = !string.IsNullOrEmpty(guest.VisaDet) ? guest.VisaDet : "";

                        if (FlightLibraryHelper.InsertProxyPaxDetail(model))
                        {
                            issuccess = true;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                if (Convert.ToInt32(param.Child) > 0)
                {
                    foreach (var guest in param.ChildDetails)
                    {
                        OfflineBookFlightDetail model = new OfflineBookFlightDetail();
                        model.AgentID = param.AgentId;
                        model.ProxyId = !string.IsNullOrEmpty(guest.ProxyId) ? guest.ProxyId : "0";
                        model.Title = guest.Title;
                        model.FirstName = guest.FirstName;
                        model.LastName = guest.LastName;
                        model.Age = guest.Age;
                        model.PaxType = guest.PaxType;
                        model.FreqFlyerNO = !string.IsNullOrEmpty(guest.FreqFlyerNO) ? guest.FreqFlyerNO : "";
                        model.PassportNo = !string.IsNullOrEmpty(guest.PassportNo) ? guest.PassportNo : "";
                        model.PPExp = !string.IsNullOrEmpty(guest.PPExp) ? guest.PPExp : "";
                        model.VisaDet = !string.IsNullOrEmpty(guest.VisaDet) ? guest.VisaDet : "";

                        if (FlightLibraryHelper.InsertProxyPaxDetail(model))
                        {
                            issuccess = true;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                if (Convert.ToInt32(param.Infant) > 0)
                {
                    foreach (var guest in param.InFantDetails)
                    {
                        OfflineBookFlightDetail model = new OfflineBookFlightDetail();
                        model.AgentID = param.AgentId;
                        model.ProxyId = !string.IsNullOrEmpty(guest.ProxyId) ? guest.ProxyId : "0";
                        model.Title = guest.Title;
                        model.FirstName = guest.FirstName;
                        model.LastName = guest.LastName;
                        model.Age = guest.Age;
                        model.PaxType = guest.PaxType;
                        model.FreqFlyerNO = !string.IsNullOrEmpty(guest.FreqFlyerNO) ? guest.FreqFlyerNO : "";
                        model.PassportNo = !string.IsNullOrEmpty(guest.PassportNo) ? guest.PassportNo : "";
                        model.PPExp = !string.IsNullOrEmpty(guest.PPExp) ? guest.PPExp : "";
                        model.VisaDet = !string.IsNullOrEmpty(guest.VisaDet) ? guest.VisaDet : "";

                        if (FlightLibraryHelper.InsertProxyPaxDetail(model))
                        {
                            issuccess = true;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return issuccess;
        }

        public static FlightPassengerDetail GetFlightPassengerDetail(string proxyid)
        {
            return FlightLibraryHelper.GetFlightPassengerDetail(proxyid);
        }
        public static List<PaxDetailModel> SelectPaxDetail(string orderid, string transid)
        {
            return FlightLibraryHelper.SelectPaxDetail(orderid, transid);
        }
        public static List<FlightHeaderModel> SelectHeaderDetail(string orderid)
        {
            return FlightLibraryHelper.SelectHeaderDetail(orderid);
        }
        public static List<FltFareDetailsModel> SelectFareDetail(string orderid,string transid)
        {
            return FlightLibraryHelper.SelectFareDetail(orderid, transid);
        }
        public static List<FltDetailsModel> SelectFlightDetail(string orderid)
        {
            return FlightLibraryHelper.SelectFlightDetail(orderid);
        }
        public static List<AgencyModel> SelectAgencyDetail(string AgentId)
        {
            return FlightLibraryHelper.SelectAgencyDetail(AgentId);
        }
        public static FlightHeaderModel SelectAgent(string orderid)
        {
            return FlightLibraryHelper.SelectAgent(orderid);
        }
        public static List<SelectedFlightDetailsGalModel> GetSelectedFltDtls_Gal(string orderid,string agentid)
        {
            return FlightLibraryHelper.GetSelectedFltDtls_Gal(orderid,agentid);
        }
        public static List<BagInfomartionModel> GetBaggageInformation(string Trip, string VC, bool IsBagFare)
        {
            return FlightLibraryHelper.GetBaggageInformation(Trip, VC, IsBagFare);
        }
        public static List<T_Flt_Meal_And_Baggage_RequestModel> Get_MEAL_BAG_FareDetails(string orderid, string transid)
        {
            return FlightLibraryHelper.Get_MEAL_BAG_FareDetails(orderid, transid);
        }
    }
}