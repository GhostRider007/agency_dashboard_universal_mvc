﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataBaseLibrary.Account;
using richatravels.Helper;
using static ModelLibrary.AccountModel;
using static richatravels.Models.Account;

namespace richatravels.Service
{
    public static class AccountService
    {
        public static List<AccountLedgerReport> GetLedgerDetail(AccountLedgerFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }
            filter.SearchType = "Own";
            return AccountLibraryHelper.GetLedgerDetail(filter);
        }
        public static AccountLedgerReport GetClosingbal(AccountLedgerFilter filter)
        {
            return AccountLibraryHelper.GetClosingbal(filter);
        }
        public static AccountLedgerReport GetInvoice(string orderid)
        {
            return AccountLibraryHelper.GetInvoice(orderid);
        }
        public static AccountLedgerReport GetAgencyDetails(string userId)
        {
            return AccountLibraryHelper.GetAgencyDetails(userId);
        }
        public static AccountLedgerReport GetCompanyAddress(string AddressType)
        {
            return AccountLibraryHelper.GetCompanyAddress(AddressType);
        }
        public static List<AccountLedgerReport> GetStaffTransaction(AccountLedgerFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }

            return AccountLibraryHelper.GetStaffTransaction(filter);
        }
        public static List<AccountLedgerReport> IntGetInvoice(AccountLedgerFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }

            return AccountLibraryHelper.IntGetInvoice(filter);
        }
        public static string ConvertStringDateToStringDateFormate(string date)
        {
            DateTime dtDate = new DateTime();

            if (!string.IsNullOrEmpty(date))
            {
                dtDate = DateTime.Parse(date);
                return dtDate.ToString("MM/dd/yyyy hh:mm tt");
            }

            return string.Empty;
        }
        public static bool LoginProcess(AgentStaffLogin login, ref string msg, ref string redirectUrl)
        {
            return AccountHelper.LoginProcess(login, ref msg, ref redirectUrl);
        }
        public static string ForgetPassword(ForgetPassword forget)
        {
            return AccountHelper.ForgetPassword(forget);
        }
        public static bool UpdateAgentProfileDetails(AgentUpdateProfile profile)
        {
            return AccountHelper.UpdateAgentProfileDetails(profile);
        }
        public static List<State> StateList(string country)
        {
            return AccountLibraryHelper.StateList(country);
        }

        public static List<City> CityList(string stateId)
        {
            return AccountLibraryHelper.CityList(stateId);
        }

        #region [Session Section]
        public static bool IsAgencyLogin(ref AgencyLoginSession lu)
        {
            return AccountHelper.IsAgencyLogin(ref lu);
        }
        public static bool GetLoginSession(ref LoginSessionDetails lu)
        {
            return AccountHelper.GetLoginSession(ref lu);
        }
        public static bool LogoutBackendLogin()
        {
            return AccountHelper.LogoutBackendLogin();
        }
        #endregion

        #region [Update Profile]
        public static void ReBindAgencyDetails(string userid)
        {
            AccountHelper.ReBindAgencyDetails(userid);
        }
        #endregion
    }
}