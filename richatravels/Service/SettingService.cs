﻿using DataBaseLibrary.Setting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ModelLibrary.SettingModel;

namespace richatravels.Service
{
    public static class SettingService
    {
        public static List<MarkupModel> GetDomMarkupDetials(MarkupModel model)
        {
            model.CmdType = "select";
            return SettingLibraryHelper.GetDomMarkupDetials(model);
        }
        public static bool InsertDomMarkup(MarkupModel model)
        {
            return SettingLibraryHelper.InsertDomMarkup(model);
        }
        public static bool UpdateDomMarkup(MarkupModel model)
        {
            return SettingLibraryHelper.UpdateDomMarkup(model);
        }
        public static bool DeleteDomMarkupBycounter(string counter)
        {
            return SettingLibraryHelper.DeleteDomMarkupBycounter(counter);
        }
        //International
        public static List<MarkupModel> GetIntMarkupDetials(MarkupModel model)
        {
            model.CmdType = "select";
            return SettingLibraryHelper.GetIntMarkupDetials(model);
        }
        public static bool InsertIntMarkup(MarkupModel model)
        {
            return SettingLibraryHelper.InsertIntMarkup(model);
        }
        public static bool UpdateIntMarkup(MarkupModel model)
        {
            return SettingLibraryHelper.UpdateIntMarkup(model);
        }
        public static bool DeleteIntMarkupBycounter(string counter)
        {
            return SettingLibraryHelper.DeleteIntMarkupBycounter(counter);
        }

        public static bool InsertAgentStaff(StaffModel model)
        {
            return SettingLibraryHelper.InsertAgentStaff(model);
        }

        public static List<StaffModel> GetStaffList(StaffModel model)
        {            
            return SettingLibraryHelper.GetStaffList(model);
        }
        public static bool StaffTransaction(StaffModel model)
        {
            return SettingDatabase.StaffTransaction(model);
        }
    }

}