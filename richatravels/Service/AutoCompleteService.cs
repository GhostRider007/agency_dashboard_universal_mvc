﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ModelLibrary.Common;
using DataBaseLibrary.Flight;

namespace richatravels.Service
{
    public static class AutoCompleteService
    {
        public static List<CityAutoSearch> CityAutoSearch(string param)
        {
            return FlightLibraryHelper.CityAutoSearch(param);
        }

        public static List<AirLineModel> GetPreferedAirlines(string param)
        {
            return FlightLibraryHelper.GetAirlinesList(param);
        }
    }
}