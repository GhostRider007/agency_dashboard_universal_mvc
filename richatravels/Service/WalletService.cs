﻿using DataBaseLibrary.Wallet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ModelLibrary.WalletModel;

namespace richatravels.Service
{
    public static class WalletService
    {
        public static List<UploadModel> GetUploadDetials(UploadModelFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }

            return WalletLibraryHelper.GetUploadDetials(filter);
        }
        public static List<UploadModel> GetCashInflowDetails(UploadModelFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.FromDate))
            {
                filter.FromDate = ConvertStringDateToStringDateFormate(filter.FromDate);
            }
            if (!string.IsNullOrEmpty(filter.ToDate))
            {
                filter.ToDate = ConvertStringDateToStringDateFormate(filter.ToDate);
            }

            return WalletLibraryHelper.GetCashInflowDetails(filter);
        }
        public static List<BankModel> GetDistributorBankDetails(BankModel model)
        {

            return WalletLibraryHelper.GetDistributorBankDetails(model);
        }
        public static bool InsertBankdetials(BankModel model)
        {
            return WalletLibraryHelper.InsertBankdetials(model);
        }
        public static bool InsertDepositDetail(UploadRequest model)
        {
            return WalletLibraryHelper.InsertDepositDetail(model);
        }
        public static string ConvertStringDateToStringDateFormate(string date)
        {
            DateTime dtDate = new DateTime();

            if (!string.IsNullOrEmpty(date))
            {
                dtDate = DateTime.Parse(date);
                return dtDate.ToString("MM/dd/yyyy hh:mm tt");
            }

            return string.Empty;
        }

        #region [Upload Request]
        public static List<UploadBankInformation> DepositeBankList(string agencyUserId)
        {
            return WalletLibraryHelper.DepositeBankList(agencyUserId);
        }
        public static List<string> DepositeOfficeList(string agencyUserId)
        {
            return WalletLibraryHelper.DepositeOfficeList(agencyUserId);
        }
        public static List<string> DepositeAccountAndBranch(string actionType, string bank = null, string type = null)
        {
            return WalletLibraryHelper.DepositeAccountAndBranch(actionType, bank, type);
        }
        #endregion
    }
}