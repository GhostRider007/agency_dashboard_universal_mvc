﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace richatravels.Models.Common
{
    public static class ProjectConfig
    {
        public static string WebsiteName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitename"]); }
        }

        public static string WebsiteUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websiteurl"]); }
        }
        public static string WebsiteLogo
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitelogo"]); }
        }

        public static string AdminEmail
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminemail"]); }
        }
    }
}