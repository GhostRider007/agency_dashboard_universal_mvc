﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static ModelLibrary.AccountModel;
using static ModelLibrary.WalletModel;

namespace richatravels.Models.Common
{
    public static class comon
    {
        public static string GetSpilitByCost(string param)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(param))
            {
                string[] city = param.Split('(');
                if (city.Length == 2)
                {
                    result = city[1].Replace(")", "");
                }
            }

            return result;
        }
        public static string GetSpilitedDateFormate(string date)//200220
        {
            string result = string.Empty;
            if (date.Length==6)
            {
                if (!string.IsNullOrEmpty(date))
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < date.Length; i++)
                    {
                        if (i % 2 == 0)
                        {
                            sb.Append('-');
                        }
                        sb.Append(date[i]);
                    }

                    result = ConvertStringDateToStringDateFormate(sb.ToString().TrimStart('-'));
                }
               
            }
            return result;


        }
        public static string ConvertStringDateToStringDateFormate(string date)
        {
            DateTime dtDate = new DateTime();

            if (!string.IsNullOrEmpty(date))
            {
                dtDate = DateTime.Parse(date);
                return dtDate.ToString("dd/MMM/yy");
            }

            return string.Empty;
        }
        public static string ConvertNumbertoWords(long number)
        {
            if (number == 0) return "Zero";
            if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
            string words = "";
            if ((number / 1000000) > 0)
            {
                words += ConvertNumbertoWords(number / 100000) + " Lakh ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " Thousand ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " Hundred ";
                number %= 100;
            }
            if (number > 0)
            {
                if (words != "") words += "And ";
                var unitsMap = new[]
                {
            "ZERO", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"
        };
                var tensMap = new[]
                {
            "ZERO", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninty"
        };
                if (number < 20) words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0) words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }
        public static List<SelectListItem> PopulateState(List<State> stateList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in stateList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.StateName.ToString(),
                    Value = item.StateCode.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateCity(List<City> cityList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in cityList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.CityName.ToString(),
                    Value = item.CityCode.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateDepositeBank(List<UploadBankInformation> bankList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in bankList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.BankName.ToString(),
                    Value = item.BankName.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateDepositeOffice(List<string> officeList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in officeList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.ToString(),
                    Value = item.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateAccountAndBranch(List<string> abList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in abList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.ToString(),
                    Value = item.ToString()
                });
            }

            return items;
        }
    }
}