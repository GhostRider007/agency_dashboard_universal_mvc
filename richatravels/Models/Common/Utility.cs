﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace richatravels.Models.Common
{
    public static class Utility
    {
        public static string ReadHTMLTemplate(string fileName)
        {
            using (StreamReader streamReader = new StreamReader(HttpContext.Current.Server.MapPath("~") + "\\" + "/EmailTemplates" + "/" + fileName))
            {
                try
                {
                    string xmlString = streamReader.ReadToEnd();
                    return xmlString;
                }
                finally
                {
                    streamReader.Close();
                    streamReader.Dispose();
                }
            }
        }

        public static string GetImagePath(string imageName, string folderName, string id)
        {
            return !string.IsNullOrEmpty(imageName) ? "/GetFile.ashx?path=" + folderName + "/" + id + "/" + imageName : string.Empty;
        }

        public static string GetFullPathWithAppData(string path, string id)
        {
            string fullPath = "/App_Data/";

            if (!string.IsNullOrEmpty(path))
            {
                fullPath = fullPath + path + "/" + id + "/";
            }

            return fullPath.Replace("//", "/");
        }

        public static string GetFullPathWithCustom(string path, string id)
        {
            string fullPath = "/Content/images/";

            if (!string.IsNullOrEmpty(path))
            {
                fullPath = fullPath + path + "/" + id + "/";
            }

            return fullPath.Replace("//", "/");
        }
    }
}