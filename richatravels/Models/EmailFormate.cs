﻿using richatravels.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using static ModelLibrary.FlightModel;

namespace richatravels.Models
{
    public static class EmailFormate
    {
        public static string EmailFormateReissueRequest(ReIssueReFund fltds)
        {
            StringBuilder message = new StringBuilder();

            try
            {
                if (fltds != null)
                {
                    message.Append(Utility.ReadHTMLTemplate("ForgotPassword.html"));
                    message.Replace("#Logo#", ProjectConfig.WebsiteUrl + ProjectConfig.WebsiteLogo);
                    message.Replace("#Url#", ProjectConfig.WebsiteUrl);
                    message.Replace("#Heading#", "Reissue Request");
                    message.Replace("#OrderId#", fltds.OrderId);
                    message.Replace("#PnrNo#", fltds.PNR);
                    message.Replace("#PaxName#", fltds.PaxName);
                    message.Replace("#PaxTYpe#", fltds.PaxType);
                    message.Replace("#NetAmount#", fltds.TotalFareAfterDis);
                    message.Replace("#Remark#", fltds.Remark);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return message.ToString();
        }
    }
}