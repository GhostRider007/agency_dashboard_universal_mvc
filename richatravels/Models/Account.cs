﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace richatravels.Models
{
    public class Account
    {
        public class LoginSessionDetails
        {
            public AgentStaffSession AgentStaffSession { get; set; }
            public AgentRegisterSession AgentRegisterSession { get; set; }
        }
        public class AgentStaffSession
        {
            public string StaffUserId { get; set; }
            public string FlightActive { get; set; }
            public string HotelActive { get; set; }
            public string BusActive { get; set; }
            public string RailActive { get; set; }
        }
        public class AgentRegisterSession
        {
            public string UID_USER { get; set; }
            public string LastLoginTime { get; set; }
            public string AgencyId { get; set; }
            public string AgencyName { get; set; }
            public string LoginByOTP { get; set; }
            public string FirstNameITZ { get; set; }
            public string UID { get; set; }
            public string UserType { get; set; }
            public string TypeID { get; set; }
            public bool IsCorp { get; set; }
            public string AGTY { get; set; }
            public string Agent_Type { get; set; }
            public string User_Type { get; set; }
            public string LoginByStaff { get; set; }
            public string LoginType { get; set; }
            public string ADMINLogin { get; set; }
            public string TripExec { get; set; }
        }
        public class AgencyLoginSession
        {
            public string Counter { get; set; }
            public string Title { get; set; }
            public string FName { get; set; }
            public string LName { get; set; }
            public string Name { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string District { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string ZipCode { get; set; }
            public string Phone { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }
            public string Agency_Name { get; set; }
            public string Website { get; set; }
            public string NamePanCard { get; set; }
            public string PanNo { get; set; }
            public string StateCode { get; set; }
            public string Status { get; set; }
            public string Sec_Qes { get; set; }
            public string Sec_Ans { get; set; }
            public string User_Id { get; set; }
            public string Password { get; set; }
            public string Agent_Type { get; set; }
            public string Crd_Limit { get; set; }
            public string Crd_Trns_Date { get; set; }
            public string Distr { get; set; }
            public string Agent_Status { get; set; }
            public string TDS { get; set; }
            public string Online_Tkt { get; set; }
            public string SalesExecID { get; set; }
            public string IsPWD { get; set; }
            public string GSTNo { get; set; }
            public string GST_Company_Name { get; set; }
            public string GST_Company_Address { get; set; }
            public string GST_PhoneNo { get; set; }
            public string GST_Email { get; set; }
            public string Is_GST_Apply { get; set; }
            public string GST_Remark { get; set; }
            public string GST_City { get; set; }
            public string GST_Pincode { get; set; }
            public string GST_Address { get; set; }
            public string GST_StateCode { get; set; }
            public string GST_State { get; set; }
            public string AgentLimit { get; set; }
            public string DueAmount { get; set; }
            public string VirtualCreditLimit { get; set; }
            public string AgencyId { get; set; }
            public string IsWhiteLabel { get; set; }
            public string Branch { get; set; }
            public string Currency { get; set; }
            public string Balance { get; set; }
            public string Image_Url { get; set; }
        }
        public enum enumMAILING
        {
            AIR_INVOICE,
            HOTEL_INVOICE,
            BUS_INVOICE,
            RAIL_INVOICE,
            UTILITY_INVOICE,
            AIR_BOOKING,
            HOTEL_BOOKING,
            BUS_BOOKING,
            RAIL_BOOKING,
            UTILITY_BOOKING,
            AIR_PNRSUMMARY,
            REGISTRATION_AGENT,
            REGISTRATION_SUBAGENT,
            REGISTRATION_STOCKIEST,
            SALESUPPORT,
            AIR_MAILINGINFANT,
            RAIL_ERSCOPY,
            RAIL_CREDITNOTE,
            OLSERIES_FQFORM,
            AIR_FLIGHTRESULT,
            FEEDBACK,
            FORGOTPASSWORD,
            ENQUIRY_D,
            ENQUIRY_I,
            RWT_REGISTRATION,
            RWT_REGISTRATION_DISTR,
            RWT_REGISTRATION_DEBITCREDIT,
            RWT_REFUND
        }
    }
}