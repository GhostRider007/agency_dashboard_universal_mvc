﻿
function UploadRequestSubmit() {
    var thisbutton = $("#btnUploadRequest");
    var paymodeseleted = $("#PaymentMode option:selected").val();

    if (CheckBlankValidation("Amount")) return !1;
    if (CheckDropDownBlankValidation("PaymentMode")) return !1;

    //if (paymodeseleted.toLowerCase() == "cash") {
    //    if (CheckBlankValidation("DepositCity")) return !1;
    //    if (CheckBlankValidation("DepositDate")) return !1;
    //    if (CheckDropDownBlankValidation("DepositeOffice")) return !1;
    //    if (CheckBlankValidation("ConcernPerson")) return !1;
    //}
    //if (CheckBlankValidation("Remark")) return !1;

    $('.maincss').addClass('hidden');
    $('.tempscss').removeClass('hidden');
}

$("#PaymentMode").on("change", function () {
    var paymode = $(this).val();

    AddCashSelectedHidden();
    AddCashDepositeInBankSelectedHidden();
    AddDepositeBank_Change_CashDepositeInBank();
    AddChequeSelectedHidden();
    AddDepositeBank_Change_Cheque();
    AddNetBankingSelectedHidden();
    AddDepositeBank_Change_NetBanking();
    AddDepositeBank_Change_RTGS();

    if (paymode != 0) {
        if (paymode.toLowerCase() == "cash") { RemoveCashSelectedHidden(); }
        else if (paymode.toLowerCase() == "cash deposite in bank") { RemoveCashDepositeInBankSelectedHidden(); }
        else if (paymode.toLowerCase() == "cheque") { RemoveChequeSelectedHidden(); }
        else if (paymode.toLowerCase() == "netbanking") { RemoveNetBankingSelectedHidden(); }
        else if (paymode.toLowerCase() == "rtgs") { RemoveRTGSSelectedHidden(); }
    }
    ResetAllFields();
    //setInterval(function () { ResetAllFields(); }, 1000);
});

function RemoveCashSelectedHidden() { $(".cashselectsection").removeClass("hidden"); }
function AddCashSelectedHidden() { $(".cashselectsection").addClass("hidden"); }

function RemoveCashDepositeInBankSelectedHidden() { $(".cashdepinbankselectsection").removeClass("hidden"); }
function AddCashDepositeInBankSelectedHidden() { $(".cashdepinbankselectsection").addClass("hidden"); }

function RemoveChequeSelectedHidden() { $(".chequeselectsection").removeClass("hidden"); }
function AddChequeSelectedHidden() { $(".chequeselectsection").addClass("hidden"); }

function RemoveNetBankingSelectedHidden() { $(".netbankingselectsection").removeClass("hidden"); }
function AddNetBankingSelectedHidden() { $(".netbankingselectsection").addClass("hidden"); }

function RemoveRTGSSelectedHidden() { $(".rtgsselectsection").removeClass("hidden"); }
function AddRTGSSelectedHidden() { $(".rtgsselectsection").addClass("hidden"); }

$("#DepositeBank").on("change", function () {
    var paymodeseleted = $("#PaymentMode option:selected").val();
    var depositeBank = $("#DepositeBank option:selected").val();

    AddDepositeBank_Change_CashDepositeInBank();
    AddDepositeBank_Change_Cheque();
    AddDepositeBank_Change_NetBanking();
    AddDepositeBank_Change_RTGS();

    if (depositeBank != "" && depositeBank != "0") {
        BindDepositeBranch(depositeBank);
        BindDepositeAccount(depositeBank);
        if (paymodeseleted.toLowerCase() == "cash deposite in bank") { RemoveDepositeBank_Change_CashDepositeInBank(); }
        else if (paymodeseleted.toLowerCase() == "cheque") { RemoveDepositeBank_Change_Cheque(); }
        else if (paymodeseleted.toLowerCase() == "netbanking") { RemoveDepositeBank_Change_NetBanking(); }
        else if (paymodeseleted.toLowerCase() == "rtgs") { RemoveDepositeBank_Change_RTGS(); }
    }
});

function RemoveDepositeBank_Change_CashDepositeInBank() { $(".dropdepositebank_cashdepinbank_selectsection").removeClass("hidden"); }
function AddDepositeBank_Change_CashDepositeInBank() { $(".dropdepositebank_cashdepinbank_selectsection").addClass("hidden"); }

function RemoveDepositeBank_Change_Cheque() { $(".dropdepositebank_cheque_selectsection").removeClass("hidden"); }
function AddDepositeBank_Change_Cheque() { $(".dropdepositebank_cheque_selectsection").addClass("hidden"); }

function RemoveDepositeBank_Change_NetBanking() { $(".dropdepositebank_netbanking_selectsection").removeClass("hidden"); }
function AddDepositeBank_Change_NetBanking() { $(".dropdepositebank_netbanking_selectsection").addClass("hidden"); }

function RemoveDepositeBank_Change_RTGS() { $(".dropdepositebank_rtgs_selectsection").removeClass("hidden"); }
function AddDepositeBank_Change_RTGS() { $(".dropdepositebank_rtgs_selectsection").addClass("hidden"); }


function BindDepositeBranch(depositeBank) {
    if (depositeBank != "" && depositeBank != "0") {
        $("#DepositeBranch").html("");
        $("#DepositeBranch").prop("disabled", true);
        $("#DepositeBranch").append("<option value=''>Please Wait...</option>");

        $.ajax({
            type: "Post",
            url: "/Wallet/BindDepositeBranch",
            data: '{bank: ' + JSON.stringify(depositeBank) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#DepositeBranch").html("");
                if (data != "") {
                    if (data == null) {
                        $("#DepositeBranch").append("<option value=''>Select Deposite Branch</option>");
                    }
                    else {
                        //$("#DepositeBranch").append("<option value=''>Select Deposite Branch</option>");
                        $.each(data, function (i, item) {
                            $("#DepositeBranch").append("<option value='" + item + "'> " + item + "</option>");
                        });
                        $("#DepositeBranch").prop("disabled", false);
                    }
                }
            }
        });
    }
}

function BindDepositeAccount(depositeBank) {
    if (depositeBank != "" && depositeBank != "0") {
        $("#DepositeAccountNo").html("");
        $("#DepositeAccountNo").prop("disabled", true);
        $("#DepositeAccountNo").append("<option value=''>Please Wait...</option>");

        $.ajax({
            type: "Post",
            url: "/Wallet/BindDepositeAccountNo",
            data: '{bank: ' + JSON.stringify(depositeBank) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#DepositeAccountNo").html("");
                if (data != "") {
                    if (data == null) {
                        $("#DepositeAccountNo").append("<option value=''>Select Deposite A/C No.</option>");
                    }
                    else {
                        //$("#DepositeBranch").append("<option value=''>Select Deposite Branch</option>");
                        $.each(data, function (i, item) {
                            $("#DepositeAccountNo").append("<option value='" + item + "'> " + item + "</option>");
                        });
                        $("#DepositeAccountNo").prop("disabled", false);
                    }
                }
            }
        });
    }
}

function ResetAllFields() {
    $("#ChequeDate").val("");
    $("#ChequeNumber").val("");
    $("#BankName").val("");
    $("#DepositeBank").val("").change();
    $("#DepositeBranch").val("").change();
    $("#DepositeAccountNo").val("").change();
    $("#TransactionID").val("");
    $("#BranchCode").val("");
    $("#DepositCity").val("");
    $("#DepositDate").val("");
    $("#DepositeOffice").val("").change();
    $("#ConcernPerson").val("");
    $("#ReceiptNumber").val("");
    $("#ReferenceNumber").val("");
    $("#Remark").val("");
}

//function ResetCashFields() {
//    $("#DepositCity").val("");
//    $("#DepositDate").val("");
//    $("#DepositeOffice").val("").change();
//    $("#ConcernPerson").val("");
//    $("#ReceiptNumber").val("");
//}

//function ResetCashDepositeInBankFields() {
//    $("#DepositeBank").val("").change();
//    $("#DepositeBranch").val("").change();
//    $("#DepositeAccountNo").val("").change();
//    $("#BranchCode").val("");
//}

//function ResetChequeFields() {
//    $("#ChequeDate").val("");
//    $("#ChequeNumber").val("");
//    $("#BankName").val("");
//    $("#DepositeBank").val("").change();
//    $("#DepositeBranch").val("").change();
//    $("#DepositeAccountNo").val("").change();
//}

//function ResetCommonFields() {
//    $("#ReferenceNumber").val("");
//    $("#Remark").val("");
//}