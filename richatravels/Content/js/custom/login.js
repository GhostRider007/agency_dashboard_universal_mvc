﻿$("#btnBackLogin").click(function () {
    $(".errormsg").addClass("hidden").html("");

    var thisbutton = $("#btnBackLogin");
    var username = $("#txtusername").val();
    var password = $("#txtpassword").val();
   
    if (username == null || username == "") { $(".usernamesec").css("border-bottom", "1px solid red"); $("#txtusername").focus(); return false; } else { $(".usernamesec").css("border-bottom", "1px solid #b2b2b2"); }
    if (username != "" && (password == null || password == "")) { $(".userpasssec").css("border-bottom", "1px solid red"); $("#txtpassword").focus(); return false; } else { $(".userpasssec").css("border-bottom", "1px solid #b2b2b2"); }

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

    $.ajax({
        type: "Post",
        url: "/Account/ProcessBackendLogin",        
        data: '{username: ' + JSON.stringify(username) + ',password: ' + JSON.stringify(password) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {             
            if (data != null) {
                if (data[0] == "true") {
                    window.location.href = data[1];
                }
                else {
                    $(".errormsg").removeClass("hidden").html(data[1]); 
                    $(thisbutton).html("Login");
                }
            }
            else {               
                $(thisbutton).html("Login");
            }            
        }
    });
});

$("#forgetpasswordid").click(function () {  
    ResetForm();
    $(".loginform").addClass("hidden");
    $(".forgetpasswordform").removeClass("hidden");
});

$("#gotologinform").click(function () {
    ResetForm();
    $(".loginform").removeClass("hidden");
    $(".forgetpasswordform").addClass("hidden");
});

function ResetForm() {
    $("#txtusername").val("");
    $("#txtpassword").val("");
    $("#txtUserId").val("");
    $("#txtRegisteredEmail").val("");
    $("#txtRegisteredMobileNo").val("");
}

$("#btnForgetPassword").click(function () {
    $(".forgeterrormsg").addClass("hidden").html("");

    var thisbutton = $("#btnForgetPassword");
    var userid = $("#txtUserId").val();
    var email = $("#txtRegisteredEmail").val();
    var mobile = $("#txtRegisteredMobileNo").val();

    if (userid == null || userid == "") { $(".useridsec").css("border-bottom", "1px solid red"); $("#txtUserId").focus(); return false; } else { $(".useridsec").css("border-bottom", "1px solid #b2b2b2"); }
    if (email == null || email == "") { $(".useremailsec").css("border-bottom", "1px solid red"); $("#txtRegisteredEmail").focus(); return false; } else { $(".useremailsec").css("border-bottom", "1px solid #b2b2b2"); }
    if (mobile == null || mobile == "") { $(".usermobilessec").css("border-bottom", "1px solid red"); $("#txtRegisteredMobileNo").focus(); return false; } else { $(".usermobilessec").css("border-bottom", "1px solid #b2b2b2"); }

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

    $.ajax({
        type: "Post",
        url: "/Account/ProcessForgetPassword",
        data: '{userid: ' + JSON.stringify(userid) + ',email: ' + JSON.stringify(email) + ',mobile: ' + JSON.stringify(mobile) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {                   
                    $(".forgeterrormsg").css("color", "#57b846").css("font-weight", "bold").removeClass("hidden").html(data[1]);
                    ResetForm();
                }
                else {
                    $(".forgeterrormsg").css("color", "#dc3545").removeClass("hidden").html(data[1]);                   
                }
            }

            $(thisbutton).html("Get Password");
        }
    });
});
