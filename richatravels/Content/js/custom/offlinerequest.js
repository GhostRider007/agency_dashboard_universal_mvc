﻿$(".TripTypeOneWay").click(function () { $(".ReturnDate").addClass("hidden"); $("#txtReturnDate").val(""); $(".TripTypeRound").removeClass("activeround"); });
$(".TripTypeRound").click(function () { $(this).addClass("activeround"); $(".ReturnDate").removeClass("hidden"); $("#txtReturnDate").val(""); });

$(document.body).on('click', "#BackToSearch", function (e) {
    window.location.reload();
    //$("#SearchSectionBind").removeClass("hidden"); $("#DynamicSectionBind").addClass("hidden");
});

var offline = {};
function OfflineRequest() {
    if (CheckBlankValidation("txtLeavingForm")) return !1;
    if (CheckBlankValidation("txtLeavingTo")) return !1;
    if (CheckBlankValidation("txtDepartureDate")) return !1;
    if ($(".TripTypeRound").hasClass("activeround")) {
        if (CheckBlankValidation("txtReturnDate")) return !1;
    }
    if (CheckBlankValidation("txtPreferedAirlines")) return !1;

    offline.TripType = $("input[name='triptype']:checked").val();
    offline.BookingType = $("input[name='bookingtype']:checked").val();
    offline.FromCity = $("#txtLeavingForm").val();
    offline.FromCountry = $("#hdnFromCountryCode").val();
    offline.DestinationCity = $("#txtLeavingTo").val();
    offline.ToCountry = $("#hdnToCountryCode").val();
    offline.DepartureDate = $("#txtDepartureDate").val();
    offline.ReturnDate = $("#txtReturnDate").val();
    offline.Time = $("#ddlDepartAnytime").val();
    offline.Adult = $("#ddlAdult").val();
    offline.Child = $("#ddlChild").val();
    offline.Infant = $("#ddlInfrant").val();
    offline.ClassType = $("#ddlClass").val();
    offline.PreferedAirlines = $("#txtPreferedAirlines").val();
    offline.PreferedAirlinesCode = $("#hdnPAirlinesyCode").val();
    offline.Classes = $("#ddlClasses").val();
    offline.PaymentLimit = $("#ddlPaymentLimit").val();

    if (parseInt(offline.Infant) > parseInt(offline.Adult)) {
        alert("No of infant should not greater than Adults");
        return false;
    }

    var result = "";
    for (i = 1; i <= parseInt(offline.Adult); i++) { result += AdultSection(i); }
    if (parseInt(offline.Child) > 0) { for (c = 1; c <= parseInt(offline.Child); c++) { result += ChildSection(c); } }
    if (parseInt(offline.Infant) > 0) { for (f = 1; f <= parseInt(offline.Infant); f++) { result += InFantSection(f); } }

    $("#InnerSectionBind").append(result);
    $("#SearchSectionBind").addClass("hidden");
    $("#DynamicSectionBind").removeClass("hidden");
}

function AdultSection(adultcount) {
    var titleFName = "<div class='row form-group'><div class='col-sm-12 form-group'><h3 class='adulthed'>Passenger " + adultcount + " (Adult)</h3></div>"
        + "<div class='col-sm-3'><div class='row'>"
        + "<div class='col-sm-6'>"
        + Title(adultcount, "Adult")
        + "</div>"
        + "<div class='col-sm-6 form-validation'>"
        + "<input type='text' class='form-control' name='txtAdultFirstName_" + adultcount + "' id='txtAdultFirstName_" + adultcount + "' placeholder='first name' />"
        + "</div>"
        + "</div>"
        + "</div>";

    var lastNameAge = "<div class='col-sm-3'>"
        + "<div class='row'>"
        + "<div class='col-sm-6 form-validation'>"
        + "<input type='text' class='form-control' name='txtAdultLastName_" + adultcount + "' id='txtAdultLastName_" + adultcount + "' placeholder='last name' />"
        + "</div>"
        + "<div class='col-sm-6 form-validation'>"
        + AdultAge(adultcount)
        + "</div>"
        + "</div>"
        + "</div>";

    var FreqFlyerNOPassportNo = "<div class='col-sm-3'>"
        + "<div class='row'>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtAdultFreqFlyerNO_" + adultcount + "' id='txtAdultFreqFlyerNO_" + adultcount + "' placeholder='freq flyer no' />"
        + "</div>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtAdultPassportNo_" + adultcount + "' id='txtAdultPassportNo_" + adultcount + "' placeholder='passport no' />"
        + "</div>"
        + "</div>"
        + "</div>";

    var PPExpVisaDet = "<div class='col-sm-3'>"
        + "<div class='row'>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtAdultPPExp_" + adultcount + "' id='txtPPExp_" + adultcount + "' placeholder='pp exp' />"
        + "</div>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtAdultVisaDet_" + adultcount + "' id='txtVisaDet_" + adultcount + "' placeholder='visa det' />"
        + "</div>"
        + "</div>"
        + "</div>";

    return titleFName + lastNameAge + FreqFlyerNOPassportNo + PPExpVisaDet + "</div>";
}

function ChildSection(childcount) {
    var titleFName = "<div class='row form-group'><div class='col-sm-12 form-group'><h3 class='adulthed'>Passenger " + childcount + " (Child)</h3></div>"
        + "<div class='col-sm-3'><div class='row'>"
        + "<div class='col-sm-6'>"
        + Title(childcount, "Child")
        + "</div>"
        + "<div class='col-sm-6 form-validation'>"
        + "<input type='text' class='form-control' name='txtChildFirstName_" + childcount + "' id='txtChildFirstName_" + childcount + "' placeholder='first name' />"
        + "</div>"
        + "</div>"
        + "</div>";

    var lastNameAge = "<div class='col-sm-3'>"
        + "<div class='row'>"
        + "<div class='col-sm-6 form-validation'>"
        + "<input type='text' class='form-control' name='txtChildLastName_" + childcount + "' id='txtChildLastName_" + childcount + "' placeholder='last name' />"
        + "</div>"
        + "<div class='col-sm-6 form-validation'>"
        + ChildAge(childcount)
        + "</div>"
        + "</div>"
        + "</div>";

    var FreqFlyerNOPassportNo = "<div class='col-sm-3'>"
        + "<div class='row'>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtChildFreqFlyerNO_" + childcount + "' id='txtChildFreqFlyerNO_" + childcount + "' placeholder='freq flyer no' />"
        + "</div>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtChildPassportNo_" + childcount + "' id='txtChildPassportNo_" + childcount + "' placeholder='passport no' />"
        + "</div>"
        + "</div>"
        + "</div>";

    var PPExpVisaDet = "<div class='col-sm-3'>"
        + "<div class='row'>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtChildPPExp_" + childcount + "' id='txtChildPPExp_" + childcount + "' placeholder='pp exp' />"
        + "</div>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtChildVisaDet_" + childcount + "' id='txtChildVisaDet_" + childcount + "' placeholder='visa det' />"
        + "</div>"
        + "</div>"
        + "</div>";

    return titleFName + lastNameAge + FreqFlyerNOPassportNo + PPExpVisaDet + "</div>";
}

function InFantSection(infantcount) {
    var titleFName = "<div class='row form-group'><div class='col-sm-12 form-group'><h3 class='adulthed'>Passenger " + infantcount + " (Infant)</h3></div>"
        + "<div class='col-sm-3'><div class='row'>"
        + "<div class='col-sm-6'>"
        + Title(infantcount, "InFant")
        + "</div>"
        + "<div class='col-sm-6 form-validation'>"
        + "<input type='text' class='form-control' name='txtInFantFirstName_" + infantcount + "' id='txtInFantFirstName_" + infantcount + "' placeholder='first name' />"
        + "</div>"
        + "</div>"
        + "</div>";

    var lastNameAge = "<div class='col-sm-3'>"
        + "<div class='row'>"
        + "<div class='col-sm-6 form-validation'>"
        + "<input type='text' class='form-control' name='txtInFantLastName_" + infantcount + "' id='txtInFantLastName_" + infantcount + "' placeholder='last name' />"
        + "</div>"
        + "<div class='col-sm-6 form-validation'>"
        + InFantAge(infantcount)
        + "</div>"
        + "</div>"
        + "</div>";

    var FreqFlyerNOPassportNo = "<div class='col-sm-3'>"
        + "<div class='row'>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtInFantFreqFlyerNO_" + infantcount + "' id='txtInFantFreqFlyerNO_" + infantcount + "' placeholder='freq flyer no' />"
        + "</div>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtInFantPassportNo_" + infantcount + "' id='txtInFantPassportNo_" + infantcount + "' placeholder='passport no' />"
        + "</div>"
        + "</div>"
        + "</div>";

    var PPExpVisaDet = "<div class='col-sm-3'>"
        + "<div class='row'>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtInFantPPExp_" + infantcount + "' id='txtInFantPPExp_" + infantcount + "' placeholder='pp exp' />"
        + "</div>"
        + "<div class='col-sm-6'>"
        + "<input type='text' class='form-control' name='txtInFantVisaDet_" + infantcount + "' id='txtInFantVisaDet_" + infantcount + "' placeholder='visa det' />"
        + "</div>"
        + "</div>"
        + "</div>";

    return titleFName + lastNameAge + FreqFlyerNOPassportNo + PPExpVisaDet + "</div>";
}

function Title(adultcount, type) {
    var title = "<select name='ddl" + type + "Title_" + adultcount + "' id='ddl" + type + "Title_" + adultcount + "' class='form-control adulttitle'>"
        + " <option value='Mr'>Mr.</option>"
        + "<option value='Mrs'>Mrs.</option>"
        + "<option value='Ms'>Ms.</option>"
        + "<option value='Mstr'>Mstr.</option>"
        + "<option value='Miss'>Miss.</option>"
        + "</select>";
    return title;
}

function AdultAge(adultcount) {
    var age = "<select name='ddlAdultAge_" + adultcount + "' id='ddlAdultAge_" + adultcount + "' class='form-control'>"
        + "<option value=''>Year</option>"
        + "<option value='12'>12</option>"
        + "<option value='13'>13</option>"
        + "<option value='14'>14</option>"
        + "<option value='15'>15</option>"
        + "<option value='16'>16</option>"
        + "<option value='17'>17</option>"
        + "<option value='18'>18</option>"
        + "<option value='19'>19</option>"
        + "<option value='20'>20</option>"
        + "<option value='21'>21</option>"
        + "<option value='22'>22</option>"
        + "<option value='23'> 23</option>"
        + "<option value='24'>24</option>"
        + "<option value='25'>25</option>"
        + "<option value='26'>26</option>"
        + "<option value='27'>27</option>"
        + "<option value='28'>28</option>"
        + "<option value='29'>29</option>"
        + "<option value='30'>30</option>"
        + "<option value='31'>31</option>"
        + "<option value='32'>32</option>"
        + " <option value='33'>33</option>"
        + " <option value='34'>34</option>"
        + " <option value='35'>35</option>"
        + " <option value='36'>36</option>"
        + " <option value='37'>37</option>"
        + " <option value='38'>38</option>"
        + " <option value='39'>39</option>"
        + " <option value='40'>40</option>"
        + " <option value='41'>41</option>"
        + " <option value='42'>42</option>"
        + " <option value='43'>43</option>"
        + " <option value='44'>44</option>"
        + " <option value='45'>45</option>"
        + " <option value='46'>46</option>"
        + " <option value='47'>47</option>"
        + " <option value='48'>48</option>"
        + " <option value='49'>49</option>"
        + " <option value='50'>50</option>"
        + " <option value='51'>51</option>"
        + " <option value='52'>52</option>"
        + " <option value='53'>53</option>"
        + " <option value='54'>54</option>"
        + " <option value='55'>55</option>"
        + " <option value='56'>56</option>"
        + " <option value='57'>57</option>"
        + " <option value='58'>58</option>"
        + " <option value='59'>59</option>"
        + " <option value='60'>60</option>"
        + " <option value='61'>61</option>"
        + " <option value='62'>62</option>"
        + " <option value='63'>63</option>"
        + " <option value='64'>64</option>"
        + " <option value='65'>65</option>"
        + " <option value='66'>66</option>"
        + " <option value='67'>67</option>"
        + " <option value='68'>68</option>"
        + " <option value='69'>69</option>"
        + " <option value='70'>70</option>"
        + " <option value='71'>71</option>"
        + " <option value='72'>72</option>"
        + " <option value='73'>73</option>"
        + " <option value='74'>74</option>"
        + " <option value='75'>75</option>"
        + " <option value='76'>76</option>"
        + " <option value='77'>77</option>"
        + " <option value='78'>78</option>"
        + " <option value='79'>79</option>"
        + " <option value='80'>80</option>"
        + " <option value='81'>81</option>"
        + " <option value='82'>82</option>"
        + " <option value='83'>83</option>"
        + " <option value='84'>84</option>"
        + " <option value='85'>85</option>"
        + " <option value='86'>86</option>"
        + "<option value='87'>87</option>"
        + " <option value='88'>88</option>"
        + " <option value='89'>89</option>"
        + " <option value='90'>90</option>"
        + "</select>";
    return age;
}

function ChildAge(childcount) {
    var age = "<select name='ddlChildAge_" + childcount + "' id='ddlChildAge_" + childcount + "' class='form-control'>"
        + "<option value=''>Year</option>"
        + "<option value='02'>02</option>"
        + "<option value='03'>03</option>"
        + "<option value='04'>04</option>"
        + "<option value='05'>05</option>"
        + "<option value='06'>06</option>"
        + "<option value='07'>07</option>"
        + "<option value='08'>08</option>"
        + "<option value='09'>09</option>"
        + "<option value='10'>10</option>"
        + "<option value='11'>11</option>"
        + "</select>";
    return age;
}

function InFantAge(infantcount) {
    var age = "<select name='ddlInFantAge_" + infantcount + "' id='ddlInFantAge_" + infantcount + "' class='form-control'>"
        + "<option value=''>Year</option>"
        + "<option value='01'>01</option>"
        + "<option value='02'>02</option>"
        + "</select>";
    return age;
}

function PostBookMyFlight() {
    var thisbutton = $("#BookMyFlight");

    for (i = 1; i <= parseInt(offline.Adult); i++) { if (CheckFocusBlankValidation("txtAdultFirstName_" + i + "")) return !1; if (CheckFocusBlankValidation("txtAdultLastName_" + i + "")) return !1; if (CheckFocusDropDownBlankValidation("ddlAdultAge_" + i + "")) return !1; }
    if (parseInt(offline.Child) > 0) { for (c = 1; c <= parseInt(offline.Child); c++) { if (CheckFocusBlankValidation("txtChildFirstName_" + c + "")) return !1; if (CheckFocusBlankValidation("txtChildLastName_" + c + "")) return !1; if (CheckFocusDropDownBlankValidation("ddlChildAge_" + i + "")) return !1; } }
    if (parseInt(offline.Infant) > 0) { for (f = 1; f <= parseInt(offline.Infant); f++) { if (CheckFocusBlankValidation("txtInFantFirstName_" + f + "")) return !1; if (CheckFocusBlankValidation("txtInFantLastName_" + f + "")) return !1; if (CheckFocusDropDownBlankValidation("ddlInFantAge_" + i + "")) return !1; } }

    offline.AdultDetails = BindBookFlightDetail("Adult", offline.Adult, "ADT");
    if (parseInt(offline.Child) > 0) { offline.ChildDetails = BindBookFlightDetail("Child", offline.Child, "CHD"); }
    if (parseInt(offline.Infant) > 0) { offline.InFantDetails = BindBookFlightDetail("InFant", offline.Infant, "INF"); }

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");
    $.ajax({
        type: "Post",
        url: "/FlightReport/InsertOfflineRequest",
        data: '{offline: ' + JSON.stringify(offline) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    ShowMessagePopup("Success", "#00a300", data[1], "#00a300");
                }
                else {
                    ShowMessagePopup("Failed", "#d91717", "Error Occured !", "#d91717");
                }
            }

            $(thisbutton).html("Submit");
        }
    });
}

function BindBookFlightDetail(guesttype, guestcount, paxtype) {
    var listDetails = [];
    for (i = 1; i <= parseInt(guestcount); i++) {
        var guestdel = {};
        guestdel.Title = $("#ddl" + guesttype + "Title_" + i).val();
        guestdel.FirstName = $("#txt" + guesttype + "FirstName_" + i).val();
        guestdel.LastName = $("#txt" + guesttype + "LastName_" + i).val();
        guestdel.Age = $("#ddl" + guesttype + "Age_" + i).val();
        guestdel.PaxType = paxtype;
        guestdel.FreqFlyerNO = $("#txt" + guesttype + "FreqFlyerNO_" + i).val();
        guestdel.PassportNo = $("#txt" + guesttype + "PassportNo_" + i).val();
        guestdel.PPExp = $("#txt" + guesttype + "PPExp_" + i).val();
        guestdel.VisaDet = $("#txt" + guesttype + "VisaDet_" + i).val();
        listDetails.push(guestdel);
    }
    return listDetails;
}

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".succmodelheading").css("color", headcolor).html(headerHeading);
    $(".succmodelcontent").css("color", bodycolor).html("<p>" + bodyMsg + "</p>");
}

$("#ModelPoupDismiss").click(function () {
    window.location.reload();
});